﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="tim-se-thay-video.aspx.cs" Inherits="go_tim_thay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Go Tìm Sẽ Thấy Video</title>
    <meta name="Description" content="Go Tìm Sẽ Thấy Video" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="laughs">
        <div class="container">

            <div class="head-menu">
                <div class="row">
                    <div class="col-lg-6">
                         <p class="site-node" id="site">
            <a href="tim-se-thay.aspx">Tìm sẽ thấy</a><span>Thư viện video</span>
        </p>
                    </div>
                    <div class="col-lg-6">
                       <%-- <asp:RadioButtonList CssClass="list-filter" ID="radSearch" AutoPostBack="true"
                            runat="server" RepeatDirection="Horizontal">
                             <asp:ListItem Value="" Selected="true">Tất Cả</asp:ListItem>
                            <asp:ListItem Value="1">Fun Quote/ Message</asp:ListItem>
                            <asp:ListItem Value="2">Fun picture</asp:ListItem>
                            <asp:ListItem Value="3">Fun clip/ video</asp:ListItem>
                        </asp:RadioButtonList>--%>
                    </div>
                </div>
            </div>
            <div class="head-menu head-img">
                <div class="row">
                    <div class="col-lg-6">
                        <h1 class="title">Gallery Video</h1>
                    </div>
                    <div class="col-lg-6">
                        <div class="pager">

                            <asp:DataPager ID="DataPager1" runat="server" PageSize="24" PagedControlID="ListView1">
                                <Fields>
                                    <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="numer-paging" CurrentPageLabelCssClass="current" />

                                </Fields>
                            </asp:DataPager>
                        </div>
                    </div>
                </div>
            </div>
            <div class="go-for-laughs">
                <div class="row">

                    <asp:ListView ID="ListView1" runat="server"
                        DataSourceID="ObjectDataSource1"
                        EnableModelValidation="True">
                        <ItemTemplate>
                            <div class="col-xs-6 col-md-3">
                                <div class="box-articles">
                                    <a class="articles-img bcover" title='<%# Eval("Title") %>' href='<%# progressTitle(Eval("Title")) + "-s" + ((string.IsNullOrEmpty(Eval("ParentID").ToString())?Eval("LearnsCategoryID").ToString() :Eval("ParentID").ToString())=="1"? "-pqd-" : (string.IsNullOrEmpty(Eval("ParentID").ToString())?Eval("LearnsCategoryID").ToString() :Eval("ParentID").ToString())=="2"? "-ppd-":"-pvd-") + Eval("LearnsID") +".aspx" %>'>
                                        <img  class="img-responsive" src='<%# ((string.IsNullOrEmpty(Eval("ParentID").ToString())?Eval("LearnsCategoryID").ToString() :Eval("ParentID").ToString()) =="1"? "http://www.liveplus.vn/res/learns/quote/" : (string.IsNullOrEmpty(Eval("ParentID").ToString())?Eval("LearnsCategoryID").ToString() :Eval("ParentID").ToString()) =="2" ? "http://www.liveplus.vn/res/learns/picture/": "http://www.liveplus.vn/res/learns/video/thumbs/") + Eval("ImagePath") %>' alt='<%# Eval("ImagePath") %>' runat="server" />
                                       <span class='mask-video'></span>
                                    </a>
                                </div>
                            </div>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <span runat="server" id="itemPlaceholder" />
                        </LayoutTemplate>
                    </asp:ListView>
                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
                        SelectMethod="LearnsSelectAll"
                        TypeName="TLLib.Learns">
                        <SelectParameters>
                            <asp:Parameter Name="StartRowIndex" Type="String" />
                            <asp:Parameter Name="EndRowIndex" Type="String" />
                            <asp:Parameter Name="Title" Type="String" />
                            <asp:Parameter Name="Description" Type="String" DefaultValue="" />
                            <asp:Parameter DefaultValue="3" Name="LearnsCategoryID" Type="String" />
                            <asp:Parameter Name="IsAvailable" Type="String" DefaultValue="true" />
                            <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                            <asp:Parameter Name="IsNew" Type="String" />
                            <asp:Parameter Name="Createby" Type="String" />
                            <asp:Parameter Name="Priority" Type="String" />
                            <asp:Parameter Name="InGallery" Type="String" DefaultValue="true" />
                            <asp:Parameter Name="IsWinner" Type="String" />
                            <asp:Parameter Name="Name" Type="String" />
                            <asp:Parameter Name="Email" Type="String" />
                            <asp:Parameter Name="Phone" Type="String" />
                            <asp:Parameter Name="SortByPriority" Type="String" DefaultValue="true" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
                <div class="pager">
                    <asp:DataPager ID="DataPager2" runat="server" PageSize="24" PagedControlID="ListView1">
                        <Fields>
                            <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="numer-paging" CurrentPageLabelCssClass="current" />

                        </Fields>
                    </asp:DataPager>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphPopup" runat="Server">
</asp:Content>

