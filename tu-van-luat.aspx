﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true"
    CodeFile="tu-van-luat.aspx.cs" Inherits="hoc_de_lam_tu_van" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Tư vấn luật</title>
    <meta name="Description" content="Tư vấn luật" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="wrapper-main">
        <div class="container">
            <div class="col-md-9">
                <div class="wrap-section">
                    <div id="site">
                        <span>Tư vấn luật</span>
                    </div>
                    <div class="head-box">
                        <div class="head-box-b">
                            <h1 class="title">Tư vấn luật</h1>
                        </div>
                    </div>
                    <div class="panel-group" id="accordion">

                        <asp:ListView ID="ListView1" runat="server"
                            DataSourceID="ObjectDataSource1"
                            EnableModelValidation="True">
                            <ItemTemplate>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href='#collapse<%# Eval("CareerID") %>'>
                                                <span class="sup-icon sup-icont show-desktop"><strong>xem
                                            <br />
                                                    trả lời</strong></span>
                                                <span class="box-sup">
                                                    <span class="sup-img">
                                                        <img id="Img1" class="img-responsive" alt="" src='<%# "http://www.liveplus.vn/res/career/" + Eval("ImageName") %>'
                                                            runat="server"
                                                            visible='<%# string.IsNullOrEmpty( Eval("ImageName").ToString()) ? false : true 
%>' />
                                                    </span>
                                                    <span class="sup-content"><%# Eval("Description") %></span>
                                                </span>
                                                <span class="sup-icon-2 show-mobile"><strong>xem trả lời</strong></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id='collapse<%# Eval("CareerID") %>' class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="sup-text">
                                                <div class="text-box">
                                                    <%# Eval("Content") %>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
                            SelectMethod="CareerSelectAll" TypeName="TLLib.Career">
                            <SelectParameters>
                                <asp:Parameter Name="StartRowIndex" Type="String" />
                                <asp:Parameter Name="EndRowIndex" Type="String" />
                                <asp:Parameter Name="Keyword" Type="String" />
                                <asp:Parameter Name="CareerTitle" Type="String" />
                                <asp:Parameter Name="Description" Type="String" />
                                <asp:Parameter Name="CareerCategoryID" Type="String" />
                                <asp:Parameter Name="Tag" Type="String" />
                                <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                                <asp:Parameter Name="FromDate" Type="String" />
                                <asp:Parameter Name="ToDate" Type="String" />
                                <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                                <asp:Parameter Name="Priority" Type="String" />
                                <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                         <div class="pager">

                            <asp:DataPager ID="DataPager1" runat="server" PageSize="6" PagedControlID="ListView1">
                                <Fields>
                                    <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="numer-paging" CurrentPageLabelCssClass="current" />

                                </Fields>
                            </asp:DataPager>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="wrap-asibar">
                    <h2 class="title-a">tin tức- sự kiện</h2>
                    <div class="row">
                        <asp:ListView ID="ListView2" runat="server"
                            DataSourceID="ObjectDataSource3"
                            EnableModelValidation="True">
                            <ItemTemplate>
                                <div class="col-xs-6 col-md-12">
                                    <div class="box-ques">
                                        <a href='<%# progressTitle(Eval("NewsFeaturedTitle")) + "-nf-" + Eval("NewsFeaturedID") +".aspx" %>' class="box-aques">
                                            <span class="box-img">
                                                <img class="img-responsive" id="Img1" alt='' src='<%# "http://www.liveplus.vn/res/newsfeatured/" + Eval("ImageName") %>' visible='<%# string.IsNullOrEmpty( Eval("ImageName").ToString()) ? false : 
true %>'
                                                    runat="server" /></span>
                                            <span class="box-name"><%# Eval("NewsFeaturedTitle") %></span>
                                        </a>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                        <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" SelectMethod="NewsFeaturedSelectAll"
                            TypeName="TLLib.NewsFeatured">
                            <SelectParameters>
                                <asp:Parameter Name="StartRowIndex" Type="String" />
                                <asp:Parameter Name="EndRowIndex" Type="String" />
                                <asp:Parameter Name="Keyword" Type="String" />
                                <asp:Parameter Name="NewsFeaturedTitle" Type="String" />
                                <asp:Parameter Name="Description" Type="String" />
                                <asp:Parameter Name="NewsFeaturedCategoryID" Type="String" />
                                <asp:Parameter Name="Tag" Type="String" />
                                <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                                <asp:Parameter Name="IsHot" Type="String" />
                                <asp:Parameter Name="IsNew" Type="String" />
                                <asp:Parameter Name="FromDate" Type="String" />
                                <asp:Parameter Name="ToDate" Type="String" />
                                <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                                <asp:Parameter Name="Priority" Type="String" />
                                <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphPopup" runat="Server">
</asp:Content>
