﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site-sub.master" AutoEventWireup="true" CodeFile="hoc-de-lam-ISO.aspx.cs" Inherits="hoc_de_lam" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>ISO 39001</title>
    <meta name="Description" content="ISO 39001" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="site">
        <a href="Default.aspx#play-learn">Học để làm</a><span>ISO 39001</span>
    </div>
    <h1 class="title">ISO 39001</h1>
    <div class="box-like">
        <div class="box-like-b">
        <div class="fb-like" data-href='<%= Page.Request.Url.AbsolutePath %>'
                            data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                        </div>
        </div>
    </div>
    <div class="panel-group tabs-group" id="accordion2">

        <asp:ListView ID="ListView1" runat="server"
            DataSourceID="ObjectDataSource1"
            EnableModelValidation="True">
            <ItemTemplate>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion2" href='#collapse<%# Eval("GoISOID") %>'>
                                <span class="tabs-name"><strong>&bull;</strong><%# Eval("GoISOTitle") %></span><span class="tabs-icon"></span></a></h4>
                    </div>
                    <div id='collapse<%# Eval("GoISOID") %>' class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="info-text">
                                <%# Eval("Content") %>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
            <LayoutTemplate>
                <span runat="server" id="itemPlaceholder" />
            </LayoutTemplate>
        </asp:ListView>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
            SelectMethod="GoISOSelectAll"
            TypeName="TLLib.GoISO">
            <SelectParameters>
                <asp:Parameter Name="StartRowIndex" Type="String" />
                <asp:Parameter Name="EndRowIndex" Type="String" />
                <asp:Parameter Name="Keyword" Type="String" />
                <asp:Parameter Name="GoISOTitle" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter Name="GoISOCategoryID" Type="String" />
                <asp:Parameter Name="Tag" Type="String" />
                <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                <asp:Parameter Name="IsHot" Type="String" />
                <asp:Parameter Name="IsNew" Type="String" />
                <asp:Parameter Name="FromDate" Type="String" />
                <asp:Parameter Name="ToDate" Type="String" />
                <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                <asp:Parameter Name="Priority" Type="String" />
                <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <div class="pager">

                            <asp:DataPager ID="DataPager1" runat="server" PageSize="6" PagedControlID="ListView1">
                                <Fields>
                                    <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="numer-paging" CurrentPageLabelCssClass="current" />

                                </Fields>
                            </asp:DataPager>
                        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphSibar" runat="Server">
    <%--<div class="box-res">
        <h2><strong>đăng ký tham gia dự án</strong>(dành cho doanh nghiệp)</h2>
        <a class="link-popup" href="javascript:void(0);" data-toggle="modal" data-target="#popupp"></a>
    </div>--%>
  <div class="box-wp">
        <h2 class="title-b">TÌM HIỂU VỀ DỰ ÁN VÀ CÁC CHƯƠNG TRÌNH</h2>
        <div class="studybox">
            <div class="row">
                   <div class="col-xs-6 col-md-12">
                    <div class="box-ques ma">
                        <a href="hoc-de-lam-tin-tuc.aspx" class="box-aques">
                            <span class="box-img"><img class="img-responsive" src="assets/images/site-img-1.jpg" alt=""/></span>
                            <span class="box-name">Tin tức</span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-md-12">
                    <div class="box-ques md">
                        <a href="hoc-de-lam.aspx" class="box-aques">
                            <span class="box-img"><img class="img-responsive" src="assets/images/site-img-2.jpg" alt=""/></span>
                            <span class="box-name">Giới thiệu dự án GOsmart</span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-md-12">
                    <div class="box-ques mv">
                        <a href="hoc-de-lam-ISO.aspx" class="box-aques">
                            <span class="box-img"><img class="img-responsive" src="assets/images/site-img-3.jpg" alt=""/></span>
                            <span class="box-name">ISO 39001</span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-md-12">
                    <div class="box-ques mx">
                        <a href="hoc-de-lam-tu-van.aspx" class="box-aques">
                            <span class="box-img"><img class="img-responsive" src="assets/images/site-img-4.jpg" alt=""/></span>
                            <span class="box-name">Tư vấn hệ thống quản lý ATGT</span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-md-12">
                    <div class="box-ques ml">
                        <a href="hoc-de-lam-dao-tao-doanh-nghiep.aspx" class="box-aques">
                            <span class="box-img"><img class="img-responsive" src="assets/images/site-img-5.jpg" alt=""/></span>
                            <span class="box-name">Đào tạo ATGT cho doanh nghiệp</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphPopup" runat="Server">

  
</asp:Content>
