$(document).ready(function () {
    /* ==========================================================================
    lazzy load image (jquery.lazyload.min.js)
    ========================================================================== */
    //$('img').lazyload({
    //    skip_invisible: false
    //}).removeAttr('style');
    /* ==========================================================================
    show - hide search textbox in site.master
    ========================================================================== */
    $("#serachtop").hover(function () {
        $(this).find(".search-text").stop().animate({ "width": 180 + "px", "right": 32 + "px" }, 300);
    }, function () {
        $(this).find(".search-text").stop().animate({ "width": 0 + "px", "right": 0 + "px" }, 300);
    });
    /* ==========================================================================
    show - hide submenu in site.master
    ========================================================================== */
    $("#menu .no-show").remove();
    $("#menu .m-hover").hover(function () {
        $(this).find(".menu-sub img").each(function () {
            var img = $(this);
            if (img.attr("data-original")) {
                img.attr("src", img.attr("data-original"));
                img.removeAttr("data-original");
            }
        });
        var wmenu = $("#menu").width();
        var pright = $(this).position().left;
        var countli = $(this).find(".menu-sub").find(".lia").size();
        $(this).find(".menu-sub").find(".menu-list").width(wmenu);
        $(this).find(".menu-sub").find(".menu-img").height(parseInt((wmenu / 4 - 40) * 0.65));
        if (countli == 5) {
            $(this).find(".menu-sub").css({ "left": "auto", "right": 0, "width": wmenu }).find(".lia").css({ "width": "20%" });
        } else if (countli == 4) {
            $(this).find(".menu-sub").css({ "left": "auto", "right": 0, "width": wmenu });
        } else if (countli == 3) {
            if ($(this).hasClass("first")) {
                $(this).css({ "position": "relative" }).find(".menu-sub").css({ "left": 0, "width": (wmenu / 4) * 3 });
            } else {
                $(this).css({ "position": "relative" }).find(".menu-sub").css({ "left": "-" + (((wmenu / 4) * 3) / 2) + "px", "width": (wmenu / 4) * 3 });
            }
        } else if (countli == 2) {
            var wi = (wmenu / 4) * 2;
            $(this).css({ "position": "relative" }).find(".menu-sub").css({ "left": "-" + (wi / 2 - 50) + "px", "width": (wmenu / 4) * 2 });
        } else {
            $(this).css({ "position": "relative" }).find(".menu-sub").css({ "left": 0, "width": (wmenu / 4) * 3 });
        }
        $(this).find(".menu-sub").stop(true, true).fadeIn();
    }, function () {
        $(this).find(".menu-sub").stop(true, true).fadeOut();
    });
});
