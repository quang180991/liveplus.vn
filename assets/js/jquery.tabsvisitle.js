﻿(function ($) {
    $.fn.extend({

        tabsvisite: function (options) {

            // Đặt các giá trị mặc định
            var defaults = {
                idwrap: "",
                csstit: "",
                booltext: true,
                csswraptext: "",
                cssboxtext: ""
            };

            var options = $.extend(defaults, options);

            return this.each(function () {
                var opts = options;

                // Đặt tên biến cho element (ul)
                var obj = $(this);
                mytabsvisite();
                function mytabsvisite() {
                    var idcontrol = $(opts.idwrap, obj);
                    var wrapcontent = $(opts.csscontent, obj);
                    var csswraptext = $(opts.csswraptext, obj);
                    var csstit = $(opts.csstit, obj);

                    obj.find(".tab-panes").css({ "visibility": "hidden", "z-index": "-1", "position": "absolute" });
                    idcontrol.find('a').click(function (e) {
                        e.preventDefault();
                        idcontrol.find('li').removeClass("active");
                        $(this).parent('li').addClass("active");
                        var idhref = $(this).attr("href");
                        obj.find(".tab-panes").css({ "visibility": "hidden", "z-index": "-1", "position": "absolute" });
                        $(idhref).css({ "visibility": "visible", "z-index": "99", "position": "relative" });
                        csstit.find("a").removeClass("active");
                        csstit.find("a[href='" + idhref + "']").addClass("active");
                        if (opts.booltext) {
                            obj.find(opts.cssboxtext).hide();
                            obj.find(opts.cssboxtext + "[data-id='" + idhref + "']").show();
                            var topp = csswraptext.offset().top;
                            $("html, body").animate({ scrollTop: topp }, 500);
                        }
                        return false;
                    });
                    csstit.find("a").click(function () {
                        csstit.find("a").removeClass("active");
                        $(this).addClass("active");
                        var idhref = $(this).attr("href");
                        idcontrol.find("a[href='" + idhref + "']").trigger("click");
                        if ($(this).parents("#play-learn").size() == 1) {
                            var topp = csswraptext.offset().top;
                            $("html, body").animate({ scrollTop: topp }, 500);
                        }
                        return false;
                    });
                    idcontrol.find('li:first').addClass("active");
                    $(opts.csscontent + ' .tab-panes:first').css({ "visibility": "visible", "z-index": "99", "position": "relative" });
                    var idtf = idcontrol.find('li:first').find("a:first").attr("href");
                    obj.find(opts.cssti).find("a[href='" + idtf + "']").addClass("active");
                    $(idtf).css({ "visibility": "visible", "z-index": "99", "position": "relative" });
                }
            });
        }
    });
})(jQuery);