﻿
(function ($) {
    $(function () {
        boxm($(window).width());
        if ($("#accordion").size() > 0) {
            $('#accordion').colpane({
                cssicon1: ".sup-icon",
                cssicon2: ".sup-icon-2",
                csscontent: ".box-sup",
                csstexticon: ".sup-icont"
            });
        }
        if ($("#scroller").size() > 0) {
            nysrolltext();
        }

        limitcol();
        //banner
        if ($('#banner').size() == 1) {
            var banner = $('#banner-slider').bxSlider({
                pager: false,
                auto: true
            });
            $(".bx-wrapper .bx-controls-direction a").click(function () {
                banner.startAuto();
                return false;
            });
        }
        if ($('#slidera').size() == 1) {
            var banners = $('#slidera').bxSlider({
                mode: 'fade',
                controls: false,
                pager: false,
                auto: true
            });
        }
        //
        $(".footer-dk a:first-child").addClass("first");
        //
        $("#serachtop").hover(function () {
            $(this).find(".search-text").stop().animate({ "width": 180 + "px", "right": 32 + "px" }, 300);
        }, function () {
            $(this).find(".search-text").stop().animate({ "width": 0 + "px", "right": 0 + "px" }, 300);
        });
        //
        $("#site a:first").addClass("first");
        //
        $('.text-box').expander({
            slicePoint: 700,
            widow: 2,
            expandSpeed: 0,
            expandText: 'Xem thêm',
            userCollapseText: 'Rút gọn'
        });
        $('.text-mobile').expander({
            slicePoint: 250,
            expandSpeed: 0,
            expandEffect: 'hide',
            collapseEffect: 'hide',
            expandText: 'Xem thêm',
            userCollapseText: 'Rút gọn'
        });
        $('.info-text').expander({
            slicePoint: 1000,
            widow: 2,
            expandSpeed: 0,
            expandText: 'Xem thêm',
            userCollapseText: 'Rút gọn'
        });
        //tabvisile
        $(".watch-flaughs").tabsvisite({
            idwrap: "#myTab",
            csstit: ".tit-tabs",
            booltext: false
        });
        $("#play-learn").tabsvisite({
            idwrap: "#tabsVideo",
            csstit: ".tit-video",
            csswraptext: ".comment-video",
            cssboxtext: ".commnet-wrap"
        });

        mytabs("desTab", "tit-des", "wrap-des");
        linkadd("#link-add", ".wrap-boxa", ".boxa", 5);
        colpase("accordion2");
        //
        $(".list-experience li:last-child").addClass("last");
        //
        if ($("#smallboxs").size() == 1) {
            $(".box-newsl .newsl-img img").remove();
            for (var i = 0; i < 3; i++) {
                var linname = $("#smallboxs .box-rss:eq(" + i + ")").find(".box-rss-name").find("a").attr("href");
                var linname2 = $("#smallboxs .box-rss:eq(" + i + ")").find(".box-rss-tit").find("a").attr("href");
                $(".box-newsl:eq(" + i + ")").find(".newsl-img").attr("href", linname).append($("#smallboxs .box-rss:eq(" + i + ")").find(".box-rss-img").find("img").addClass("img-responsive"));
                $(".box-newsl:eq(" + i + ")").find(".small-name").find("a").attr("href", linname).text($("#smallboxs .box-rss:eq(" + i + ")").find(".box-rss-name").find("a").text());
                $(".box-newsl:eq(" + i + ")").find(".news-smmall").find("a").attr("href", linname2).text($("#smallboxs .box-rss:eq(" + i + ")").find(".box-rss-tit").find("a").text());
            }
            $("#smallboxs").remove();
            //                myhd2(".know-box");

            $(".know-box").converbox({
                wrapbox: " .box-newsl",
                boolw: false
            });
            $(window).resize(function () {
                //                    myhd2(".know-box");
                $(".know-box").converbox({
                    wrapbox: " .box-newsl",
                    boolw: false
                });
            });
        }
        if ($("#listnews").size() == 1) {
            $(".newsb-wrap").each(function () {
                $(this).find(".newsb").find("img").remove();
                var linka = $(this).find(".box-rss-name").find("a").attr("href");
                $(this).find(".news-img").attr("href", linka).append($(this).find(".box-rss-img").find("a").find("img").addClass("img-responsive"));
                $(this).find(".description").text($(this).find(".box-rss-img").text());
                $(this).find(".news-smmall").find("a").attr("href", linka).text($(this).find(".box-rss-tit").find("a").text());
                $(this).find(".news-name").find("a").attr("href", $(this).find(".box-rss-name").find("a").attr("href")).text($(this).find(".box-rss-name").find("a").text());
                $(this).find(".box-rss").remove();
            });
        }

        if ($("#bigboxs").size() == 1) {
            $(".big-news .news-img img").remove();
            var linkbig = $("#bigboxs .big-rss-name a").attr("href");
            $(".big-news .news-img").attr("href", linkbig).append($("#bigboxs .big-rss-imgdes a").find("img").addClass("img-responsive"));
            $(".big-news .description").text($("#bigboxs .big-rss-imgdes").text());
            $(".big-news .date").text($("#bigboxs .big-rss-date").text());
            $(".big-news .news-name a").attr("href", linkbig).text($("#bigboxs .big-rss-name a").text());
            $("#bigboxs").remove();
        }

        if ($("#wnews").size() == 1) {
            var countnew = $("#wnews .news-ns").size();
            for (var i = 0; i < countnew; i++) {
                $("#wnews-detail").append('<div class="news-dbox"><a class="news-img" href="#"></a><div class="news-content"><h3 class="news-name"><a href="#"></a></h3><p class="date"></p><div class="description"></div></div></div>');

                $("#wnews-detail .news-dbox:eq(" + i + ")").find("img").remove();
                var linka = $("#wnews .news-ns:eq(" + i + ")").find(".box-rss-name").attr("href");
                $("#wnews-detail .news-dbox:eq(" + i + ")").find(".news-img").attr("href", linka).append($("#wnews .news-ns:eq(" + i + ")").find(".box-rss-img").find("a").find("img").addClass("img-responsive"));
                $("#wnews-detail .news-dbox:eq(" + i + ")").find(".description").text($("#wnews .news-ns:eq(" + i + ")").find(".box-rss-img").text());
                $("#wnews-detail .news-dbox:eq(" + i + ")").find(".date").text($("#wnews .news-ns:eq(" + i + ")").find(".date").text());
                $("#wnews-detail .news-dbox:eq(" + i + ")").find(".news-name").find("a").attr("href", linka).text($("#wnews .news-ns:eq(" + i + ")").find(".box-rss-name").text());

            }
            $("#wnews").remove();

            $("div.holder").jPages({
                containerID: "wnews-detail",
                perPage: 6,
                first: false,
                previous: false,
                next: false,
                last: false
            });
            $("#wnews-detail .news-dbox:nth-child(6n)").addClass("last");
        }
        //
        choiseup();

        //
        $("#menu .no-show").remove();
        $("#menu .m-hover").hover(function () {
            var wmenu = $("#menu").width();
            var pright = $(this).position().left;
            var countli = $(this).find(".menu-sub").find(".lia").size();
            $(this).find(".menu-sub").find(".menu-list").width(wmenu);
            $(this).find(".menu-sub").find(".menu-img").height(parseInt((wmenu / 4 - 40) * 0.65));
            if (countli == 5) {
                $(this).find(".menu-sub").css({ "left": "auto", "right": 0, "width": wmenu }).find(".lia").css({ "width": "20%" });
            } else if (countli == 4) {
                $(this).find(".menu-sub").css({ "left": "auto", "right": 0, "width": wmenu });
            } else if (countli == 3) {
                if ($(this).hasClass("first")) {
                    $(this).css({ "position": "relative" }).find(".menu-sub").css({ "left": 0, "width": (wmenu / 4) * 3 });
                } else {
                    $(this).css({ "position": "relative" }).find(".menu-sub").css({ "left": "-" + (((wmenu / 4) * 3) / 2) + "px", "width": (wmenu / 4) * 3 });
                }
            } else if (countli == 2) {
                var wi = (wmenu / 4) * 2;
                $(this).css({ "position": "relative" }).find(".menu-sub").css({ "left": "-" + (wi / 2 - 50) + "px", "width": (wmenu / 4) * 2 });
            } else {
                $(this).css({ "position": "relative" }).find(".menu-sub").css({ "left": 0, "width": (wmenu / 4) * 3 });
            }
            $(this).find(".menu-sub").stop(true, true).fadeIn();
        }, function () {
            $(this).find(".menu-sub").stop(true, true).fadeOut();
        });
        //
        $(".newsd .box-newsl:last").addClass("last");
        $(".news-list li:last-child").addClass("last");
        $(".list-rand li:last").addClass("last");
        $(".boxbar-list box-newsg:last").addClass("last");
        $(".detail-news .news-dbox:last").addClass("last");
        var az = ["A", "B", "C", "D", "E"];
        $("#game-list .list-radio .hidden").remove();
        $(".radio-check label .i-content").before('<strong class="i-name"></strong><span class="i-cham">.</span>');
        $(".list-radio").each(function () {
            var countques = $(this).find("input:radio").size();
            for (var i = 0; i < countques; i++) {
                $(this).find("li:eq(" + i + ")").find(".radio-check").find(".i-name").text(az[i]);
            }
        });
        $("#game-list .game-title").each(function (eindex) {
            $(this).find(".game-num").text("Câu " + (eindex + 1) + ":");
        });
        $(".list-radio .radio-check").find("input:radio").attr("checked", false);
        $(".list-radio .radio-check").click(function () {
            $(this).parents(".list-radio").find("li").removeClass("current");
            if ($(this).find("input:radio").is(":checked")) {
                $(this).parent("li").addClass("current");
            }
        });
        quealiw($(window).width());
        $(window).resize(function () {
            //                myhd2(".know-box");
            quealiw($(window).width());
        });
        //block
        myclock(5, 0);
        $("#startgame a").click(function () {
            $(this).hide();
            $(this).parent(".btn-starts").find("span").show();
            $("#wrapgame").css({ "visibility": "visible" });
            myclockup();
            return false;
        });
        $("#gameplay .more-gamesp a").click(function () {
            $("#gameplay .close").trigger("click");
        });
    });
    //=====windown
    $(window).load(function () {


    });
    //=====================//
    
    //function clock
    function myclock(mm, ss) {
        $("#mm").text(checkTime(mm));
        $("#ss").text(checkTime(ss));
        var showimg = true;
        if (showimg) {
            $("#clockBox").hide().after('<div id="dateimg"><span id="datemm"></span><span class="cham">:</span><span id="datess"></span></div>');
            myTimefr();
        }
    }
    function myclockup() {
        var myVar = setInterval(function () { myTimer() }, 1000);
        function myTimer() {
            var showimg = true;
            var mm = parseInt($("#mm").text());
            var ss = parseInt($("#ss").text());
            if (mm == 0 && ss == 0) {
                myStopFunction();
            } else {
                if (ss <= 0) {
                    ss = 59;
                    mm = mm - 1;
                } else {
                    ss = ss - 1;
                }
                $("#mm").text(checkTime(mm));
                $("#ss").text(checkTime(ss));
                if (showimg) {
                    $("#datemm").html(numchar(mm));
                    $("#datess").html(numchar(ss));
                }
                $("#time-ms .time-mm").val(mm);
                $("#time-ms .time-ss").val(ss);
            }
        }
        function myStopFunction() {
            clearInterval(myVar);
            $("#ques-btn .btn-link").remove();
            $("#ques-btn .result").show();
            $("#ques-btn .mask-ques").hide();
            $("#ques-btn .end-time").show();

            $("#time-ms .time-mm").val(parseInt($("#mm").text()));
            $("#time-ms .time-ss").val(parseInt($("#ss").text()));
            setTimeout(function () { $("#ques-btn .result").trigger("click") }, 3000);
        }
        $("#ques-btn .result").click(function () {
            clearInterval(myVar);
            //                        alert("aaa");
        });
    }
    function myTimefr() {
        var showimg = true;
        var mm = parseInt($("#mm").text());
        var ss = parseInt($("#ss").text());
        $("#mm").text(checkTime(mm));
        $("#ss").text(checkTime(ss));
        if (showimg) {
            $("#datemm").html(numchar(mm));
            $("#datess").html(numchar(ss));
        }
        $("#time-ms .time-mm").val(mm);
        $("#time-ms .time-ss").val(ss);
    }
    function checkTime(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
    function numchar(i) {
        if (i < 10) {
            i = "0" + i;
        }
        var a = [];
        var visitors = i.toString();
        var charlen = visitors.length;
        var html = "";
        for (var ki = 0; ki < charlen; ki++) {
            a[ki] = visitors.substr(ki, 1);
        }
        for (var k = 0; k < charlen; k++) {
            if (a[k] == "0") {
                html += "<img class='vam' src='assets/images/visitor/i0.png' />";
            } else if (a[k] == "1") {
                html += "<img class='vam' src='assets/images/visitor/i1.png' />";
            } else if (a[k] == "2") {
                html += "<img class='vam' src='assets/images/visitor/i2.png' />";
            } else if (a[k] == "3") {
                html += "<img class='vam' src='assets/images/visitor/i3.png' />";
            } else if (a[k] == "4") {
                html += "<img class='vam' src='assets/images/visitor/i4.png' />";
            } else if (a[k] == "5") {
                html += "<img class='vam' src='assets/images/visitor/i5.png' />";
            } else if (a[k] == "6") {
                html += "<img class='vam' src='assets/images/visitor/i6.png' />";
            } else if (a[k] == "7") {
                html += "<img class='vam' src='assets/images/visitor/i7.png' />";
            } else if (a[k] == "8") {
                html += "<img class='vam' src='assets/images/visitor/i8.png' />";
            } else if (a[k] == "9") {
                html += "<img class='vam' src='assets/images/visitor/i9.png' />";
            }
        }
        return html;
    }
    function quealiw(w) {
        if (w > 767) {
            $(".list-radio").find("li").css({ "width": "auto" });
            setTimeout(function () {
                $(".list-radio").each(function () {
                    var countques = $(this).find("input:radio").size();
                    var maxWidth = Math.max.apply(null, $("li", this).map(function () {
                        return $(this).width();
                    }).get());
                    if ((countques > 4) || (countques == 3)) {
                        if (maxWidth > 440) {
                            $(this).find("li").css({ "width": "100%" });
                        } else if (maxWidth > 320 && maxWidth < 441) {
                            $(this).find("li").css({ "width": "50%" });
                        } else {
                            $(this).find("li").css({ "width": "33.33%" });
                        }
                    } else {
                        if (maxWidth > 440) {
                            $(this).find("li").css({ "width": "100%" });
                        } else {
                            $(this).find("li").css({ "width": "50%" });
                        }
                    }
                });
            }, 100);
        } else {
            $(".list-radio").find("li").css({ "width": "100%" });
        }
    }
    //function ready
    //function

    function choiseup() {
        $(".wrap-radio .check-list input:radio").change(function () {
            var idid = $(this).val();
            choiseif(idid);
        });
        $(".wrap-radio .check-list td:first input:radio").trigger("click");
        $(".wrap-radio .check-list td:first input:radio").trigger("change");
    }
    function choiseif(idif) {
        if (idif == 1) {
            $(".wrap-uptext .upimg").hide();
            $(".wrap-uptext .upvideo").hide();
            $(".wrap-uptext .up-text").show();
        }
        else if (idif == 2) {
            $(".wrap-uptext .upimg").show();
            $(".wrap-uptext .upvideo").hide();
            $(".wrap-uptext .up-text").hide();
        }
        else if (idif == 3) {
            $(".wrap-uptext .upvideo").show();
            $(".wrap-uptext .upimg").hide();
            $(".wrap-uptext .up-text").hide();
        }

    }
    //
    function mytabs(idwrap, csstit, csscontent) {
        $('#' + idwrap + ' a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
            $("." + csstit + " a").removeClass("active");
            var idhref = $(this).attr("href");
            $("." + csstit + " a[href='" + idhref + "']").addClass("active");
            if ($(this).parents("#play-learn").size() == 1) {
                $(".comment-video").find(".commnet-wrap").hide();
                $(".comment-video").find(".commnet-wrap[data-id='" + idhref + "']").show();
                var topp = $("#commenttext").offset().top;
                $("html, body").animate({ scrollTop: topp }, 500);
            }
            return false;
        });
        $("." + csstit + " a").click(function () {
            $("." + csstit + " a").removeClass("active");
            $(this).addClass("active");
            var idhref = $(this).attr("href");
            $("#" + idwrap + " li a[href='" + idhref + "']").trigger("click");
            if ($(this).parents("#play-learn").size() == 1) {
                var topp = $("#commenttext").offset().top;
                $("html, body").animate({ scrollTop: topp }, 500);
            }
            return false;
        });
        $('#' + idwrap + ' li:first').addClass("active");
        $('.' + csscontent + ' .tab-pane:first').addClass("active");
        var idtf = $('#' + idwrap + ' li:first').find("a:first").attr("href");
        $("." + csstit + " a[href='" + idtf + "']").addClass("active");
        $(".comment-video").find(".commnet-wrap").hide();
        $(".comment-video").find(".commnet-wrap:first").show();
    }
    function mytabsa(idwrap) {
        $('#' + idwrap + ' a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
            return false;
        });
        $('#' + idwrap + ' li:first').find("a").trigger("click");
    }
    function myhd2(csswrap) {
        $(csswrap + " .box-newsl").height("auto");
        $(csswrap).each(function () {
            var maxHeight = Math.max.apply(null, $(".box-newsl", this).map(function () {
                return $(this).height();
            }).get());
            $(".box-newsl", this).height(maxHeight);
        });
    }
    function linkadd(idlink, wrapcsslink, csslink) {
        var addl = 2;
        var snum = addl + 1;
        $(wrapcsslink + " " + csslink + ":gt(" + addl + ")").addClass("vhidden");
        if ($(wrapcsslink).find(csslink).size() <= snum) {
            $(idlink).hide();
        }
        $(idlink).find("a").click(function () {
            var counttr = $(wrapcsslink).find(csslink).size() - 1;
            if (addl >= counttr) {
                addl = addl;
            } else {
                addl = addl + snum;
                $(wrapcsslink).find(csslink).removeClass("vhidden");
                $(wrapcsslink + " " + csslink + ":gt(" + addl + ")").addClass("vhidden");
            }
            if (addl >= counttr) {
                $(idlink).hide();
            }
            return false;
        });
    }
    function colpase(idname) {
        $('#' + idname + ' .panel-default:first').find(".panel-collapse").addClass("in");
        $("#" + idname + " .panel-heading:first").addClass("active");
        $("#" + idname + " .panel-heading:first").find(".panel-title").find("a:first").addClass("current").find(".tabs-icon").hide();
        $('#' + idname + ' [data-toggle]').on('click', function () {
            $("#" + idname + " .panel-heading").removeClass("active");
            $(this).parents(".panel-heading").addClass("active");
            $(this).toggleClass("current");
            var topp = 0;
            if ($(this).parents(".panel-heading").hasClass("active") && $(this).hasClass("current")) {
                $(this).parents(".panel-heading").addClass('active').find(".tabs-icon").hide();
                topp = $(this).parents(".panel-default").prev().offset().top;
            } else {
                $(this).parents(".panel-heading").find(".tabs-icon").show();
                topp = $(this).parents(".panel-default").prev().offset().top;
            }

            $("#" + idname + " .panel-heading").each(function () {
                if ($(this).hasClass("active") && $(this).find(".panel-title").find("a:first").hasClass("current")) {
                    $(this).find(".tabs-icon").hide();
                } else {
                    $(this).removeClass("active").find(".panel-title").find("a:first").removeClass("current").find(".tabs-icon").show();
                }
            });
            $("html, body").animate({ scrollTop: topp }, 500);
        });
    }
    //function load windwon
    
    function textinimg(cssname) {
        $(cssname).each(function () {
            var hbtext = $(this).height();
            $(this).css({ "margin-top": "-" + (hbtext / 2) + "px" });
        });
    }
    function boxm(w) {
        $(".news-main").find(".boxh").height("auto");
        var hmain = $(".news-main").height();
        if (w > 970) {
            $(".news-main").find(".boxh").height(hmain);
        } else {
            $(".news-main").find(".boxh").height("auto");
        }
    }

    function boxnewsd(csswrap, w) {
        $(csswrap + " .newsb-wrap").css({ "height": "auto" });

        setTimeout(function () {
            $(csswrap).each(function () {
                var maxHeight = Math.max.apply(null, $(".newsb-wrap", this).map(function () {
                    return $(this).height();
                }).get());
                if (w > 480) {
                    $(".newsb-wrap", this).height(maxHeight);
                } else {
                    $(".newsb-wrap", this).height("auto");
                }
            });
        }, 100);
    }
    function nysrolltext() {
        var wss = 0;
        $("#scroller li").each(function () {
            var wlils = $(this).find(".text-sroll").find(".text-srolls").width() + 50;
            wss = wss + wlils;
            $(this).width(wlils);
        });
        if (wss < 981) {
            $("#scroller li").clone().prependTo($("#scroller"));
        }
    }
    function limitcol() {
        limitfun($(window).width());
        $(window).resize(function () {
            setTimeout(function () {
                limitfun($(window).width());
            }, 200);
        });
    }
    function limitfun(w) {
        $(".wrap-video .wrap-content .box-v").removeClass("hidden-visible");
        if (w > 980) {
            $(".wrap-video .wrap-content .box-v").removeClass("hidden-visible");
        } else if (w > 470 & w < 980) {
            $(".wrap-video .wrap-content").each(function () {
                $(this).find(".box-v:gt(3)").addClass("hidden-visible");
            });
        } else {
            $(".wrap-video .wrap-content").each(function () {
                $(this).find(".box-v:gt(2)").addClass("hidden-visible");
            });
        }
    }
    //
    
    //=============================
   


    (function ($) {
        $(function () {
            $("#scroller").simplyScroll();
        });
    })(jQuery);
    $('#menu a.noclick').click(function (event) {
        return false;
    });
})(jQuery);