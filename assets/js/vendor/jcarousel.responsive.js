(function ($) {
    $(function () {
        $(".watch-flaughs .jcarousel").each(function () {
            var jcarousels = $(this);
            myjareponsive(jcarousels);
        });
        function myjareponsive(jcarousel) {
            jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var width = jcarousel.parents(".watch-flaughs").width();
                if (width >= 780) {
                    width = (width - 60) / 3;
                    jcarousel.parents(".wrap-content").width(width * 3 - 20);
                } else if (width >= 500 && width < 780) {
                    width = (width - 60) / 2;
                    jcarousel.parents(".wrap-content").width(width * 2 - 20);
                } else {
                    width = width - 2;
                    jcarousel.parents(".wrap-content").width(width);
                }

                jcarousel.jcarousel('items').css('width', width + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

            jcarousel.parents(".jcarousel-wrapper").find('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

            jcarousel.parents(".jcarousel-wrapper").find('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

            jcarousel.parents(".jcarousel-wrapper").find('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function () {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function () {
                $(this).removeClass('active');
            })
            .on('click', function (e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function (page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
        }
    });
})(jQuery);
