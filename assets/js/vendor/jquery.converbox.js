﻿(function ($) {
    $.fn.extend({

        converbox: function (options) {

            // Đặt các giá trị mặc định
            var defaults = {
                resizewin: true,
                wrapbool: true,
                wrapbox: "",
                boolw: false,
                hnum: 0.65,
                boximg: "",
                cssclass1: "",
                cssclass2: "",
                cssclass3: "",
                wtrue: false,
                widwin: 480,
                textmid: ""
            };

            var options = $.extend(defaults, options);

            return this.each(function () {
                var opts = options;

                // Đặt tên biến cho element (ul)
                var obj = $(this);

                myconverbox($(window).width());
                if (opts.resizewin) {
                    myresize();
                }
                function myresize() {
                    $(window).resize(function () {
                        myconverbox($(window).width());
                    });
                }
                function myconverbox(ww) {
                    obj.find(opts.wrapbox).animate({ "opacity": 0 }, 0);
                    obj.find(opts.wrapbox).css({ "height": "auto" });
                    obj.find(opts.cssclass1).css({ "height": "auto" });
                    obj.find(opts.cssclass2).css({ "height": "auto" });
                    obj.find(opts.cssclass3).css({ "height": "auto" });
                    obj.find(opts.boximg).css({ "height": "auto" });
                    setTimeout(function () {
                        obj.each(function () {
                            if (opts.wtrue) {
                                if (ww > opts.widwin) {
                                    if (opts.cssclass1 != null || opts.cssclass1 != "") {
                                        var maxHeight = Math.max.apply(null, $(opts.cssclass1, this).map(function () {
                                            return $(this).height();
                                        }).get());
                                        $(opts.cssclass1, this).height(maxHeight);
                                    }
                                    if (opts.cssclass2 != null || opts.cssclass2 != "") {
                                        var maxHeightcss2 = Math.max.apply(null, $(opts.cssclass2, this).map(function () {
                                            return $(this).height();
                                        }).get());
                                        $(opts.cssclass2, this).height(maxHeightcss2);
                                    }
                                    if (opts.cssclass3 != null || opts.cssclass3 != "") {
                                        var maxHeightcss3 = Math.max.apply(null, $(opts.cssclass3, this).map(function () {
                                            return $(this).height();
                                        }).get());
                                        $(opts.cssclass3, this).height(maxHeightcss3);
                                    }
                                } else {
                                    obj.find(opts.cssclass1).css({ "height": "auto" });
                                    obj.find(opts.cssclass2).css({ "height": "auto" });
                                    obj.find(opts.cssclass3).css({ "height": "auto" });
                                }
                            } else {
                                if (opts.cssclass1 != null || opts.cssclass1 != "") {
                                    var maxHeight = Math.max.apply(null, $(opts.cssclass1, this).map(function () {
                                        return $(this).height();
                                    }).get());
                                    $(opts.cssclass1, this).height(maxHeight);
                                }
                                if (opts.cssclass2 != null || opts.cssclass2 != "") {
                                    var maxHeightcss2 = Math.max.apply(null, $(opts.cssclass2, this).map(function () {
                                        return $(this).height();
                                    }).get());
                                    $(opts.cssclass2, this).height(maxHeightcss2);
                                }
                                if (opts.cssclass3 != null || opts.cssclass3 != "") {
                                    var maxHeightcss3 = Math.max.apply(null, $(opts.cssclass3, this).map(function () {
                                        return $(this).height();
                                    }).get());
                                    $(opts.cssclass3, this).height(maxHeightcss3);
                                }
                            }
                            var maxHeight2 = Math.max.apply(null, $(opts.boximg, this).map(function () {
                                return $(this).height();
                            }).get());
                            var wimg = 0;
                            var hmid = 0;
                            if (opts.boolw) {
                                wimg = Math.max.apply(null, $(opts.boximg, this).map(function () {
                                    return $(this).width();
                                }).get());
                                hmid = parseInt(wimg * opts.hnum);
                                $(opts.boximg, this).height(hmid);
                            } else {
                                var maxHeight3 = Math.max.apply(null, $(opts.boximg, this).map(function () {
                                    return $(this).height();
                                }).get());
                                $(opts.boximg, this).height(maxHeight3);
                            }
                            if (opts.wrapbool) {
                                var maxHeightbox = Math.max.apply(null, $(opts.wrapbox, this).map(function () {
                                    return $(this).height();
                                }).get());
                                $(opts.wrapbox, this).height(maxHeightbox);
                            }
                            if (opts.textmid != null || opts.textmid != "") {
                                $(this).find(opts.textmid).each(function () {
                                    $this = $(this);
                                    var htext = $(this).find("strong").height();
                                    $(this).find("strong").css({ "margin-top": (hmid - htext) / 2 });
                                });
                            }
                        });
                        obj.find(opts.wrapbox).animate({ "opacity": 1 }, 100);
                    }, 400);
                }
            });
        }
    });
})(jQuery);
//
(function ($) {
    $.fn.extend({

        tabsvisite: function (options) {

            // Đặt các giá trị mặc định
            var defaults = {
                idwrap: "",
                csstit: "",
                booltext: true,
                csswraptext: "",
                cssboxtext: ""
            };

            var options = $.extend(defaults, options);

            return this.each(function () {
                var opts = options;

                // Đặt tên biến cho element (ul)
                var obj = $(this);
                mytabsvisite();
                function mytabsvisite() {
                    var idcontrol = $(opts.idwrap, obj);
                    var wrapcontent = $(opts.csscontent, obj);
                    var csswraptext = $(opts.csswraptext, obj);
                    var csstit = $(opts.csstit, obj);

                    obj.find(".tab-panes").css({ "visibility": "hidden", "z-index": "-1", "position": "absolute" }).find(".like-box").css({"position": "fixed", "left" : "-9999em", "top" : "-9999em"});
                    idcontrol.find('a').click(function (e) {
                        e.preventDefault();
                        idcontrol.find('li').removeClass("active");
                        $(this).parent('li').addClass("active");
                        var idhref = $(this).attr("href");
                        obj.find(".tab-panes").css({ "visibility": "hidden", "z-index": "-1", "position": "absolute" }).find(".like-box").css({"position": "fixed", "left" : "-9999em", "top" : "-9999em"});
                        $(idhref).css({ "visibility": "visible", "z-index": "99", "position": "relative" }).find(".like-box").css({"position": "relative", "left" : 0, "top" : 0});
                        csstit.find("a").removeClass("active");
                        csstit.find("a[href='" + idhref + "']").addClass("active");
                        if (opts.booltext) {
                            obj.find(opts.cssboxtext).hide();
                            obj.find(opts.cssboxtext + "[data-id='" + idhref + "']").show();
                            var topp = csswraptext.offset().top;
                            $("html, body").animate({ scrollTop: topp }, 500);
                        }
                        return false;
                    });
                    csstit.find("a").click(function () {
                        csstit.find("a").removeClass("active");
                        $(this).addClass("active");
                        var idhref = $(this).attr("href");
                        idcontrol.find("a[href='" + idhref + "']").trigger("click");
                        if ($(this).parents("#play-learn").size() == 1) {
                            var topp = csswraptext.offset().top;
                            $("html, body").animate({ scrollTop: topp }, 500);
                        }
                        return false;
                    });
                    idcontrol.find('li:first').addClass("active");
//                    $(opts.csscontent + ' .tab-panes:first-child').css({ "visibility": "visible", "z-index": "99", "position": "relative" }).css({"position": "relative", "left" : 0, "top" : 0});
                    var idtf = idcontrol.find('li:first').find("a:first").attr("href");
                    obj.find(opts.cssti).find("a[href='" + idtf + "']").addClass("active");
                    $(idtf).css({ "visibility": "visible", "z-index": "99", "position": "relative" }).find(".like-box").css({"position": "relative", "left" : 0, "top" : 0});
                }
            });
        }
    });
})(jQuery);
//
(function ($) {
    $.fn.extend({

        colpane: function (options) {

            // Đặt các giá trị mặc định
            var defaults = {
                cssicon1: "",
                cssicon2: "",
                csscontent: "",
                csstexticon: ""
            };

            var options = $.extend(defaults, options);

            return this.each(function () {
                var opts = options;

                // Đặt tên biến cho element (ul)
                var obj = $(this);
                obj.animate({ "opacity": 0 }, 0);
                setTimeout(function () {
                    mycolpane();
                    myhtit();
                    obj.animate({ "opacity": 1 }, 100);
                }, 100);
                $(window).resize(function () {
                    myhtit();
                });
                function mycolpane() {
                    obj.find('[data-toggle]').on('click', function () {
                        obj.find(".panel-heading").removeClass("active");
                        $(this).parents(".panel-heading").addClass("active");
                        $(this).toggleClass("current");
                        var topp = 0;
                        if ($(this).parents(".panel-heading").hasClass("active") && $(this).hasClass("current")) {
                            $(this).parents(".panel-heading").addClass('active').find(opts.cssicon1).find("strong").html("tắt <br/> trả lời")
                        .parents(".panel-heading").find(opts.cssicon2).find("strong").html("tắt trả lời");
                            topp = $(this).find(opts.cssicon2).offset().top;
                        } else {
                            $(this).parents(".panel-heading").find(opts.cssicon1).find("strong").html("xem <br/> trả lời")
                        .parents(".panel-heading").find(opts.cssicon2).find("strong").html("xem trả lời");
                            topp = $(this).offset().top;
                        }

                        obj.find(".panel-heading").each(function () {
                            if ($(this).hasClass("active") && $(this).find(".panel-title").find("a:first").hasClass("current")) {
                                $(this).find(opts.cssicon1).find("strong").html("tắt <br/> trả lời")
                                .parents(".panel-heading").find(opts.cssicon2).find("strong").html("tắt trả lời");
                            } else {
                                $(this).removeClass("active").find(".panel-title").find("a:first").removeClass("current").find(opts.cssicon1).find("strong").html("xem <br/> trả lời")
                            .parents(".panel-heading").find(opts.cssicon2).find("strong").html("xem trả lời");
                            }
                        });
                        $("html, body").animate({ scrollTop: topp }, 500);
                    });
                }
                function myhtit() {
                    obj.find(".panel-title").find(opts.csscontent).height("auto");
                    obj.find(".panel-title").each(function () {
                        var hg = $(this).find(opts.csscontent).outerHeight();
                        var htext = $(this).find(opts.csstexticon).find("strong").outerHeight();
                        $(this).find(opts.csstexticon).height(hg);
                        $(this).find(opts.csstexticon).find("strong").css({ "margin-top": (hg - htext) / 2 });
                    });
                }
            });
        }
    });
})(jQuery);