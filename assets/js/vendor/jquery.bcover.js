﻿(function ($) {
    $.fn.extend({
        bcover: function () {
            return this.each(function () {
                // Đặt tên biến cho element
                var obj = $(this);
                // Lấy tất cả thẻ img trong css
//                obj.css({ "background-position": "center center", "background-repeat": "no-repeat", "background-size": "cover" });
                var items = $("img", obj);
                var srcimg = items.attr("src");
                items.animate({ "opacity": 0 }, 10);
                obj.css({ "background-image": "url(" + srcimg + ")" });
                if (jQuery.browser.msie && parseInt(jQuery.browser.version) < 9) {
                    var textie = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + srcimg + "',sizingMethod='scale')";
                    obj.css({ "filter" : textie });
                    obj.css({ "-ms-filter" : textie });
                }
            });
        }
    });
})(jQuery);