﻿(function ($) {
    $.fn.extend({

        colpane: function (options) {

            // Đặt các giá trị mặc định
            var defaults = {
                cssicon1: "",
                cssicon2: "",
                csscontent: "",
                csstexticon: ""
            };

            var options = $.extend(defaults, options);

            return this.each(function () {
                var opts = options;

                // Đặt tên biến cho element (ul)
                var obj = $(this);
                obj.animate({ "opacity": 0 }, 0);
                setTimeout(function () {
                    mycolpane();
                    myhtit();
                    obj.animate({ "opacity": 1 }, 100);
                }, 100);
                $(window).resize(function () {
                    myhtit();
                });
                function mycolpane() {
                    obj.find('[data-toggle]').on('click', function () {
                        obj.find(".panel-heading").removeClass("active");
                        $(this).parents(".panel-heading").addClass("active");
                        $(this).toggleClass("current");
                        var topp = 0;
                        if ($(this).parents(".panel-heading").hasClass("active") && $(this).hasClass("current")) {
                            $(this).parents(".panel-heading").addClass('active').find(opts.cssicon1).find("strong").html("tắt <br/> trả lời")
                        .parents(".panel-heading").find(opts.cssicon2).find("strong").html("tắt trả lời");
                            topp = $(this).find(opts.cssicon2).offset().top;
                        } else {
                            $(this).parents(".panel-heading").find(opts.cssicon1).find("strong").html("xem <br/> trả lời")
                        .parents(".panel-heading").find(opts.cssicon2).find("strong").html("xem trả lời");
                            topp = $(this).offset().top;
                        }

                        obj.find(".panel-heading").each(function () {
                            if ($(this).hasClass("active") && $(this).find(".panel-title").find("a:first").hasClass("current")) {
                                $(this).find(opts.cssicon1).find("strong").html("tắt <br/> trả lời")
                                .parents(".panel-heading").find(opts.cssicon2).find("strong").html("tắt trả lời");
                            } else {
                                $(this).removeClass("active").find(".panel-title").find("a:first").removeClass("current").find(opts.cssicon1).find("strong").html("xem <br/> trả lời")
                            .parents(".panel-heading").find(opts.cssicon2).find("strong").html("xem trả lời");
                            }
                        });
                        $("html, body").animate({ scrollTop: topp }, 500);
                    });
                }
                function myhtit() {
                    obj.find(".panel-title").find(opts.csscontent).height("auto");
                    obj.find(".panel-title").each(function () {
                        var hg = $(this).find(opts.csscontent).outerHeight();
                        var htext = $(this).find(opts.csstexticon).find("strong").outerHeight();
                        $(this).find(opts.csstexticon).height(hg);
                        $(this).find(opts.csstexticon).find("strong").css({ "margin-top": (hg - htext) / 2 });
                    });
                }
            });
        }
    });
})(jQuery);