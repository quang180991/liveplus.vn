﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site-sub.master" AutoEventWireup="true" CodeFile="hoc-de-lam.aspx.cs" Inherits="hoc_de_lam" %>

<%@ Register Src="~/uc/successDN.ascx" TagPrefix="uc1" TagName="successDN" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Giới thiệu dự án GOsmart</title>
    <meta name="Description" content="Giới thiệu dự án GOsmart" />
    <style type="text/css">
        .auto-style1
        {
            color: #FF3300;
        }
        .auto-style2
        {
            color: #33CC33;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="site">
        <a href="Default.aspx#play-learn">Học để làm</a><span>Giới thiệu dự án GOsmart</span>
    </div>
    <h1 class="title">Giới thiệu dự án <span class="auto-style1">G</span><span class="auto-style2">O</span>smart</h1>
    <div class="box-like">
        <div class="box-like-b">
          <div class="fb-like" data-href='<%= Page.Request.Url.AbsolutePath %>'
                            data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                        </div>
        </div>
    </div>
    <div class="panel-group tabs-group" id="accordion2">

        <asp:ListView ID="ListView1" runat="server"
            DataSourceID="ObjectDataSource1"
            EnableModelValidation="True">
            <ItemTemplate>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion2" href='#collapse<%# Eval("GoProjectID") %>'>
                                <span class="tabs-name"><strong>&bull;</strong><%# Eval("GoProjectTitle") %></span><span class="tabs-icon"></span></a></h4>
                    </div>
                    <div id='collapse<%# Eval("GoProjectID") %>' class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="info-text">
                                <%# Eval("Content") %>
                                <%--   <div style="margin-bottom: 10px;">
                                    <img src="assets/images/img-m-1.jpg" alt="" />
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <p>
                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                                    <br />
                                    Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt
                                </p>
                                <p>
                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                                    <br />
                                    Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt
                                </p>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
            <LayoutTemplate>
                <span runat="server" id="itemPlaceholder" />
            </LayoutTemplate>
        </asp:ListView>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
            SelectMethod="GoProjectSelectAll"
            TypeName="TLLib.GoProject">
            <SelectParameters>
                <asp:Parameter Name="StartRowIndex" Type="String" />
                <asp:Parameter Name="EndRowIndex" Type="String" />
                <asp:Parameter Name="Keyword" Type="String" />
                <asp:Parameter Name="GoProjectTitle" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter Name="GoProjectCategoryID" Type="String" />
                <asp:Parameter Name="Tag" Type="String" />
                <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                <asp:Parameter Name="IsHot" Type="String" />
                <asp:Parameter Name="IsNew" Type="String" />
                <asp:Parameter Name="FromDate" Type="String" />
                <asp:Parameter Name="ToDate" Type="String" />
                <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                <asp:Parameter Name="Priority" Type="String" />
                <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>

        <div class="pager">

            <asp:DataPager ID="DataPager1" runat="server" PageSize="6" PagedControlID="ListView1">
                <Fields>
                    <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="numer-paging" CurrentPageLabelCssClass="current" />

                </Fields>
            </asp:DataPager>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphSibar" runat="Server">
    <div class="box-res">
        <h2><strong>đăng ký tham gia dự án</strong>(dành cho doanh nghiệp)</h2>
        <a class="link-popup" href="javascript:void(0);" data-toggle="modal" data-target="#popupp"></a>
    </div>
    <div class="box-wp">
        <h2 class="title-b">TÌM HIỂU VỀ DỰ ÁN VÀ CÁC CHƯƠNG TRÌNH</h2>
        <div class="studybox">
            <div class="row">
                <div class="col-xs-6 col-md-12">
                    <div class="box-ques ma">
                        <a href="hoc-de-lam-tin-tuc.aspx" class="box-aques">
                            <span class="box-img">
                                <img class="img-responsive" src="assets/images/site-img-1.jpg" alt="" /></span>
                            <span class="box-name">Tin tức</span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-md-12">
                    <div class="box-ques md">
                        <a href="hoc-de-lam.aspx" class="box-aques">
                            <span class="box-img">
                                <img class="img-responsive" src="assets/images/site-img-2.jpg" alt="" /></span>
                            <span class="box-name">Giới thiệu dự án GOsmart</span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-md-12">
                    <div class="box-ques mv">
                        <a href="hoc-de-lam-ISO.aspx" class="box-aques">
                            <span class="box-img">
                                <img class="img-responsive" src="assets/images/site-img-3.jpg" alt="" /></span>
                            <span class="box-name">ISO 39001</span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-md-12">
                    <div class="box-ques mx">
                        <a href="hoc-de-lam-tu-van.aspx" class="box-aques">
                            <span class="box-img">
                                <img class="img-responsive" src="assets/images/site-img-4.jpg" alt="" /></span>
                            <span class="box-name">Tư vấn hệ thống quản lý ATGT</span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-md-12">
                    <div class="box-ques ml">
                        <a href="hoc-de-lam-dao-tao-doanh-nghiep.aspx" class="box-aques">
                            <span class="box-img">
                                <img class="img-responsive" src="assets/images/site-img-5.jpg" alt="" /></span>
                            <span class="box-name">Đào tạo ATGT cho doanh nghiệp</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphPopup" runat="Server">

    <!-- Modal -->
    <div class="modal fade popup2" id="popupp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg contest-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h2 class="modal-title" id="myModalLabel">đăng kí thông tin doanh nghiệp</h2>
                </div>
                <div class="modal-body">
                    <fieldset class="send-wrap">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tên công ty</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtFullName" CssClass="form-control" placeholder="-- Tên công ty --" runat="server" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator7" runat="server"
                                    Display="Dynamic" ValidationGroup="InformationHDL" ControlToValidate="txtFullName"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Địa chỉ</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtAddress" CssClass="form-control" placeholder="-- Địa chỉ --" runat="server" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator5" runat="server"
                                    Display="Dynamic" ValidationGroup="InformationHDL" ControlToValidate="txtAddress"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Lĩnh vực hoạt động</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtLinhVuc" CssClass="form-control" placeholder="-- Lĩnh vực hoạt động --" runat="server" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator6" runat="server"
                                    Display="Dynamic" ValidationGroup="InformationHDL" ControlToValidate="txtLinhVuc"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tên người liên hệ</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtName" CssClass="form-control" placeholder="-- Nhập họ tên --" runat="server" Text=""></asp:TextBox>
                                        <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator2" runat="server"
                                            Display="Dynamic" ValidationGroup="InformationHDL" ControlToValidate="txtName"
                                            ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-offset-1 col-sm-4 control-label">Chức vụ</label>
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtChucVu" CssClass="form-control" placeholder="-- Chức vụ --" runat="server" Text=""></asp:TextBox>
                                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator3" runat="server"
                                                    Display="Dynamic" ValidationGroup="InformationHDL" ControlToValidate="txtChucVu"
                                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" Text="" placeholder="-- Nhập email --"></asp:TextBox>
                                <asp:RegularExpressionValidator CssClass="lb-error" ID="RegularExpressionValidator1"
                                    runat="server" ValidationGroup="InformationHDL" ControlToValidate="txtEmail" ErrorMessage="Sai định dạng email!"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"
                                    ForeColor="Red"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator1" runat="server"
                                    ValidationGroup="InformationHDL" ControlToValidate="txtEmail" ErrorMessage="Thông tin bắt buộc!"
                                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Điện thoại</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtPhone" CssClass="form-control" runat="server" Text="" placeholder="-- Nhập số điện thoại --"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator4" runat="server"
                                    Display="Dynamic" ValidationGroup="InformationHDL" ControlToValidate="txtPhone"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator CssClass="lb-error" ID="RegularExpressionValidator2"
                                    runat="server" ValidationGroup="InformationHDL" ControlToValidate="txtPhone" ErrorMessage="Vui lòng nhập số!"
                                    ValidationExpression="^[0-9]+$" Display="Dynamic"
                                    ForeColor="Red"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <div class="wrap-btn">
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <asp:Button ID="btnUpLoad" CssClass="btn-send" OnClick="btnUpLoad_Click" ValidationGroup="InformationHDL"  runat="server" Text="đăng ký thông tin" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <a href="javascript:void(0);" class="show-popup-3" data-toggle="modal" data-target="#loginDN"></a>
        <div class="modal fade popupbox" id="loginDN" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <uc1:successDN runat="server" ID="successDN" />
                </div>
            </div>
        </div>
</asp:Content>
