﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site-sub2.master" AutoEventWireup="true" CodeFile="tim-se-thay-chi-tiet-thong-diep.aspx.cs" Inherits="choi_de_hieu_chi_tiet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
 <script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share"
                                                    type="text/javascript"></script>
     <meta property="og:type" content="activity" />
    <%--đường link muốn share--%>
    <meta property="og:url" content='<%=Request.Url.Scheme + "://" + Page.Request.Url.Host.ToString()+ "/"+ Request.Url.AbsolutePath.Substring(Request.Url.AbsolutePath.LastIndexOf("/") + 1) %>' />
    <%--tiêu đề muốn share--%>
    <meta property="og:title" content='<%= hdnTitle.Value %>' />
    <%--hình ảnh muốn share--%>
    <%--mô tả muốn share--%>
    <meta property="og:description" content='<%= hdnDescription.Value %>' />
      <meta name="thumbnail" content='<%= Request.Url.Scheme + "://" +((hdnCID.Value == ""? "1" :hdnCID.Value.ToString())=="1"?Page.Request.Url.Host.ToString() + "/res/learns/quote/":"") + hdnImageName.Value  %>' />
    <meta property="og:image" content='<%= Request.Url.Scheme + "://" +((hdnCID.Value == ""? "1" :hdnCID.Value.ToString())=="1"?Page.Request.Url.Host.ToString() + "/res/learns/quote/":"") + hdnImageName.Value  %>' />
   <%--tên website muốn share--%>
    <meta property="og:site_name" content='<%= Request.Url.Scheme + "://" +Page.Request.Url.Host.ToString()%>' />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server"> 
     <asp:HiddenField ID="hdnImageName" runat="server" />
    <asp:HiddenField ID="hdnTitle" runat="server" />
    <asp:HiddenField ID="hdnDescription" runat="server" />
    <asp:HiddenField ID="hdnCID" runat="server" />
    <div class="wrap-section-b">
        <asp:ListView ID="ListView1" runat="server"
            DataSourceID="ObjectDataSource1"
            EnableModelValidation="True">
            <ItemTemplate>
                <div id="site">
                    <a href="Default.aspx#play-learn"><%# Eval("InGallery").ToString() =="True" ?"Tìm sẽ thấy" : "Chơi để hiểu" %></a><span><%# Eval("Title") %></span>
                </div>
                <h1 class="title-6"><span style="background: none;"><%# Eval("Title") %></span></h1>
                <div class="head-b line">
                    <span class="head-name">Tác giả: <strong><%# Eval("Name") %></strong></span>
                    <%--<span class="head-name">NGUỒN: <strong>Theo MASK Online</strong></span>--%>
                  <div class="like-boxb btn-baotrung">
                      <p class="more-a "> <a href="javascript:void(0);" class="btrung" data-toggle="modal" data-target="#popupbaotrung">Báo trùng</a></p>
                         <%--<div class="fb-like" data-href='<%# Page.Request.Url.Host.ToString() + ""+ progressTitle(Eval("Title")) + "-pqd-" + Eval("LearnsID") +".aspx"  %>'
                            data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                        </div>--%>

                    </div>
                </div>
                <div class="head-b"><span class="views">Lượt xem: <strong><%# Eval("Views") %></strong></span></div>
                <div class="head-b"><span class="views">Lượt bình chọn: <strong><%# Eval("Voted") %></strong></span></div>

                <div id="bchondehieu" class="wrap-text-2">
                    <div class="boxm-b">
                        <div class="description"><%# Eval("Description") %></div>
                        <div class="boxm-img">
                            <img id="Img2" alt='<%# Eval("ImagePath") %>' src='<%# string.IsNullOrEmpty( Eval("ImagePath").ToString())?"assets/images/logo.png":"~/res/learns/quote/" + Eval("ImagePath") %>'
                                runat="server" class="img-responsive" />
                            <div class="boxm-text">
                                <div class="in-text"><%# Eval("LearnsQuote") %></div>
                            </div>
                            <div class="boxm-mask"></div>
                        </div>
                    </div>

                </div>
                <div class="wrap-comment">
                    <h2 class="order-title">Bình luận</h2>
                    <div class="fb-comments" data-href='<%=  Page.Request.Url.Host.ToString()+ Page.Request.Url.AbsolutePath.ToString()%>' data-width="890"></div>
                </div>
            </ItemTemplate>
            <LayoutTemplate>
                <span runat="server" id="itemPlaceholder" />
            </LayoutTemplate>
        </asp:ListView>
        <asp:HiddenField ID="hdnGallery" runat="server" />
       <asp:HiddenField ID="hdndateold" runat="server" />
       <div id="btnhondehieu" class="btn-baotrung">
            <p class="more-a1"><a name="fb_share" type="button_count" expr:share_url="data:post.url" share_url='<%= Page.Request.Url.AbsolutePath%>'>Chia Sẻ</a></p>
            <p class="more-b <%= hdndateold.Value.ToString()=="0" ? "hidden":""  %> <%= hdnGallery.Value =="True" ?"hidden" : "" %> <%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ?"hidden" : "" %>">
                <a href="javascript:void(0);" data-toggle="modal" data-target='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ?"" : "#registerloginBC" %>'><span>Bình chọn</span></a>
            </p>
            <p class="more-b  <%= hdndateold.Value.ToString()=="0" ? "hidden":""  %> <%= hdnGallery.Value =="True" ?"hidden" : "" %> <%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ?"" : "hidden" %>">
                <asp:LinkButton ID="btnVote" OnClick="btnVote_Click"
                    ToolTip="Bình chọn" runat="server"><span>Bình chọn</span></asp:LinkButton>
            </p>  
              <asp:HiddenField ID="hdnBT" runat="server" /> 
            <p class="more-b1 <%= hdndateold.Value.ToString()=="0" ? "hidden":""  %> <%= hdnGallery.Value =="True" ?"hidden" : "" %> <%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ?"" : "hidden" %>  <%= hdnBT.Value.ToString()=="1" ? "":"hidden"  %>">
                <a href="javascript:void(0);" ><span>Bạn đã bình chọn cho tác phẩm này</span></a>
            </p>
        </div>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
            SelectMethod="LearnsSelectOne"
            TypeName="TLLib.Learns">
            <SelectParameters>
                <asp:QueryStringParameter Name="LearnsID" QueryStringField="pqd" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphSibar" runat="Server">
    <asp:ListView ID="ListView2" runat="server"
        DataSourceID="ObjectDataSource2"
        EnableModelValidation="True">
        <ItemTemplate>
            <div class="box-adv">
                <a href='<%# Eval("Website") %> '>
                    <img class="img-responsive" id="Img1" alt='<%# Eval("FileName") %>' src='<%# "http://www.liveplus.vn/res/advertisement/" + Eval("FileName") %>' visible='<%# string.IsNullOrEmpty( Eval("FileName").ToString()) ? false : 
true %>'
                        runat="server" />
                </a>

            </div>
        </ItemTemplate>
        <LayoutTemplate>
            <span runat="server" id="itemPlaceholder" />
        </LayoutTemplate>
    </asp:ListView>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="AdsBannerSelectAll"
        TypeName="TLLib.AdsBanner">
        <SelectParameters>
            <asp:Parameter Name="StartRowIndex" Type="String" DefaultValue="1" />
            <asp:Parameter Name="EndRowIndex" Type="String" DefaultValue="2" />
            <asp:Parameter Name="AdsCategoryID" Type="String" DefaultValue="5" />
            <asp:Parameter Name="CompanyName" Type="String" />
            <asp:Parameter Name="Website" Type="String" />
            <asp:Parameter Name="FromDate" Type="String" />
            <asp:Parameter Name="ToDate" Type="String" />
            <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
            <asp:Parameter Name="Priority" Type="String" />
            <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <div class="box-list">
        <h2 class="title-d">Các tác phẩm khác</h2>
        <div id="boxab" class="box-ab">
            <div id="abfirst">
                <asp:ListView ID="ListView3" runat="server"
                    DataSourceID="ObjectDataSource3"
                    EnableModelValidation="True">
                    <ItemTemplate>
                        <div class="box-art">
                            <a href='<%# progressTitle(Eval("Title")) +"-s"+ "-pqd-" + Eval("LearnsID") +".aspx" %>' class="art-img bcover">
                                <img id="Img2" alt='<%# Eval("ImagePath") %>' src='<%#string.IsNullOrEmpty( Eval("ImagePath").ToString())?"assets/images/logo.png": "http://www.liveplus.vn/res/learns/quote/" + Eval("ImagePath") %>'
                                    runat="server" class="img-responsive" />
                                <span class="text-art"><strong><%# TLLib.Common.SplitSummary(Eval("LearnsQuote").ToString(),20) %></strong></span>
                            </a>
                            <div class="art-content">
                                <h3 class="art-name"><a href='<%# progressTitle(Eval("Title"))+"-s" + "-pqd-" + Eval("LearnsID") +".aspx" %>'><%# Eval("Title") %></a></h3>
                                <p class="hot-see">Lượt xem: <strong><%# Eval("Views") %></strong></p>
                                <div class="articles-content">
                                    <p class="line">Tác giả: <span><%# Eval("Name") %></span></p>

                                    <%--<p>NGUỒN: <span>Theo MASK Online</span></p>--%>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <LayoutTemplate>
                        <span runat="server" id="itemPlaceholder" />
                    </LayoutTemplate>
                </asp:ListView>
            </div>
            <p class="pagerb">
                <span class="slider-prev"></span><span class="slider-next"></span>
            </p>
        </div>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" SelectMethod="LearnsSameSelectAll"
        TypeName="TLLib.Learns">
        <SelectParameters>
            <asp:Parameter Name="RerultCount" Type="String" DefaultValue="100" />
            <asp:QueryStringParameter DefaultValue="" Name="LearnsID" QueryStringField="pqd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphPopup" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#bchondehieu").append($("#btnhondehieu"));
        });
    </script>
</asp:Content>

