﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="chia-se-kinh-nghiem.aspx.cs" Inherits="doc_de_biet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Đọc để biết - Chia sẻ  &amp; Trải nghiệm</title>
    <meta name="Description" content="Đọc để biết - Chia sẻ trải nghiệm" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="wrapper-main">
        <div class="container">
            <div id="site">
                <a href="Default.aspx#learn-know">Đọc để biết</a><%--<a href="#">Chia sẻ &amp; kinh nghiệm</a>--%><span>Chia sẻ &amp; trải nghiệm</span>
            </div>
            <div class="wrap-read">
                <div class="row">
                    <asp:ListView ID="ListView1" runat="server"
                        DataSourceID="ObjectDataSource1"
                        EnableModelValidation="True">
                        <ItemTemplate>
                            <div class="col-sm-6 col-md-4 col-lg-3">
                                <div class="box-show">
                                    <div class="box-read">
                                        <h3 class="read-name"><a href='<%# progressTitle(Eval("ReadersShareCategoryName")) + "-rs-" + Eval("ReadersShareCategoryID") +".aspx" %>'><%# Eval("ReadersShareCategoryName") %></a></h3>
                                        <a href='<%# progressTitle(Eval("ReadersShareCategoryName")) + "-rs-" + Eval("ReadersShareCategoryID") +".aspx" %>' class="read-img">
                                            <img class="img-responsive" id="Img1" alt='' src='<%# "http://www.liveplus.vn/res/readerssharecategory/" + Eval("ImageName") %>'
                                                visible='<%# string.IsNullOrEmpty( Eval("ImageName").ToString()) ? false : 
true %>'/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <span runat="server" id="itemPlaceholder" />
                        </LayoutTemplate>
                    </asp:ListView>
                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
                        SelectMethod="ReadersShareCategorySelectAll" TypeName="TLLib.ReadersShareCategory"></asp:ObjectDataSource>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphPopup" runat="Server">
</asp:Content>

