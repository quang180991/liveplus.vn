﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        string msg = "<h3>Live Plus : Đăng ký</h3><br/>";

        var fileMap = new ExeConfigurationFileMap() { ExeConfigFilename = Server.MapPath("~/config/app.config") };
        var config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
        var section = (AppSettingsSection)config.GetSection("appSettings");
        var strUseSSL = section.Settings["UseSSL"].Value;
        var strPort = section.Settings["Port"].Value;
        var strHost = section.Settings["Host"].Value;
        int n;
        bool isNumeric = int.TryParse(strPort, out n);
        var iPort = isNumeric ? n : 0;
        var strMailFrom = section.Settings["Email"].Value;
        var strUserName = section.Settings["UserName"].Value; 
        var strPassword = TLLib.MD5Hash.Decrypt(section.Settings["Password"].Value, true);
        var strReceivedEmails = section.Settings["ReceivedEmails"].Value;
        var bEnableSsl = strUseSSL.ToLower() == "true" ? true : false;
        var strSubject = "Chúc mừng bạn đã trở thành thành viên Live Plus";
        var strBody = msg;
        var lstMailTo = new List<string>() { strReceivedEmails, "nguyennam92@gmail.com" };
        var lstCC = "";
        var lstAttachment = new List<string>();
        foreach (string item in lstMailTo)
        {
            Response.Write(iPort + " - " + strHost + " " + strMailFrom + " " + strUserName + " " + strPassword + " " + bEnableSsl + " " + item+ "<br>");
            var bSendSuccess = TLLib.Common.SendMail(
                    strHost,
                    iPort,
                    strMailFrom,
                    strUserName,
                    strPassword,
                    item,
                    lstCC,
                    strSubject,
                    strBody,
                    bEnableSsl
                );
        }
    }
}