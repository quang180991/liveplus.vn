﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class cong_tac_vien_chi_tiet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            HtmlMeta meta = new HtmlMeta();
            meta.Name = "description";
            var dv = (DataView)ObjectDataSource1.Select();
            if (dv.Count > 0)
            {
                lbname.Text = dv[0]["LawCounselTitle"].ToString();
                Page.Title = dv[0]["MetaTittle"].ToString();
                meta.Content = dv[0]["MetaDescription"].ToString();
                Header.Controls.Add(meta);
                var row = dv[0];

                var strImageName = Server.HtmlDecode(row["ImageName"].ToString());
                var strTitle = Server.HtmlDecode(row["LawCounselTitle"].ToString());
                var strDescription = Server.HtmlDecode(row["Description"].ToString());

                Header.Controls.Add(meta);
                hdnImageName.Value = strImageName;
                hdnTitle.Value = strTitle;
                hdnDescription.Value = strDescription;
            }

        }
    }
    protected string progressTitle(object input)
    {
        var convertTitle = new ConvertTitle();
        return convertTitle.convertToLowerCase(input.ToString());
    }
}