﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site-sub2.master" AutoEventWireup="true" CodeFile="choi-de-hieu-chi-tiet.aspx.cs" Inherits="choi_de_hieu_chi_tiet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="wrap-section-b">
        <asp:ListView ID="ListView1" runat="server"
            DataSourceID="ObjectDataSource1"
            EnableModelValidation="True">
            <ItemTemplate>
                <div id="site">
                    <a href="Default.aspx#play-learn">Chơi để hiểu</a><span><%# Eval("Title") %></span>
                </div>
                <h1 class="title-6"><span><%# Eval("Title") %></span></h1>
                <div class="head-b line">
                    <span class="head-name">Đăng tin: <strong><%# Eval("Name") %></strong></span>
                    <%--<span class="head-name">NGUỒN: <strong>Theo MASK Online</strong></span>--%>
                    <div class="like-boxb">

                        <div class="addthis_toolbox addthis_default_style ">
                            <a class="addthis_counter addthis_pill_style" data-href='<%# Page.Request.Url.Host.ToString() + "choi-de-hieu-chi-tiet.aspx#" + Eval("LearnID") %>'></a>
                        </div>
                        <script type="text/javascript">        var addthis_config = { "data_track_addressbar": false };</script>
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-508f410e061f6a9d"></script>
                        <div class="fb-like fb-like-1" data-href='<%# Page.Request.Url.Host.ToString() + "choi-de-hieu-chi-tiet.aspx#" + Eval("LearnID") %>'
                            data-send="false" data-layout="button_count" data-width="50" data-show-faces="true">
                        </div>
                    </div>
                </div>
                <div class="head-b"><span class="views">Lượt xem: <strong><%# Eval("Views") %></strong></span></div>
                <div class="head-b"><span class="views">Lượt bình chọn: <strong><%# Eval("Voted") %></strong></span></div>

                <div class="wrap-text-2">
                   
                    <p class="more-a"><a href="#">Báo trùng</a></p>
                    <p class="more-b">
                        <asp:LinkButton ID="btnVote" OnClick="btnVote_Click"
                            ToolTip="Bình chọn" runat="server"><span>Bình chọn</span></asp:LinkButton>
                    </p>
                </div>
                <div class="wrap-comment">
                    <h2 class="order-title">Bình luận</h2>
                    <div class="fb-comments" data-href='<%# Page.Request.Url.Host.ToString() + "choi-de-hieu-chi-tiet.aspx#" + Eval("LearnID") %>' data-width="760"></div>
                </div>
            </ItemTemplate>
            <LayoutTemplate>
                <span runat="server" id="itemPlaceholder" />
            </LayoutTemplate>
        </asp:ListView>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
            SelectMethod="LearnsSelectOne"
            TypeName="TLLib.Learns">
            <SelectParameters>
                <asp:QueryStringParameter Name="LearnsID" QueryStringField="pl" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphSibar" runat="Server">
    <div class="box-adv">
        <img class="img-responsive" src="assets/images/banner-adv.jpg" alt="" />
    </div>
    <div class="box-adv">
        <img class="img-responsive" src="assets/images/banner-adv.jpg" alt="" />
    </div>
    <div class="box-list">
        <h2 class="title-d">Các tác phẩm khác</h2>
        <div id="boxab" class="box-ab">
            <div id="abfirst">
                <div class="box-art">
                    <a href="#" class="art-img">
                        <img class="img-responsive" src="assets/images/sknow-img-1.jpg" alt="" /></a>
                    <div class="art-content">
                        <h3 class="art-name"><a href='<%# progressTitle(Eval("Title")) + "-pl-" + Eval("LearnsID") +".aspx" %>'>Ba giây là bấm</a></h3>
                        <p class="hot-see">Lượt xem: <strong>20</strong></p>
                        <div class="articles-content">
                            <p class="line">Đăng tin: <span>Admin</span></p>
                            <p>NGUỒN: <span>Theo MASK Online</span></p>
                        </div>
                    </div>
                </div>
                <div class="box-art">
                    <a href="#" class="art-img">
                        <img class="img-responsive" src="assets/images/sknow-img-1.jpg" alt="" /></a>
                    <div class="art-content">
                        <h3 class="art-name"><a href="#">Ba giây là bấm</a></h3>
                        <p class="hot-see">Lượt xem: <strong>20</strong></p>
                        <div class="articles-content">
                            <p class="line">Đăng tin: <span>Admin</span></p>
                            <p>NGUỒN: <span>Theo MASK Online</span></p>
                        </div>
                    </div>
                </div>
                <div class="box-art">
                    <a href="#" class="art-img">
                        <img class="img-responsive" src="assets/images/sknow-img-1.jpg" alt="" /></a>
                    <div class="art-content">
                        <h3 class="art-name"><a href="#">Ba giây là bấm 3</a></h3>
                        <p class="hot-see">Lượt xem: <strong>20</strong></p>
                        <div class="articles-content">
                            <p class="line">Đăng tin: <span>Admin</span></p>
                            <p>NGUỒN: <span>Theo MASK Online</span></p>
                        </div>
                    </div>
                </div>
                <div class="box-art">
                    <a href="#" class="art-img">
                        <img class="img-responsive" src="assets/images/sknow-img-1.jpg" alt="" /></a>
                    <div class="art-content">
                        <h3 class="art-name"><a href="#">Ba giây là bấm</a></h3>
                        <p class="hot-see">Lượt xem: <strong>20</strong></p>
                        <div class="articles-content">
                            <p class="line">Đăng tin: <span>Admin</span></p>
                            <p>NGUỒN: <span>Theo MASK Online</span></p>
                        </div>
                    </div>
                </div>
                <div class="box-art">
                    <a href="#" class="art-img">
                        <img class="img-responsive" src="assets/images/sknow-img-1.jpg" alt="" /></a>
                    <div class="art-content">
                        <h3 class="art-name"><a href="#">Ba giây là bấm</a></h3>
                        <p class="hot-see">Lượt xem: <strong>20</strong></p>
                        <div class="articles-content">
                            <p class="line">Đăng tin: <span>Admin</span></p>
                            <p>NGUỒN: <span>Theo MASK Online</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <p class="pagerb">
                <span class="slider-prev"></span><span class="slider-next"></span>
            </p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphPopup" runat="Server">
</asp:Content>

