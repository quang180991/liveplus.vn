﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class doc_de_biet_kinh_nghiem : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            HtmlMeta meta = new HtmlMeta();
            meta.Name = "description";
            string CID = Request.QueryString["rs"];
            if (CID != null)
            {
                var dv = new TLLib.ReadersShareCategory().ReadersShareCategorySelectOne(CID).DefaultView;
                if (dv.Count > 0)
                {
                    lbName.Text = dv[0]["ReadersShareCategoryName"].ToString();
                    Page.Title = dv[0]["MetaTitle"].ToString();
                    meta.Content = dv[0]["MetaDescription"].ToString();
                    Header.Controls.Add(meta);
                }
            }
        }
    }
    protected string progressTitle(object input)
    {
        var convertTitle = new ConvertTitle();
        return convertTitle.convertToLowerCase(input.ToString());
    }
}