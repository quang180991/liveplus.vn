﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site-sub.master" AutoEventWireup="true" CodeFile="chia-se-kinh-nghiem-chi-tiet.aspx.cs" Inherits="doc_de_hieu_chi_tiet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">   
    <meta property="og:type" content="activity" />
    <%--đường link muốn share--%>
    <meta property="og:url" content='<%= Request.Url.Scheme + "://" +Page.Request.Url.Host.ToString()+ "/"+ Request.Url.AbsolutePath.Substring(Request.Url.AbsolutePath.LastIndexOf("/") + 1) %>' />
    <%--tiêu đề muốn share--%>
    <meta property="og:title" content='<%= hdnTitle.Value %>' />
    <%--hình ảnh muốn share--%>
    <%--mô tả muốn share--%>
    <meta property="og:description" content='<%= hdnDescription.Value %>' />
     <meta property="og:image" content='<%= Request.Url.Scheme + "://" +Page.Request.Url.Host.ToString() + "/res/readersshare/" + hdnImageName.Value  %>' />
    <%--tên website muốn share--%>
    <meta property="og:site_name" content='<%= Request.Url.Scheme + "://" +Page.Request.Url.Host.ToString()%>' />
    <meta name="thumbnail" content='<%= Request.Url.Scheme + "://" +Page.Request.Url.Host.ToString() + "/res/readersshare/" + hdnImageName.Value  %>' />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <asp:HiddenField ID="hdnImageName" runat="server" />
    <asp:HiddenField ID="hdnTitle" runat="server" />
    <asp:HiddenField ID="hdnDescription" runat="server" />
    <asp:ListView ID="ListView1" runat="server"
        DataSourceID="ObjectDataSource2"
        EnableModelValidation="True">
        <ItemTemplate>
            <div id="site">
                <a href="Default.aspx#learn-know">Đọc để biết</a><a href="chia-se-kinh-nghiem.aspx">Chia sẻ &amp; trải nghiệm</a><a href='<%# progressTitle(Eval("ReadersShareCategoryName")) + "-rs-" + Eval("ReadersShareCategoryID") +".aspx" %>'>
                    <%# Eval("ReadersShareCategoryName") %></a><span><%# Eval("ReadersShareTitle") %> </span>
            </div>
            <h1 class="title">
                <%# Eval("ReadersShareTitle") %>  </h1>
            <div class="box-like">
                <div class="box-like-b">
                    <div class="fb-like" data-href='<%= Page.Request.Url.AbsolutePath%>'
                        data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                    </div>
                </div>
            </div>
            <div class="wrap-detail">
                <div class="wrap-text">
                    <%# Eval("Content") %>
                </div>
                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ReadersShareID") %>' />
                <div class="wrap-order">
                    <h2 class="order-title">Bài viết liên quan</h2>
                    <ul class="list-order">
                        <asp:ListView ID="ListView2" runat="server"
                            DataSourceID="ObjectDataSource3"
                            EnableModelValidation="True">
                            <ItemTemplate>
                                <li><a href='<%# progressTitle(Eval("ReadersShareTitle")) + "-rsd-" + Eval("ReadersShareID") +".aspx" %>'><%# Eval("ReadersShareTitle") %></a></li>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                    </ul>
                </div>
                <asp:ObjectDataSource ID="ObjectDataSource3" runat="server"
                    SelectMethod="ReadersShareSameSelectAll" TypeName="TLLib.ReadersShare">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="10" Name="RerultCount" Type="String" />
                        <asp:ControlParameter ControlID="hdnID" Name="ReadersShareID" PropertyName="Value" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>

                <div class="wrap-comment">
                    <h2 class="order-title">Bình luận</h2>

                    <div class="fb-comments" data-href='<%=  Page.Request.Url.Host.ToString()+ Page.Request.Url.AbsolutePath.ToString()%>' data-width="860"></div>

                </div>
            </div>
        </ItemTemplate>
        <LayoutTemplate>
            <span runat="server" id="itemPlaceholder" />
        </LayoutTemplate>
    </asp:ListView>

    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server"
        SelectMethod="ReadersShareSelectOne" TypeName="TLLib.ReadersShare">
        <SelectParameters>
            <asp:QueryStringParameter Name="ReadersShareID" QueryStringField="rsd" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphSibar" runat="Server">
    <h2 class="title-c">Danh mục</h2>
    <ul class="list-nav nav-line">
        <asp:ListView ID="ListView3" runat="server"
            DataSourceID="ObjectDataSource1"
            EnableModelValidation="True">
            <ItemTemplate>
                <li><a href='<%# progressTitle(Eval("ReadersShareCategoryName")) + "-rs-" + Eval("ReadersShareCategoryID") +".aspx" %>'><%# Eval("ReadersShareCategoryName") %></a></li>
            </ItemTemplate>
            <LayoutTemplate>
                <span runat="server" id="itemPlaceholder" />
            </LayoutTemplate>
        </asp:ListView>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
            SelectMethod="ReadersShareCategorySelectAll" TypeName="TLLib.ReadersShareCategory"></asp:ObjectDataSource>
    </ul>
    <div class="box-wp">
        <h2 class="title-b">các bài viết mới nhất</h2>
        <ul class="list-experience">
            <asp:ListView ID="ListView5" runat="server"
                DataSourceID="ObjectDataSource4"
                EnableModelValidation="True">
                <ItemTemplate>
                    <li>
                        <h4 class="experience-name"><a href='<%# progressTitle(Eval("ReadersShareCategoryName")) + "-rs-" + Eval("ReadersShareCategoryID") +".aspx" %>'><%# Eval("ReadersShareCategoryName") %></a></h4>
                        <h3 class="exper-name"><a href='<%# progressTitle(Eval("ReadersShareTitle")) + "-rsd-" + Eval("ReadersShareID") +".aspx" %>'><%# Eval("ReadersShareTitle") %></h3>
                        <div class="description"><%# TLLib.Common.SplitSummary(Eval("Description").ToString(),100) %></div>
                    </li>
                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>

        </ul>
        <asp:ObjectDataSource ID="ObjectDataSource4" runat="server"
            SelectMethod="ReadersShareSelectAll" TypeName="TLLib.ReadersShare">
            <SelectParameters>
                <asp:Parameter DefaultValue="1" Name="StartRowIndex" Type="String" />
                <asp:Parameter Name="EndRowIndex" Type="String" DefaultValue="3" />
                <asp:Parameter DefaultValue="" Name="Keyword" Type="String" />
                <asp:Parameter Name="ReadersShareTitle" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter DefaultValue="" Name="ReadersShareCategoryID" Type="String" />
                <asp:Parameter Name="Tag" Type="String" />
                <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                <asp:Parameter Name="IsHot" Type="String" />
                <asp:Parameter Name="IsNew" Type="String" DefaultValue="true" />
                <asp:Parameter Name="FromDate" Type="String" />
                <asp:Parameter Name="ToDate" Type="String" />
                <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                <asp:Parameter Name="Priority" Type="String" />
                <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphPopup" runat="Server">
</asp:Content>

