﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class choi_de_cuoi : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            pnSearch1.Visible = false;
            pnNoSearch1.Visible = true;
        }

    }
    protected string progressTitle(object input)
    {
        var convertTitle = new ConvertTitle();
        return convertTitle.convertToLowerCase(input.ToString());
    }
    protected void radSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        pnNoSearch1.Visible = false;
        pnSearch1.Visible = true;


        var dv = (DataView)ObjectDataSource8.Select();

        if (dv.Count > 0)
        {
            if (dv[0]["WatchLaughCategoryID"].ToString() == "3")
            {
                DataPager2.PageSize = 6;

            }
            else
            {
                DataPager2.PageSize = 12;
            }

        }
    }
}