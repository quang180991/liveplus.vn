﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class createusers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    protected void Login1_LoggedIn(object sender, EventArgs e)
    {
        string UserName = Login1.UserName;
        //MembershipUser mu = Membership.GetUser(UserName);
        //Session["PWD"] = Login1.Password;
        if (UserName != null)
        {
            Session["UserName"] = UserName;
            if (Request.QueryString["ReturnURL"] != null)
            {
                Response.Redirect(Request.QueryString["ReturnURL"]);
            }
            else
            {
                string[] roleUser = Roles.GetRolesForUser(UserName.ToString());
                for (int i = 0; i < roleUser.Length; i++)
                {
                    if (roleUser[i] == "admin")
                        Response.Redirect("ad/bilingual/");
                    else
                        Response.Redirect("~/");
                }
            }
        }
    }
}
