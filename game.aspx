﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true"
    CodeFile="game.aspx.cs" Inherits="game" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
    <title>Live Plus Games</title>
    <meta name="Description" content="Live Plus Games" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="wrapper-main">
        <div class="container">
            <div id="site">
                <a href="cong-tac-vien.aspx">Chơi để hiểu</a><span>Game</span>
            </div>
            <asp:ListView ID="ListView1" runat="server"
                DataSourceID="ObjectDataSource2"
                EnableModelValidation="True">
                <ItemTemplate>
                    <h1 class="title-7"> <%# Eval("AggregatedTitle") %></h1>
                    <%# Eval("Content") %>
                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>
            <asp:ObjectDataSource ID="ObjectDataSource2" runat="server"
                SelectMethod="AggregatedSelectAll" TypeName="TLLib.Aggregated">
                <SelectParameters>
                    <asp:Parameter Name="StartRowIndex" Type="String" />
                    <asp:Parameter Name="EndRowIndex" Type="String" />
                    <asp:Parameter Name="Keyword" Type="String" />
                    <asp:Parameter Name="AggregatedTitle" Type="String" />
                    <asp:Parameter Name="Description" Type="String" />
                    <asp:Parameter Name="AggregatedCategoryID" Type="String" DefaultValue="16" />
                    <asp:Parameter Name="Tag" Type="String" />
                    <asp:Parameter DefaultValue="" Name="IsShowOnHomePage" Type="String" />
                    <asp:Parameter Name="IsHot" Type="String" />
                    <asp:Parameter Name="IsNew" Type="String" />
                    <asp:Parameter Name="FromDate" Type="String" />
                    <asp:Parameter Name="ToDate" Type="String" />
                    <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                    <asp:Parameter DefaultValue="" Name="Priority" Type="String" />
                    <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>

            <p class="more-gamesp">
                <a class='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "hidden" : "" %>'
                    data-toggle="modal" data-target='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "" : "#registerlogingames" %>'
                    href="javascript:void(0);">tham gia trò chơi</a> <a class='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "" : "hidden" %>'
                        href='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "game-play.aspx" : "javascript:void(0);" %>'>tham gia trò chơi </a>
            </p>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphPopup" runat="Server">
</asp:Content>
