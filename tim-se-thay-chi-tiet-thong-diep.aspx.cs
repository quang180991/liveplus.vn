﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TLLib;

public partial class choi_de_hieu_chi_tiet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            HtmlMeta meta = new HtmlMeta();
            meta.Name = "description";
            var dv1 = (DataView)ObjectDataSource1.Select();
            if (dv1.Count > 0)
            {

                DateTime dateNow = DateTime.Now.Date;
                DateTime dateOld = DateTime.Now.AddDays(-1).Date;
                if (dv1[0]["FinishDate"] != DBNull.Value)
                {

                    dateOld = (Convert.ToDateTime(dv1[0]["FinishDate"])).Date;

                }

                if (dateNow > dateOld)
                {
                    hdndateold.Value = "0";
                }

                string ID = dv1[0]["LearnsID"].ToString();
                hdnGallery.Value = dv1[0]["InGallery"].ToString();
                Page.Title = dv1[0]["Title"].ToString();
                meta.Content = dv1[0]["Title"].ToString();
                Header.Controls.Add(meta);

                var row = dv1[0];

                var strTitle = Server.HtmlDecode(row["Title"].ToString());
                var strDescription = Server.HtmlDecode(row["Title"].ToString());
                var strImageName = Server.HtmlDecode(row["ImagePath"].ToString());

                Header.Controls.Add(meta); 
                hdnCID.Value = dv1[0]["ParentID"].ToString();
                hdnImageName.Value = strImageName;
                hdnTitle.Value = strTitle;
                hdnDescription.Value = strDescription;

                new Learns().LearnsUpdateCountViews(ID.ToString());
                string UserName = System.Web.HttpContext.Current.User.Identity.Name;
                var oVote = new Learns_User().LearnsUserSelectAll(ID.ToString(), UserName).DefaultView;
                if (oVote.Count == 1)
                {
                    hdnBT.Value = "1";
                    btnVote.CssClass = "hidden";

                }
            }
            if (Session["LoginBC"] != null)
            {
                if (Session["LoginBC"].ToString() == "true")
                {
                    bool IsLogin = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
                    if (IsLogin == true)
                    {
                        string UserName = System.Web.HttpContext.Current.User.Identity.Name;
                        string LearnsID = dv1[0]["LearnsID"].ToString();
                        new Learns_User().LearnsUserInsert(LearnsID, UserName, "", "", "true");
                        new Learns().LearnsUpdateCountVoted(LearnsID);
                        ListView1.DataBind();
                        var oVote = new Learns_User().LearnsUserSelectAll(LearnsID, UserName).DefaultView;
                        if (oVote.Count == 1)
                        {
                            hdnBT.Value = "1";
                            btnVote.CssClass = "hidden";

                        }
                    }
                    Session["LoginBC"] = "";
                }
            }
        }
    }
    protected string progressTitle(object input)
    {
        var convertTitle = new ConvertTitle();
        return convertTitle.convertToLowerCase(input.ToString());
    }

    protected void btnVote_Click(object sender, EventArgs e)
    {
        bool IsLogin = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;

        var dv1 = (DataView)ObjectDataSource1.Select();
        if (dv1.Count > 0)
        {
            if (IsLogin == true)
            {
                string UserName = System.Web.HttpContext.Current.User.Identity.Name;
                string LearnsID = dv1[0]["LearnsID"].ToString();
                new Learns_User().LearnsUserInsert(LearnsID, UserName, "", "", "true");
                new Learns().LearnsUpdateCountVoted(LearnsID);
                ListView1.DataBind();
                Response.Redirect(Page.Request.Url.AbsolutePath.ToString());
            }
        }
    }
}