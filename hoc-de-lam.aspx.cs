﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class hoc_de_lam : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["DNAcc"] != null)
        {
            if (Session["DNAcc"] == "true")
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "runtime", " $(document).ready(function () {popupDNaccess();});", true);
                Session["DNAcc"] = "";
            }
        }
        if (((DataView)ObjectDataSource1.Select()).Count <= DataPager1.PageSize)
        {
            DataPager1.Visible = false;
        }
    }
    protected string progressTitle(object input)
    {
        var convertTitle = new ConvertTitle();
        return convertTitle.convertToLowerCase(input.ToString());
    }
    protected void btnUpLoad_Click(object sender, EventArgs e)
    {
        string msg = "<h3>Gosmart xin chào anh chị,</h3><br/>";
        msg += "<p>Cảm ơn anh chị đã quan tâm đến việc triển khai Hệ thống quản lý ATGT đường bộ theo tiêu chuẩn ISO 39001:2012. Chúng tôi đã nhận được thông tin của anh chị và sẽ liên hệ với anh chị qua thông tin anh chị đã cung cấp trong thời gian sớm nhất.</p><br/>";
        msg += "<p>Mọi thắc mắc về chương trình, anh chị có thể liên hệ trực tiếp với Ban Quản lý dự án:</p><br/>";
        msg += "<h3>Thông tin liên hệ của dự án</h3><br/>";
        msg += "<b>Email: </b><a href='mailto:hotro@liveplus.vn'>hotro@liveplus.vn</a><br /><br />";
        msg += "<b>Điện thoại: </b>090.626.0928<br />";
        msg += "<p><b>Địa chỉ: </b>Chương trình Gosmart<br />";
        msg += "Thực hiện bởi Công ty TUV NORD Vietnam<br />";
        msg += "P803 Tòa nhà Thăng Long - 105 Láng Hạ - Phường Láng Hạ - Quận Đống Đa - Hà Nội<br />";
        msg += "<b>Website chính thức của chương trình: </b> <a href='www.liveplus.vn'>www.liveplus.vn</a></p><br/>";
        msg += "-------------------------------------------------------------------------------------------<br/><br/>";
        msg += "<h3>Chúng tôi xin xác nhận thông tin của bạn như sau:</h3><br/>";
        msg += "<b>Tên công ty: </b>" + txtFullName.Text.ToString().Trim() + "<br /><br />";
        msg += "<b>Địc chỉ: </b>" + txtAddress.Text.ToString().Trim() + "<br /><br />";
        msg += "<b>Lĩnh vực hoạt động: </b>" + txtLinhVuc.Text.Trim().ToString() + "<br /><br />";
        msg += "<b>Tên người liên hệ: </b>" + txtName.Text.Trim().ToString() + "<br /><br />";
        msg += "<b>Chức vụ: </b>" + txtChucVu.Text.Trim().ToString() + "<br /><br />";
        msg += "<b>Điện thoại: </b>" + txtPhone.Text.Trim().ToString() + "<br /><br />";
        msg += "<b>Email: </b>" + txtEmail.Text.Trim().ToString() + "<br /><br />";

        var fileMap = new ExeConfigurationFileMap() { ExeConfigFilename = Server.MapPath("~/config/app.config") };
        var config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
        var section = (AppSettingsSection)config.GetSection("appSettings");
        var strUseSSL = section.Settings["UseSSL"].Value;
        var strPort = section.Settings["Port"].Value;
        var strHost = section.Settings["Host"].Value;
        int n;
        bool isNumeric = int.TryParse(strPort, out n);
        var iPort = isNumeric ? n : 0;
        var strMailFrom = section.Settings["Email"].Value;
        var strUserName = section.Settings["UserName"].Value;
        var strPassword = TLLib.MD5Hash.Decrypt(section.Settings["Password"].Value, true);
        var strReceivedEmails = section.Settings["ReceivedEmails"].Value;
        var bEnableSsl = strUseSSL.ToLower() == "true" ? true : false;
        var strSubject = "Contact from Live Plus website";
        var strBody = msg;
        var lstMailTo = strReceivedEmails.Replace(',', ';').Split(';').ToList<string>();
        var lstCC = new List<string>();
        var lstAttachment = new List<string>();

        var bSendSuccess = TLLib.Common.SendMail(
                strHost,
                iPort,
                strMailFrom,
                strUserName,
                strPassword,
                lstMailTo,
                lstCC,
                lstAttachment,
                strSubject,
                strBody,
                bEnableSsl
            );
        txtFullName.Text = txtAddress.Text = txtLinhVuc.Text = txtName.Text = txtChucVu.Text = txtPhone.Text = txtEmail.Text = "";
        Session["DNAcc"] = "true";
        Response.Redirect(Page.Request.Url.AbsolutePath.ToString());
    }
}