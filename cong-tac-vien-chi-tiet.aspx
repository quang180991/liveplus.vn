﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site-sub.master" AutoEventWireup="true" CodeFile="cong-tac-vien-chi-tiet.aspx.cs" Inherits="cong_tac_vien_chi_tiet" %>

<%@ Register Src="~/uc/registeredCTV.ascx" TagPrefix="uc1" TagName="registeredCTV" %>
<%@ Register Src="~/uc/successCTV.ascx" TagPrefix="uc1" TagName="successCTV" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta property="og:type" content="activity" />
    <%--đường link muốn share--%>
    <meta property="og:url" content='<%= Request.Url.Scheme + "://" +Page.Request.Url.Host.ToString() +  "/"+ Request.Url.AbsolutePath.Substring(Request.Url.AbsolutePath.LastIndexOf("/") + 1) %>' />
    <%--tiêu đề muốn share--%>
    <meta property="og:title" content='<%= hdnTitle.Value %>' />
    <%--hình ảnh muốn share--%>
    <%--mô tả muốn share--%>
    <meta property="og:description" content='<%= hdnDescription.Value %>' />
    <meta property="og:image" content='<%= Request.Url.Scheme + "://" + Page.Request.Url.Host.ToString() + "/res/lawcounsel/" + hdnImageName.Value  %>' />
    <%--tên website muốn share--%>
    <meta property="og:site_name" content='<%= Request.Url.Scheme + "://" + Page.Request.Url.Host.ToString()%>' />
    <meta name="thumbnail" content='<%= Request.Url.Scheme + "://" + Page.Request.Url.Host.ToString() + "/res/lawcounsel/" + hdnImageName.Value  %>' />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hdnImageName" runat="server" />
    <asp:HiddenField ID="hdnTitle" runat="server" />
    <asp:HiddenField ID="hdnDescription" runat="server" />
    <div id="site">
        <a href="cong-tac-vien.aspx">Cộng tác viên</a><span>Hoạt động cộng tác viên</span>
    </div>
    <div class="head-box">
        <div class="head-box-b">
            <h1 class="title">
                <asp:Label ID="lbname" runat="server"></asp:Label></h1>
            <div style="float: right; position: relative; bottom: 0;">
                <div class="fb-like"
                    data-href='<%= Page.Request.Url.AbsolutePath%>'
                    data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                </div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
    <div class="wrap-text">
        <asp:ListView ID="ListView1" runat="server"
            DataSourceID="ObjectDataSource1"
            EnableModelValidation="True">
            <ItemTemplate>
                <%# Eval("Content") %>

                <div class="wrap-comment">
                    <h2 class="order-title">Bình luận</h2>

                    <div class="fb-comments" data-href='<%=  Page.Request.Url.Host.ToString()+ Page.Request.Url.AbsolutePath.ToString()%>' data-width="860"></div>

                </div>
            </ItemTemplate>
            <LayoutTemplate>
                <span runat="server" id="itemPlaceholder" />
            </LayoutTemplate>
        </asp:ListView>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
            SelectMethod="LawCounselSelectOne" TypeName="TLLib.LawCounsel">
            <SelectParameters>
                <asp:QueryStringParameter Name="LawCounselID" QueryStringField="cb" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphSibar" runat="Server">
    <h2 class="title-c">Danh mục</h2>
    <ul class="list-nav">
        <li><a href="cong-tac-vien-quy-dinh.aspx">Thông tin dành cho cộng tác viên</a></li>
        <li><a href="cong-tac-vien.aspx">Hoạt động cộng tác viên</a></li>
    </ul>
    <div class="box-res">
        <h2>Hãy tham gia vào cộng đồng <strong>CỘNG TÁC VIÊN LIVE PLUS </strong><span>để cuộc sống mỗi ngày trở nên ý nghĩa hơn</span></h2>
        <p class="more-gis"><a href='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ?"javarscript:void(0);" : "popuppage/Login.aspx?cmd=CTV" %>' data-toggle="modal" data-target='<%= Roles.IsUserInRole(System.Web.HttpContext.Current.User.Identity.Name,"partners") ==true ?"": System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "#popupctv" : "#popupPages" %>'><%= Roles.IsUserInRole(System.Web.HttpContext.Current.User.Identity.Name,"partners") ==true ?"Bạn đã là Cộng Tác Viên":"Đăng kí"%></a></p>
    </div>
    <div class="asibar-slider">
        <ul id="slidera">
            <asp:ListView ID="ListView3" runat="server"
                DataSourceID="ObjectDataSource3"
                EnableModelValidation="True">
                <ItemTemplate>
                    <li>
                        <div class="box-s">
                            <img id="Img2" alt='' src='<%# "http://www.liveplus.vn/res/advertisement/" + Eval("FileName") %>' class="img-responsive"
                                visible='<%# string.IsNullOrEmpty( Eval("FileName").ToString()) ? false : 
true %>'
                                runat="server" />
                            <div class="box-scontent"><a href='<%# Eval("Website") %> '><%# Eval("CompanyName") %> </a></div>
                        </div>
                    </li>
                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>
        </ul>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server"
        SelectMethod="AdsBannerSelectAll" TypeName="TLLib.AdsBanner">
        <SelectParameters>
            <asp:Parameter Name="StartRowIndex" Type="String" />
            <asp:Parameter Name="EndRowIndex" Type="String" />
            <asp:Parameter Name="AdsCategoryID" Type="String" DefaultValue="4" />
            <asp:Parameter Name="CompanyName" Type="String" />
            <asp:Parameter Name="Website" Type="String" />
            <asp:Parameter Name="FromDate" Type="String" />
            <asp:Parameter Name="ToDate" Type="String" />
            <asp:Parameter Name="IsAvailable" Type="String" DefaultValue="true" />
            <asp:Parameter Name="Priority" Type="String" />
            <asp:Parameter Name="SortByPriority" Type="String" DefaultValue="true" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <div class="box-wp">
        <h2 class="title-b">các hoạt động của
            <br />
            cộng tác viên</h2>
        <div class="row">
            <asp:ListView ID="ListView2" runat="server"
                DataSourceID="ObjectDataSource2"
                EnableModelValidation="True">
                <ItemTemplate>
                    <div class="col-md-12 col-xs-6">
                        <div class="box-news news-bs">
                            <a href='<%# progressTitle(Eval("LawCounselTitle")) + "-cb-" + Eval("LawCounselID") +".aspx" %>' class="news-img">
                                <img id="Img1" alt='' src='<%# "http://www.liveplus.vn/res/lawcounsel/" + Eval("ImageName") %>' class="img-responsive"
                                    visible='<%# string.IsNullOrEmpty( Eval("ImageName").ToString()) ? false : 
true %>'
                                    runat="server" /></a>
                            <div class="news-content">
                                <h3 class="news-names"><a href='<%# progressTitle(Eval("LawCounselTitle")) + "-cb-" + Eval("LawCounselID") +".aspx" %>'><%# Eval("LawCounselTitle") %></a></h3>
                                <div class="colla-bottom">
                                    <span class="date"><%# string.Format("{0:dd.MM.yyyy}", Eval("CreateDate"))%></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>

        </div>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server"
        SelectMethod="LawCounselSameSelectAll" TypeName="TLLib.LawCounsel">
        <SelectParameters>
            <asp:Parameter Name="RerultCount" Type="String" DefaultValue="6" />
            <asp:QueryStringParameter DefaultValue="" Name="LawCounselID" QueryStringField="cb" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphPopup" runat="Server">

    <!-- Modal -->
    <a href="javascript:void(0);" class="show-ctvup" data-toggle="modal" data-target="#popupctv"></a>
    <div class="modal fade popuplogin" id="popupctv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <uc1:registeredCTV runat="server" ID="registeredCTV" />
            </div>
        </div>
    </div>
    <!-- Modal -->
    <a href="javascript:void(0);" class="ctv-ctvacc" data-toggle="modal" data-target="#ctva"></a>
    <div class="modal fade popupbox" id="ctva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <uc1:successCTV runat="server" ID="successCTV" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="clientScript" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            //Goi Upload Choi De Hieu video
            <%if (Session["LoginCMD"] != null)
              { %>
              <%if (Session["LoginCMD"].ToString() == "CTV")
                { %>
            popuporctvupload();
            <% Session["LoginCMD"] = ""; %>
            <% }
              } %>
            //Goi Success Choi De Hieu video
            <%if (Session["Success"] != null)
              { %>
            <%if (Session["Success"].ToString() == "true")
              { %>
            popupctvaccess();
            <% Session["Success"] = ""; %>
            <% }
              } %>
        });</script>
</asp:Content>

