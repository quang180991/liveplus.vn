﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class doc_de_biet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Giao thông
        string url = "http://dantri.com.vn/giao-thong.rss";
        XmlTextReader reader = new XmlTextReader(url);

        DataSet ds = new DataSet();
        ds.ReadXml(reader);

        var dt = ds.Tables["item"];
        var dt1 = dt.Clone();
        dt1.Rows.Add(dt.Rows[0].ItemArray);

        rptRSS.DataSource = dt1;
        rptRSS.DataBind();

        dt1.Rows.Clear();

        for (int i = 1; i < 4; i++)
        {
            dt1.Rows.Add(dt.Rows[i].ItemArray);
        }
        rptRSS1.DataSource = dt1;
        rptRSS1.DataBind();

        dt1.Rows.Clear();

        for (int i = 4; i < 7; i++)
        {
            dt1.Rows.Add(dt.Rows[i].ItemArray);
        }
        rptRSS2.DataSource = dt1;
        rptRSS2.DataBind();
        //////////////////////////////////////

        //Xa hoi
        string urlXaHoi = "http://dantri.com.vn/xa-hoi.rss";
        XmlTextReader readerXaHoi = new XmlTextReader(urlXaHoi);

        DataSet dsXaHoi = new DataSet();
        dsXaHoi.ReadXml(readerXaHoi);

        var dtXaHoi = dsXaHoi.Tables["item"];
        var dtXaHoi1 = dtXaHoi.Clone();
        dtXaHoi1.Rows.Add(dtXaHoi.Rows[0].ItemArray);

        lvXaHoi.DataSource = dtXaHoi1;
        lvXaHoi.DataBind();

        dtXaHoi1.Rows.Clear();

        for (int i = 1; i<4; i++)
        {
            dtXaHoi1.Rows.Add(dtXaHoi.Rows[i].ItemArray);
        }
        lvXaHoi1.DataSource = dtXaHoi1;
        lvXaHoi1.DataBind();
        ///////////////////////////////////////

        //The Gioi
        string urlTheGioi = "http://dantri.com.vn/Thegioi.rss";
        XmlTextReader readerTheGioi = new XmlTextReader(urlTheGioi);

        DataSet dsTheGioi = new DataSet();
        dsTheGioi.ReadXml(readerTheGioi);

        var dtTheGioi = dsTheGioi.Tables["item"];
        var dtTheGioi1 = dtTheGioi.Clone();
        dtTheGioi1.Rows.Add(dtTheGioi.Rows[0].ItemArray);

        lvTheGioi.DataSource = dtTheGioi1;
        lvTheGioi.DataBind();

        dtTheGioi1.Rows.Clear();

        for (int i = 1; i<4; i++)
        {
            dtTheGioi1.Rows.Add(dtTheGioi.Rows[i].ItemArray);
        }
        lvTheGioi1.DataSource = dtTheGioi1;
        lvTheGioi1.DataBind();
        ///////////////////////////////////////

        //The Thao
        string urlTheThao = "http://dantri.com.vn/The-Thao.rss";
        XmlTextReader readerTheThao = new XmlTextReader(urlTheThao);

        DataSet dsTheThao = new DataSet();
        dsTheThao.ReadXml(readerTheThao);

        var dtTheThao = dsTheThao.Tables["item"];
        var dtTheThao1 = dtTheThao.Clone();
        dtTheThao1.Rows.Add(dtTheThao.Rows[0].ItemArray);

        lvTheThao.DataSource = dtTheThao1;
        lvTheThao.DataBind();

        dtTheThao1.Rows.Clear();

        for (int i = 1; i<4; i++)
        {
            dtTheThao1.Rows.Add(dtTheThao.Rows[i].ItemArray);
        }
        lvTheThao1.DataSource = dtTheThao1;
        lvTheThao1.DataBind();
        ///////////////////////////////////////

        //Giao Duc
        string urlGiaoDuc = "http://dantri.com.vn/giaoduc-khuyenhoc.rss";
        XmlTextReader readerGiaoDuc = new XmlTextReader(urlGiaoDuc);

        DataSet dsGiaoDuc = new DataSet();
        dsGiaoDuc.ReadXml(readerGiaoDuc);

        var dtGiaoDuc = dsGiaoDuc.Tables["item"];
        var dtGiaoDuc1 = dtGiaoDuc.Clone();
        dtGiaoDuc1.Rows.Add(dtGiaoDuc.Rows[0].ItemArray);

        lvGiaoDuc.DataSource = dtGiaoDuc1;
        lvGiaoDuc.DataBind();

        dtGiaoDuc1.Rows.Clear();

        for (int i = 1; i<4; i++)
        {
            dtGiaoDuc1.Rows.Add(dtGiaoDuc.Rows[i].ItemArray);
        }
        lvGiaoDuc1.DataSource = dtGiaoDuc1;
        lvGiaoDuc1.DataBind();
        ///////////////////////////////////////

        //Tam Long
        string urlTamLong = "http://dantri.com.vn/tamlongnhanai.rss";
        XmlTextReader readerTamLong = new XmlTextReader(urlTamLong);

        DataSet dsTamLong = new DataSet();
        dsTamLong.ReadXml(readerTamLong);

        var dtTamLong = dsTamLong.Tables["item"];
        var dtTamLong1 = dtTamLong.Clone();
        dtTamLong1.Rows.Add(dtTamLong.Rows[0].ItemArray);

        lvTamLong.DataSource = dtTamLong1;
        lvTamLong.DataBind();

        dtTamLong1.Rows.Clear();

        for (int i = 1; i<4; i++)
        {
            dtTamLong1.Rows.Add(dtTamLong.Rows[i].ItemArray);
        }
        lvTamLong1.DataSource = dtTamLong1;
        lvTamLong1.DataBind();
        ///////////////////////////////////////

        //Kinh Doanh
        string urlKinhDoanh = "http://dantri.com.vn/kinhdoanh.rss";
        XmlTextReader readerKinhDoanh = new XmlTextReader(urlKinhDoanh);

        DataSet dsKinhDoanh = new DataSet();
        dsKinhDoanh.ReadXml(readerKinhDoanh);

        var dtKinhDoanh = dsKinhDoanh.Tables["item"];
        var dtKinhDoanh1 = dtKinhDoanh.Clone();
        dtKinhDoanh1.Rows.Add(dtKinhDoanh.Rows[0].ItemArray);

        lvKinhDoanh.DataSource = dtKinhDoanh1;
        lvKinhDoanh.DataBind();

        dtKinhDoanh1.Rows.Clear();

        for (int i = 1; i<4; i++)
        {
            dtKinhDoanh1.Rows.Add(dtKinhDoanh.Rows[i].ItemArray);
        }
        lvKinhDoanh1.DataSource = dtKinhDoanh1;
        lvKinhDoanh1.DataBind();
        ///////////////////////////////////////

        //Van Hoa
        string urlVanHoa = "http://dantri.com.vn/van-hoa.rss";
        XmlTextReader readerVanHoa = new XmlTextReader(urlVanHoa);

        DataSet dsVanHoa = new DataSet();
        dsVanHoa.ReadXml(readerVanHoa);

        var dtVanHoa = dsVanHoa.Tables["item"];
        var dtVanHoa1 = dtVanHoa.Clone();
        dtVanHoa1.Rows.Add(dtVanHoa.Rows[0].ItemArray);

        lvVanHoa.DataSource = dtVanHoa1;
        lvVanHoa.DataBind();

        dtVanHoa1.Rows.Clear();

        for (int i = 1; i<4; i++)
        {
            dtVanHoa1.Rows.Add(dtVanHoa.Rows[i].ItemArray);
        }
        lvVanHoa1.DataSource = dtVanHoa1;
        lvVanHoa1.DataBind();
        ///////////////////////////////////////

        //Phap Luat
        string urlPhapLuat = "http://dantri.com.vn/skphapluat.rss";
        XmlTextReader readerPhapLuat = new XmlTextReader(urlPhapLuat);

        DataSet dsPhapLuat = new DataSet();
        dsPhapLuat.ReadXml(readerPhapLuat);

        var dtPhapLuat = dsPhapLuat.Tables["item"];
        var dtPhapLuat1 = dtPhapLuat.Clone();
        dtPhapLuat1.Rows.Add(dtPhapLuat.Rows[0].ItemArray);

        lvPhapLuat.DataSource = dtPhapLuat1;
        lvPhapLuat.DataBind();

        dtPhapLuat1.Rows.Clear();

        for (int i = 1; i<4; i++)
        {
            dtPhapLuat1.Rows.Add(dtPhapLuat.Rows[i].ItemArray);
        }
        lvPhapLuat1.DataSource = dtPhapLuat1;
        lvPhapLuat1.DataBind();
        ///////////////////////////////////////

        //Suc Manh
        string urlSucManh = "http://dantri.com.vn/suc-manh-tri-thuc.rss";
        XmlTextReader readerSucManh = new XmlTextReader(urlSucManh);

        DataSet dsSucManh = new DataSet();
        dsSucManh.ReadXml(readerSucManh);

        var dtSucManh = dsSucManh.Tables["item"];
        var dtSucManh1 = dtSucManh.Clone();
        dtSucManh1.Rows.Add(dtSucManh.Rows[0].ItemArray);

        lvSucManh.DataSource = dtSucManh1;
        lvSucManh.DataBind();

        dtSucManh1.Rows.Clear();

        for (int i = 1; i<4; i++)
        {
            dtSucManh1.Rows.Add(dtSucManh.Rows[i].ItemArray);
        }
        lvSucManh1.DataSource = dtSucManh1;
        lvSucManh1.DataBind();
        ///////////////////////////////////////

        //Giai Tri
        string urlGiaiTri = "http://dantri.com.vn/giaitri.rss";
        XmlTextReader readerGiaiTri = new XmlTextReader(urlGiaiTri);

        DataSet dsGiaiTri = new DataSet();
        dsGiaiTri.ReadXml(readerGiaiTri);

        var dtGiaiTri = dsGiaiTri.Tables["item"];
        var dtGiaiTri1 = dtGiaiTri.Clone();
        dtGiaiTri1.Rows.Add(dtGiaiTri.Rows[0].ItemArray);

        lvGiaiTri.DataSource = dtGiaiTri1;
        lvGiaiTri.DataBind();

        dtGiaiTri1.Rows.Clear();

        for (int i = 1; i<4; i++)
        {
            dtGiaiTri1.Rows.Add(dtGiaiTri.Rows[i].ItemArray);
        }
        lvGiaiTri1.DataSource = dtGiaiTri1;
        lvGiaiTri1.DataBind();
        ///////////////////////////////////////
    }
}