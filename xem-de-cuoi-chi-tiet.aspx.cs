﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TLLib;

public partial class choi_de_hieu_chi_tiet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            HtmlMeta meta = new HtmlMeta();
            meta.Name = "description";
            var dv = (DataView)ObjectDataSource1.Select();
            if (dv.Count > 0)
            {

                Page.Title = dv[0]["Title"].ToString();
                meta.Content = dv[0]["Title"].ToString();
                Header.Controls.Add(meta);
                var row = dv[0];

                var strTitle = Server.HtmlDecode(row["Title"].ToString());
                var strDescription = Server.HtmlDecode(row["Title"].ToString());
                var strImageName = Server.HtmlDecode(row["ImagePath"].ToString());

                Header.Controls.Add(meta);
                hdnCID.Value = dv[0]["WatchLaughCategoryID"].ToString();
                hdnImageName.Value = strImageName;
                hdnTitle.Value = strTitle;
                hdnDescription.Value = strDescription;
            }

            string ID = Request.QueryString["xc"];
            if (ID != null)
            {
                new WatchLaugh().WatchLaughUpdateCountViews(ID);
                string UserName = System.Web.HttpContext.Current.User.Identity.Name;
                var oVote = new WatchLaugh_User().WatchLaughUserSelectAll(ID, UserName).DefaultView;
                if (oVote.Count == 1)
                {

                    //btnVote.Enabled = false;

                }
            }
        }
    }
    protected string progressTitle(object input)
    {
        var convertTitle = new ConvertTitle();
        return convertTitle.convertToLowerCase(input.ToString());
    }
    ////protected void btnVote_Click(object sender, EventArgs e)
    ////{
    ////    bool IsLogin = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;

    ////    var dv1 = (DataView)ObjectDataSource1.Select();
    ////    if (dv1.Count > 0)
    ////    {
    ////        if (IsLogin == true)
    ////        {
    ////            string UserName = System.Web.HttpContext.Current.User.Identity.Name;
    ////            string WatchLaughID = dv1[0]["WatchLaughID"].ToString();
    ////            new WatchLaugh_User().WatchLaughUserInsert(WatchLaughID, UserName, "", "", "true");
    ////            new WatchLaugh().WatchLaughUpdateCountVoted(WatchLaughID);
    ////            ListView1.DataBind();
    ////        }
    ////        else
    ////        {
    ////            TLLib.Common.ShowAlert(" Bạn hãy đăng nhập để thực hiện chức năng này");
    ////            Response.Redirect("Login.aspx?ReturnUrl=" + Page.Request.Url.AbsolutePath.ToString());
    ////        }

    ////    }
    ////}
}