﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using ASPSnippets.FaceBookAPI;

public partial class Default2 : System.Web.UI.Page
{
    protected void Login(object sender, EventArgs e)
    {
        FaceBookConnect.Authorize("user_photos,email,user_location,user_birthday", Request.Url.AbsoluteUri.Split('?')[0]);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        FaceBookConnect.API_Key = "312072895644091";
        FaceBookConnect.API_Secret = "4f8e3f2760e9beea6c0d6aa9b8a05a3b";
        if (!IsPostBack)
        {
            if (Request.QueryString["error"] == "access_denied")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('User has denied access.')", true);
                return;
            }

            string code = Request.QueryString["code"];
            if (!string.IsNullOrEmpty(code))
            {
                string data = FaceBookConnect.Fetch(code, "me");
                FaceBookUser faceBookUser = new JavaScriptSerializer().Deserialize<FaceBookUser>(data);
                faceBookUser.pictureurl = string.Format("https://graph.facebook.com/{0}/picture", faceBookUser.id);
                pnlFaceBookUser.Visible = true;
                lblId.Text = faceBookUser.id;
                lblUserName.Text = faceBookUser.username;
                lblName.Text = faceBookUser.name;
                lblEmail.Text = faceBookUser.email;
                lblLink.Text = faceBookUser.link;
                lblLocation.Text = faceBookUser.location.name;
                ProfileImage.ImageUrl = faceBookUser.pictureurl;
                btnLogin.Enabled = false;
            }
        }
    }
}
public class FaceBookUser
{
    public string id { get; set; }
    public string name { get; set; }
    public string username { get; set; }
    public string pictureurl { get; set; }
    public string email { get; set; }
    public string link { get; set; }
    public FaceBookEntity location { get; set; }
}
public class FaceBookEntity
{
    public string id { get; set; }
    public string name { get; set; }
}