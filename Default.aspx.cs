﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using TLLib;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
   
        if (!IsPostBack)
        {
            var video = (DataView)ObjectDataSource8.Select();
            if (video.Count > 0)
            {
                hdnVideoID.Value = video[0]["LearnsCategoryID"].ToString();
                var video1 = new Learns().LearnsSelectAll("1", "6", "", "", hdnVideoID.Value, "true", "", "", "", "", "false", "", "", "", "", "true").DefaultView;
                ListView10.DataSource = video1;
                ListView10.DataBind();
            }

            var quote = (DataView)ObjectDataSource9.Select();
            if (quote.Count > 0)
            {
                hdnQuoteID.Value = quote[0]["LearnsCategoryID"].ToString();
                var quote1 = new Learns().LearnsSelectAll("1", "6", "", "", hdnQuoteID.Value, "true", "", "", "", "", "false", "", "", "", "", "true").DefaultView;
                ListView11.DataSource = quote1;
                ListView11.DataBind();
            }
            var picture = (DataView)ObjectDataSource7.Select();
            if (picture.Count > 0)
            {
                hdnPictureID.Value = picture[0]["LearnsCategoryID"].ToString();
                var picture1 = new Learns().LearnsSelectAll("1", "6", "", "", hdnPictureID.Value, "true", "", "", "", "", "false", "", "", "", "", "true").DefaultView;
                ListView12.DataSource = picture1;
                ListView12.DataBind();
            }

            //Open Popup Game
            //if (Session["ShowPopUp"] == null)
            //{
            //    Session["ShowPopUp"] = "1";
            //    RegisterStartupScript("runtime", "<script type='text/javascript'>$(function(){ $('.more-game:first a').trigger('click'); });</script>");
            //}
        }


        // string url = "http://dantri.com.vn/giao-thong.rss";
        string url = "http://vnexpress.net/rss/oto-xe-may.rss";
        XmlTextReader reader = new XmlTextReader(url);

        DataSet ds = new DataSet();
        ds.ReadXml(reader);

        var dt = ds.Tables["item"];
        var dt1 = dt.Clone();
        dt1.Rows.Add(dt.Rows[0].ItemArray);

        rptRSS.DataSource = dt1;
        rptRSS.DataBind();

        dt1.Rows.Clear();

        for (int i = 1; i < 4; i++)
        {
            dt1.Rows.Add(dt.Rows[i].ItemArray);
        }
        rptRSS1.DataSource = dt1;
        rptRSS1.DataBind();

        dt1.Rows.Clear();

        for (int i = 4; i < 7; i++)
        {
            dt1.Rows.Add(dt.Rows[i].ItemArray);
        }
        rptRSS2.DataSource = dt1;
        rptRSS2.DataBind();
    }
    protected string progressTitle(object input)
    {
        var convertTitle = new ConvertTitle();
        return convertTitle.convertToLowerCase(input.ToString());
    }
}