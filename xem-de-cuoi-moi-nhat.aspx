﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="xem-de-cuoi-moi-nhat.aspx.cs" Inherits="choi_de_cuoi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Xem Để Cười - Tác Phẩm Mới Nhất</title>
    <meta name="Description" content="Xem Để Cười - Tác Phẩm Mới Nhất" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="laughs">
        <div class="container">
            <div class="head-b">
                <p id="site" class="site-node"><a href="xem-de-cuoi.aspx">Xem để cười</a><span>Tác phẩm mới nhất</span></p>
                <div class="box-upload">
                    <ahref='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ?"popuppage/UploadXemDeCuoi.aspx" : "popuppage/Login.aspx?cmd=XDC" %>' data-target="#popupPages" data-toggle="modal" class="upload-a"><span>upload tác phẩm của bạn</span></a>
                    <span class="upn show-desktop">Với mỗi tác phẩm hài (có hoặc không liên quan đến giao thông), bạn sẽ là nhân tố quyết định trong việc xây dựng GOsmart</span>
                    <%--<a href="javascript:void(0);" data-toggle="modal" data-target="#registersite" class="upload-a"><span>upload tác phẩm của bạn</span></a>
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#registerloginsite" class="upload-a"><span>upload tác phẩm của bạn</span></a>--%>
                </div>
            </div>
            <div class="head-menu">
                <div class="row">
                    <div class="col-lg-6">
                        <h2 class="title"><a href="xem-de-cuoi-moi-nhat.aspx">Tác phẩm mới nhất</a></h2>
                    </div>
                    <div class="col-lg-6">
                        <asp:RadioButtonList CssClass="list-filter" ID="radSearch" OnSelectedIndexChanged="radSearch_SelectedIndexChanged" AutoPostBack="true"
                            runat="server" RepeatDirection="Horizontal">
                            <%--<asp:ListItem Value="" Selected="True">Tất Cả</asp:ListItem>--%>
                            <asp:ListItem Value="1">Fun Quote/ Message</asp:ListItem>
                            <asp:ListItem Value="2">Fun picture</asp:ListItem>
                            <asp:ListItem Value="3">Fun clip/ video</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnNoSearch1" runat="server">
                <div class="laughs-ivt">
                    <div class="row">
                        <asp:ListView ID="ListView5" runat="server"
                            DataSourceID="ObjectDataSource5"
                            EnableModelValidation="True">
                            <ItemTemplate>
                                <div class="col-md-6">
                                    <div class="box-articles">
                                        <a class="articles-img bcover" title='<%# Eval("Title") %>' href='<%# progressTitle(Eval("Title")) + "-xc-" + Eval("WatchLaughID") + ".aspx" %>'>
                                            <img id="Img3" alt='<%# Eval("ImagePath") %>' src='<%# "http://www.liveplus.vn/res/watchlaugh/video/thumbs/" + Eval("ImagePath") %>'
                                                visible='<%# string.IsNullOrEmpty( Eval("ImagePath").ToString()) ? false : 
true %>'
                                                runat="server" class="img-responsive" />
                                            <span class="box-text-see show-mobile" style='<%# Eval("Views").ToString() == "0"? "display: none;": ""%>'><%# Eval("Views") %> lượt xem</span>
                                            <span class="mask-video"></span>
                                        </a>
                                        <div class="articles-content articles-h">
                                            <div class="like-box">
                                                <%--<div class="fb-like" data-href='<%# Page.Request.Url.Host.ToString()+ progressTitle(Eval("Title")) + "-xc-" + Eval("WatchLaughID") +".aspx" %>'
                                                    data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                                                </div>--%>
                                                <span class="box-text-see" style='<%# Eval("Views").ToString() == "0"? "display: none;": ""%>'><%# Eval("Views") %> lượt xem</span>
                                            </div>
                                            <p class="line">
                                                Đăng tin: <span><%# Eval("Createby") %></span>
                                            </p>
                                            <p>
                                                Tiêu đề: <span><%# Eval("Title") %></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                    </div>
                </div>
                <asp:ObjectDataSource ID="ObjectDataSource5" runat="server"
                    SelectMethod="WatchLaughSelectNews"
                    TypeName="TLLib.WatchLaugh">
                    <SelectParameters>
                        <asp:Parameter Name="StartRowIndex" Type="String" DefaultValue="1" />
                        <asp:Parameter DefaultValue="2" Name="EndRowIndex" Type="String" />
                        <asp:Parameter Name="Title" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:Parameter DefaultValue="3" Name="WatchLaughCategoryID" Type="String" />
                        <asp:Parameter Name="IsAvailable" Type="String" DefaultValue="true" />
                        <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                        <asp:Parameter Name="IsNew" Type="String" />
                        <asp:Parameter Name="Createby" Type="String" />
                        <asp:Parameter Name="Priority" Type="String" />
                        <asp:Parameter Name="SortByPriority" Type="String" DefaultValue="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <div class="laughs-ivt">
                    <div class="row">
                        <asp:ListView ID="ListView6" runat="server"
                            DataSourceID="ObjectDataSource6"
                            EnableModelValidation="True">
                            <ItemTemplate>
                                <div class="col-md-3 col-md-6">
                                    <div class="box-articles">
                                        <a class="articles-img bcover" title='<%# Eval("Title") %>' href='<%# progressTitle(Eval("Title")) + "-xc-" + Eval("WatchLaughID") + ".aspx" %>'>
                                            <img id="Img3" alt='<%# Eval("ImagePath") %>' src='<%# "http://www.liveplus.vn/res/watchlaugh/picture/" + Eval("ImagePath") %>'
                                                visible='<%# string.IsNullOrEmpty( Eval("ImagePath").ToString()) ? false : 
true %>'
                                                runat="server" class="img-responsive" />
                                            <span class="box-text-see" style='<%# Eval("Views").ToString() == "0"? "display: none;": ""%>'><%# Eval("Views") %> lượt xem</span>

                                        </a>

                                        <div class="articles-content articles-h">
                                           <%-- <div class="like-box">
                                                <div class="fb-like" data-href='<%# Page.Request.Url.Host.ToString()+ progressTitle(Eval("Title")) + "-xc-" + Eval("WatchLaughID") +".aspx" %>'
                                                    data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                                                </div>
                                            </div>--%>
                                            <p class="line">
                                                Đăng tin: <span><%# Eval("Createby") %></span>
                                            </p>
                                            <p>
                                                Tiêu đề: <span><%# Eval("Title") %></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                    </div>
                </div>
                <asp:ObjectDataSource ID="ObjectDataSource6" runat="server"
                    SelectMethod="WatchLaughSelectNews"
                    TypeName="TLLib.WatchLaugh">
                    <SelectParameters>
                        <asp:Parameter Name="StartRowIndex" Type="String" DefaultValue="1" />
                        <asp:Parameter DefaultValue="4" Name="EndRowIndex" Type="String" />
                        <asp:Parameter Name="Title" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:Parameter DefaultValue="2" Name="WatchLaughCategoryID" Type="String" />
                        <asp:Parameter Name="IsAvailable" Type="String" DefaultValue="true" />
                        <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                        <asp:Parameter Name="IsNew" Type="String" />
                        <asp:Parameter Name="Createby" Type="String" />
                        <asp:Parameter Name="Priority" Type="String" />
                        <asp:Parameter Name="SortByPriority" Type="String" DefaultValue="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <div class="laughs-ivt">
                    <div class="row">
                        <asp:ListView ID="ListView7" runat="server"
                            DataSourceID="ObjectDataSource7"
                            EnableModelValidation="True">
                            <ItemTemplate>
                                <div class="col-md-3 col-md-6">
                                    <div class="box-articles">
                                        <a class="articles-img bcover" title='<%# Eval("Title") %>' href='<%# progressTitle(Eval("Title")) + "-xc-" + Eval("WatchLaughID") + ".aspx" %>'>
                                            <img id="Img3" alt='<%# Eval("ImagePath") %>' src='<%# Eval("ImagePath") != DBNull.Value ? "http://www.liveplus.vn/res/watchlaugh/quote/" + Eval("ImagePath") : "http://www.liveplus.vn/Uploads/Background/" + SiteCode.RamdomBackground() %>'
                                                runat="server" class="img-responsive" />
                                            <span class="text-art"><span><strong><%#Eval("WatchLaughQuote") %></strong></span></span>
                                            <span class="box-text-see" style='<%# Eval("Views").ToString() == "0"? "display: none;": ""%>'><%# Eval("Views") %> lượt xem</span></a>
                                        <div class="articles-content articles-h">
                                          <%--  <div class="like-box">
                                                <div class="fb-like" data-href='<%# Page.Request.Url.Host.ToString()+ progressTitle(Eval("Title")) + "-xc-" + Eval("WatchLaughID") +".aspx" %>'
                                                    data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                                                </div>
                                            </div>--%>
                                            <p class="line">
                                                Đăng tin: <span><%# Eval("Createby") %></span>
                                            </p>
                                            <p>
                                                Tiêu đề: <span><%# Eval("Title") %></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>

                    </div>
                </div>
                <asp:ObjectDataSource ID="ObjectDataSource7" runat="server"
                    SelectMethod="WatchLaughSelectNews"
                    TypeName="TLLib.WatchLaugh">
                    <SelectParameters>
                        <asp:Parameter Name="StartRowIndex" Type="String" DefaultValue="1" />
                        <asp:Parameter DefaultValue="4" Name="EndRowIndex" Type="String" />
                        <asp:Parameter Name="Title" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:Parameter DefaultValue="1" Name="WatchLaughCategoryID" Type="String" />
                        <asp:Parameter Name="IsAvailable" Type="String" DefaultValue="true" />
                        <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                        <asp:Parameter Name="IsNew" Type="String" />
                        <asp:Parameter Name="Createby" Type="String" />
                        <asp:Parameter Name="Priority" Type="String" />
                        <asp:Parameter Name="SortByPriority" Type="String" DefaultValue="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </asp:Panel>

            <asp:Panel ID="pnSearch1" runat="server">
                <div class="laughs-ivt">
                    <div class="row">
                        <asp:ListView ID="ListView8" runat="server"
                            DataSourceID="ObjectDataSource8"
                            EnableModelValidation="True">
                            <ItemTemplate>
                                <div class='<%# Eval("WatchLaughCategoryID").ToString() =="3"?"col-md-6": "col-md-3 col-md-6" %>'>
                                    <div class="box-articles">
                                        <a class="articles-img bcover" title='<%# Eval("Title") %>' href='<%# progressTitle(Eval("Title")) + "-xc-" + Eval("WatchLaughID") + ".aspx" %>'>
                                            <img id="Img3" alt='<%# Eval("ImagePath") %>' src='<%# (Eval("WatchLaughCategoryID").ToString() =="1" ? (Eval("ImagePath") != DBNull.Value ? "http://www.liveplus.vn/res/watchlaugh/quote/" : "http://www.liveplus.vn/Uploads/Background/") : Eval("WatchLaughCategoryID").ToString() =="2" ? "http://www.liveplus.vn/res/watchlaugh/picture/": "http://www.liveplus.vn/res/watchlaugh/video/thumbs/") + (Eval("WatchLaughCategoryID").ToString() =="1" ? (Eval("ImagePath") != DBNull.Value ? Eval("ImagePath") : SiteCode.RamdomBackground()) : Eval("ImagePath")) %>'
                                                visible='<%# Eval("WatchLaughCategoryID").ToString() !="1" && string.IsNullOrEmpty( Eval("ImagePath").ToString()) ? false : 
true %>'
                                                runat="server" class="img-responsive" />
                                            <span class='<%# Eval("WatchLaughCategoryID").ToString() =="1"?"text-art": "hidden" %>'><span><strong><%#Eval("WatchLaughQuote") %></strong></span></span>
                                            <span class='<%# Eval("WatchLaughCategoryID").ToString() =="3"?"box-text-see show-mobile": "box-text-see" %>' style='<%# Eval("Views").ToString() == "0"? "display: none;": ""%>'><%# Eval("Views") %> lượt xem</span>
                                            <%# Eval("WatchLaughCategoryID").ToString() =="3"? "<span class='mask-video'></span>" : ""%>   
                                        </a>
                                        <div class="articles-content articles-h">
                                            <div class="like-box">
                                                <%--<div class="fb-like" data-href='<%# Page.Request.Url.Host.ToString()+ progressTitle(Eval("Title")) + "-xc-" + Eval("WatchLaughID") +".aspx" %>'
                                                    data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                                                </div>--%>
                                                <span class='<%# Eval("WatchLaughCategoryID").ToString() =="3"?"box-text-see": "hidden" %>' style='<%# Eval("Views").ToString() == "0"? "display: none;": ""%>'><%# Eval("Views") %> lượt xem</span>
                                            </div>
                                            <p class="line">
                                                Đăng tin: <span><%# Eval("Createby") %></span>
                                            </p>
                                            <p>
                                                Tiêu đề: <span><%# Eval("Title") %></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                    </div>
                </div>
                <asp:ObjectDataSource ID="ObjectDataSource8" runat="server"
                    SelectMethod="WatchLaughSelectNews"
                    TypeName="TLLib.WatchLaugh">
                    <SelectParameters>
                        <asp:Parameter Name="StartRowIndex" Type="String" />
                        <asp:Parameter Name="EndRowIndex" Type="String" />
                        <asp:Parameter Name="Title" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:ControlParameter ControlID="radSearch" DefaultValue="" Name="WatchLaughCategoryID" PropertyName="SelectedValue" Type="String" />
                        <asp:Parameter Name="IsAvailable" Type="String" DefaultValue="true" />
                        <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                        <asp:Parameter Name="IsNew" Type="String" />
                        <asp:Parameter Name="Createby" Type="String" />
                        <asp:Parameter Name="Priority" Type="String" />
                        <asp:Parameter Name="SortByPriority" Type="String" DefaultValue="true" />
                    </SelectParameters>
                </asp:ObjectDataSource>

            </asp:Panel>
        </div>
        <div class="pager">
            <asp:DataPager ID="DataPager2" runat="server" PageSize="24" PagedControlID="ListView8">
                <Fields>
                    <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="numer-paging" CurrentPageLabelCssClass="current" />

                </Fields>
            </asp:DataPager>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphPopup" runat="Server">
</asp:Content>

