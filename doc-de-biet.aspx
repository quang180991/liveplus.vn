﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="doc-de-biet.aspx.cs" Inherits="doc_de_biet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <p class="site-node">Đọc để biết</p>
        <div class="news-main">
            <div class="row">
                <div class="col-md-7">
                    <div class="news-bwrap boxh">
                        <div class="big-news">
                            <a class="news-img" href="#"></a>
                            <div class="news-content">
                                <h3 class="news-name"><a href="#" target="_blank"></a></h3>
                                <p class="date"></p>
                                <div class="description"></div>
                            </div>
                        </div>
                        <asp:ListView ID="rptRSS" runat="server"
                            EnableModelValidation="True" Visible="true">
                            <ItemTemplate>
                                <div id="bigboxs">
                                    <h3 class="big-rss-name"><a href='<%# Eval("link") %>' target="_blank"><%# Eval("title") %></a></h3>
                                    <p class="big-rss-date"><%# Eval("pubDate") %></p>
                                    <div class="big-rss-imgdes"><%# Eval("description") %></div>
                                </div>

                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="newsc">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="newsd boxh">
                                    <div class="row">
                                        <div class="col-xs-6 col-md-12">
                                            <div class="box-newsl">
                                                <a href="#" target="_blank" class="newsl-img">
                                                    <img class="img-responsive" src="assets/images/news-img-1.jpg" alt="" /></a>
                                                <div class="newsl-content">
                                                    <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                    <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-md-12">
                                            <div class="box-newsl">
                                                <a href="#" target="_blank" class="newsl-img">
                                                    <img class="img-responsive" src="assets/images/news-img-1.jpg" alt="" /></a>
                                                <div class="newsl-content">
                                                    <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                    <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="smallboxs">
                                            <asp:ListView ID="rptRSS1" runat="server"
                                                EnableModelValidation="True" Visible="true">
                                                <ItemTemplate>
                                                    <div class="box-rss">
                                                        <div class="box-rss-img"><%# Eval("description") %></div>
                                                        <div class="box-rss-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></div>
                                                        <div class="box-rss-tit"><a href="http://dantri.com.vn">Báo Dân Trí</a></div>
                                                    </div>
                                                </ItemTemplate>
                                                <LayoutTemplate>
                                                    <span runat="server" id="itemPlaceholder" />
                                                </LayoutTemplate>
                                            </asp:ListView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="small-news">
                                    <ul class="news-list">
                                        <asp:ListView ID="rptRSS2" runat="server"
                                            EnableModelValidation="True">
                                            <ItemTemplate>
                                                <li>
                                                    <p class="news-smmall"><a href="http://dantri.com.vn">Báo Dân Trí</a></p>
                                                    <h3 class="small-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></h3>
                                                </li>
                                            </ItemTemplate>
                                            <LayoutTemplate>
                                                <span runat="server" id="itemPlaceholder" />
                                            </LayoutTemplate>
                                        </asp:ListView>
                                        <%-- <li>
                                            <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                            <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                        </li>
                                        <li>
                                            <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                            <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                        </li>
                                        <li>
                                            <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                            <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                        </li>
                                        <li>
                                            <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                            <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                        </li>--%>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="listnews" class="wrapper-group">
            <div class="row">
                <div class="col-xs-6">
                    <div class="newsb-wrap">
                        <h2 class="title-news"><a class="mtc-1" href="doc-de-biet-the-gioi.aspx" target="_blank">thế giới</a></h2>
                        <div class="newsb-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="newsb">
                                        <a class="news-img" href="#">
                                            <img class="img-responsive" src="assets/images/news-img-1.jpg" alt="" /></a>
                                        <div class="news-content">
                                            <h3 class="news-name"><a href="#" target="_blank">Báo Trung Quốc: Quân đội Việt Nam đủ sức ...</a></h3>
                                            <p class="news-smmall"><a href="#" target="_blank">Lao Độngc</a></p>
                                            <div class="description">Tổ chức Giám sát Nhân quyền Human Rights Watch cho biết họ có bằng chứng mới về việc nhóm Nhà…</div>
                                        </div>
                                    </div>
                                    <asp:ListView ID="lvTheGioi" runat="server"
                                        EnableModelValidation="True" Visible="true">
                                        <ItemTemplate>
                                            <div class="box-rss">
                                                <div class="box-rss-img"><%# Eval("description") %></div>
                                                <div class="box-rss-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></div>
                                                <div class="box-rss-tit"><a href="http://dantri.com.vn">Báo Dân Trí</a></div>
                                            </div>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <span runat="server" id="itemPlaceholder" />
                                        </LayoutTemplate>
                                    </asp:ListView>
                                </div>
                                <div class="col-md-4">
                                    <div class="small-news">
                                        <ul class="news-list">
                                            <asp:ListView ID="lvTheGioi1" runat="server"
                                                EnableModelValidation="True">
                                                <ItemTemplate>
                                                    <li>
                                                        <p class="news-smmall"><a href="http://dantri.com.vn">Báo Dân Trí</a></p>
                                                        <h3 class="small-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></h3>
                                                    </li>
                                                </ItemTemplate>
                                                <LayoutTemplate>
                                                    <span runat="server" id="itemPlaceholder" />
                                                </LayoutTemplate>
                                            </asp:ListView>
                                            <%--                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>--%>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="newsb-wrap">
                        <h2 class="title-news"><a class="mtc-2" href="doc-de-biet-xa-hoi.aspx" target="_blank">Xã hội</a></h2>
                        <div class="newsb-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="newsb">
                                        <a class="news-img" href="#">
                                            <img class="img-responsive" src="assets/images/news-img-1.jpg" alt="" /></a>
                                        <div class="news-content">
                                            <h3 class="news-name"><a href="#" target="_blank">Báo Trung Quốc: Quân đội Việt Nam đủ sức ...</a></h3>
                                            <p class="news-smmall"><a href="#" target="_blank">Lao Độngc</a></p>
                                            <div class="description">Tổ chức Giám sát Nhân quyền Human Rights Watch cho biết họ có bằng chứng mới về việc nhóm Nhà…</div>
                                        </div>
                                    </div>
                                    <asp:ListView ID="lvXaHoi" runat="server"
                                        EnableModelValidation="True" Visible="true">
                                        <ItemTemplate>
                                            <div class="box-rss">
                                                <div class="box-rss-img"><%# Eval("description") %></div>
                                                <div class="box-rss-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></div>
                                                <div class="box-rss-tit"><a href="http://dantri.com.vn">Báo Dân Trí</a></div>
                                            </div>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <span runat="server" id="itemPlaceholder" />
                                        </LayoutTemplate>
                                    </asp:ListView>
                                </div>
                                <div class="col-md-4">
                                    <div class="small-news">
                                        <ul class="news-list">
                                            <asp:ListView ID="lvXaHoi1" runat="server"
                                                EnableModelValidation="True">
                                                <ItemTemplate>
                                                    <li>
                                                        <p class="news-smmall"><a href="http://dantri.com.vn">Báo Dân Trí</a></p>
                                                        <h3 class="small-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></h3>
                                                    </li>
                                                </ItemTemplate>
                                                <LayoutTemplate>
                                                    <span runat="server" id="itemPlaceholder" />
                                                </LayoutTemplate>
                                            </asp:ListView>
                                            <%--<li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>--%>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="newsb-wrap">
                        <h2 class="title-news"><a class="mtc-3" href="doc-de-biet-the-thao.aspx" target="_blank">Thể thao</a></h2>
                        <div class="newsb-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="newsb">
                                        <a class="news-img" href="#">
                                            <img class="img-responsive" src="assets/images/news-img-1.jpg" alt="" /></a>
                                        <div class="news-content">
                                            <h3 class="news-name"><a href="#" target="_blank">Báo Trung Quốc: Quân đội Việt Nam đủ sức ...</a></h3>
                                            <p class="news-smmall"><a href="#" target="_blank">Lao Độngc</a></p>
                                            <div class="description">Tổ chức Giám sát Nhân quyền Human Rights Watch cho biết họ có bằng chứng mới về việc nhóm Nhà…</div>
                                        </div>
                                    </div>
                                    <asp:ListView ID="lvTheThao" runat="server"
                                        EnableModelValidation="True" Visible="true">
                                        <ItemTemplate>
                                            <div class="box-rss">
                                                <div class="box-rss-img"><%# Eval("description") %></div>
                                                <div class="box-rss-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></div>
                                                <div class="box-rss-tit"><a href="http://dantri.com.vn">Báo Dân Trí</a></div>
                                            </div>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <span runat="server" id="itemPlaceholder" />
                                        </LayoutTemplate>
                                    </asp:ListView>
                                </div>
                                <div class="col-md-4">
                                    <div class="small-news">
                                        <ul class="news-list">
                                            <asp:ListView ID="lvTheThao1" runat="server"
                                                EnableModelValidation="True">
                                                <ItemTemplate>
                                                    <li>
                                                        <p class="news-smmall"><a href="http://dantri.com.vn">Báo Dân Trí</a></p>
                                                        <h3 class="small-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></h3>
                                                    </li>
                                                </ItemTemplate>
                                                <LayoutTemplate>
                                                    <span runat="server" id="itemPlaceholder" />
                                                </LayoutTemplate>
                                            </asp:ListView>
                                            <%--  <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>--%>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="newsb-wrap">
                        <h2 class="title-news"><a class="mtc-4" href="doc-de-biet-giao-duc.aspx" target="_blank">Giáo dục - khuyến học</a></h2>
                        <div class="newsb-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="newsb">
                                        <a class="news-img" href="#">
                                            <img class="img-responsive" src="assets/images/news-img-1.jpg" alt="" /></a>
                                        <div class="news-content">
                                            <h3 class="news-name"><a href="#" target="_blank">Báo Trung Quốc: Quân đội Việt Nam đủ sức ...</a></h3>
                                            <p class="news-smmall"><a href="#" target="_blank">Lao Độngc</a></p>
                                            <div class="description">Tổ chức Giám sát Nhân quyền Human Rights Watch cho biết họ có bằng chứng mới về việc nhóm Nhà…</div>
                                        </div>
                                    </div>
                                    <asp:ListView ID="lvGiaoDuc" runat="server"
                                        EnableModelValidation="True" Visible="true">
                                        <ItemTemplate>
                                            <div class="box-rss">
                                                <div class="box-rss-img"><%# Eval("description") %></div>
                                                <div class="box-rss-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></div>
                                                <div class="box-rss-tit"><a href="http://dantri.com.vn">Báo Dân Trí</a></div>
                                            </div>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <span runat="server" id="itemPlaceholder" />
                                        </LayoutTemplate>
                                    </asp:ListView>
                                </div>
                                <div class="col-md-4">
                                    <div class="small-news">
                                        <ul class="news-list">
                                            <asp:ListView ID="lvGiaoDuc1" runat="server"
                                                EnableModelValidation="True">
                                                <ItemTemplate>
                                                    <li>
                                                        <p class="news-smmall"><a href="http://dantri.com.vn">Báo Dân Trí</a></p>
                                                        <h3 class="small-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></h3>
                                                    </li>
                                                </ItemTemplate>
                                                <LayoutTemplate>
                                                    <span runat="server" id="itemPlaceholder" />
                                                </LayoutTemplate>
                                            </asp:ListView>
                                            <%--  <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>--%>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="newsb-wrap">
                        <h2 class="title-news"><a class="mtc-5" href="doc-de-biet-tam-long-nhan-ai.aspx" target="_blank">Tấm lòng nhân ái</a></h2>
                        <div class="newsb-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="newsb">
                                        <a class="news-img" href="#">
                                            <img class="img-responsive" src="assets/images/news-img-1.jpg" alt="" /></a>
                                        <div class="news-content">
                                            <h3 class="news-name"><a href="#" target="_blank">Báo Trung Quốc: Quân đội Việt Nam đủ sức ...</a></h3>
                                            <p class="news-smmall"><a href="#" target="_blank">Lao Độngc</a></p>
                                            <div class="description">Tổ chức Giám sát Nhân quyền Human Rights Watch cho biết họ có bằng chứng mới về việc nhóm Nhà…</div>
                                        </div>
                                    </div>
                                    <asp:ListView ID="lvTamLong" runat="server"
                                        EnableModelValidation="True" Visible="true">
                                        <ItemTemplate>
                                            <div class="box-rss">
                                                <div class="box-rss-img"><%# Eval("description") %></div>
                                                <div class="box-rss-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></div>
                                                <div class="box-rss-tit"><a href="http://dantri.com.vn">Báo Dân Trí</a></div>
                                            </div>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <span runat="server" id="itemPlaceholder" />
                                        </LayoutTemplate>
                                    </asp:ListView>
                                </div>
                                <div class="col-md-4">
                                    <div class="small-news">
                                        <ul class="news-list">
                                            <asp:ListView ID="lvTamLong1" runat="server"
                                                EnableModelValidation="True">
                                                <ItemTemplate>
                                                    <li>
                                                        <p class="news-smmall"><a href="http://dantri.com.vn">Báo Dân Trí</a></p>
                                                        <h3 class="small-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></h3>
                                                    </li>
                                                </ItemTemplate>
                                                <LayoutTemplate>
                                                    <span runat="server" id="itemPlaceholder" />
                                                </LayoutTemplate>
                                            </asp:ListView>
                                            <%--   <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>--%>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="newsb-wrap">
                        <h2 class="title-news"><a class="mtc-6" href="doc-de-biet-kinh-doanh.aspx" target="_blank">Kinh Doanh</a></h2>
                        <div class="newsb-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="newsb">
                                        <a class="news-img" href="#">
                                            <img class="img-responsive" src="assets/images/news-img-1.jpg" alt="" /></a>
                                        <div class="news-content">
                                            <h3 class="news-name"><a href="#" target="_blank">Báo Trung Quốc: Quân đội Việt Nam đủ sức ...</a></h3>
                                            <p class="news-smmall"><a href="#" target="_blank">Lao Độngc</a></p>
                                            <div class="description">Tổ chức Giám sát Nhân quyền Human Rights Watch cho biết họ có bằng chứng mới về việc nhóm Nhà…</div>
                                        </div>
                                    </div>
                                    <asp:ListView ID="lvKinhDoanh" runat="server"
                                        EnableModelValidation="True" Visible="true">
                                        <ItemTemplate>
                                            <div class="box-rss">
                                                <div class="box-rss-img"><%# Eval("description") %></div>
                                                <div class="box-rss-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></div>
                                                <div class="box-rss-tit"><a href="http://dantri.com.vn">Báo Dân Trí</a></div>
                                            </div>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <span runat="server" id="itemPlaceholder" />
                                        </LayoutTemplate>
                                    </asp:ListView>
                                </div>
                                <div class="col-md-4">
                                    <div class="small-news">
                                        <ul class="news-list">
                                            <asp:ListView ID="lvKinhDoanh1" runat="server"
                                                EnableModelValidation="True">
                                                <ItemTemplate>
                                                    <li>
                                                        <p class="news-smmall"><a href="http://dantri.com.vn">Báo Dân Trí</a></p>
                                                        <h3 class="small-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></h3>
                                                    </li>
                                                </ItemTemplate>
                                                <LayoutTemplate>
                                                    <span runat="server" id="itemPlaceholder" />
                                                </LayoutTemplate>
                                            </asp:ListView>
                                            <%--   <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>--%>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="newsb-wrap">
                        <h2 class="title-news"><a class="mtc-7" href="doc-de-biet-van-hoa.aspx" target="_blank">Văn hóa</a></h2>
                        <div class="newsb-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="newsb">
                                        <a class="news-img" href="#">
                                            <img class="img-responsive" src="assets/images/news-img-1.jpg" alt="" /></a>
                                        <div class="news-content">
                                            <h3 class="news-name"><a href="#" target="_blank">Báo Trung Quốc: Quân đội Việt Nam đủ sức ...</a></h3>
                                            <p class="news-smmall"><a href="#" target="_blank">Lao Độngc</a></p>
                                            <div class="description">Tổ chức Giám sát Nhân quyền Human Rights Watch cho biết họ có bằng chứng mới về việc nhóm Nhà…</div>
                                        </div>
                                    </div>
                                    <asp:ListView ID="lvVanHoa" runat="server"
                                        EnableModelValidation="True" Visible="true">
                                        <ItemTemplate>
                                            <div class="box-rss">
                                                <div class="box-rss-img"><%# Eval("description") %></div>
                                                <div class="box-rss-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></div>
                                                <div class="box-rss-tit"><a href="http://dantri.com.vn">Báo Dân Trí</a></div>
                                            </div>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <span runat="server" id="itemPlaceholder" />
                                        </LayoutTemplate>
                                    </asp:ListView>
                                </div>
                                <div class="col-md-4">
                                    <div class="small-news">
                                        <ul class="news-list">
                                            <asp:ListView ID="lvVanHoa1" runat="server"
                                                EnableModelValidation="True">
                                                <ItemTemplate>
                                                    <li>
                                                        <p class="news-smmall"><a href="http://dantri.com.vn">Báo Dân Trí</a></p>
                                                        <h3 class="small-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></h3>
                                                    </li>
                                                </ItemTemplate>
                                                <LayoutTemplate>
                                                    <span runat="server" id="itemPlaceholder" />
                                                </LayoutTemplate>
                                            </asp:ListView>
                                            <%--   <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>--%>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="newsb-wrap">
                        <h2 class="title-news"><a class="mtc-8" href="doc-de-biet-phap-luat.aspx" target="_blank">Pháp luật</a></h2>
                        <div class="newsb-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="newsb">
                                        <a class="news-img" href="#">
                                            <img class="img-responsive" src="assets/images/news-img-1.jpg" alt="" /></a>
                                        <div class="news-content">
                                            <h3 class="news-name"><a href="#" target="_blank">Báo Trung Quốc: Quân đội Việt Nam đủ sức ...</a></h3>
                                            <p class="news-smmall"><a href="#" target="_blank">Lao Độngc</a></p>
                                            <div class="description">Tổ chức Giám sát Nhân quyền Human Rights Watch cho biết họ có bằng chứng mới về việc nhóm Nhà…</div>
                                        </div>
                                    </div>
                                    <asp:ListView ID="lvPhapLuat" runat="server"
                                        EnableModelValidation="True" Visible="true">
                                        <ItemTemplate>
                                            <div class="box-rss">
                                                <div class="box-rss-img"><%# Eval("description") %></div>
                                                <div class="box-rss-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></div>
                                                <div class="box-rss-tit"><a href="http://dantri.com.vn">Báo Dân Trí</a></div>
                                            </div>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <span runat="server" id="itemPlaceholder" />
                                        </LayoutTemplate>
                                    </asp:ListView>
                                </div>
                                <div class="col-md-4">
                                    <div class="small-news">
                                        <ul class="news-list">
                                            <asp:ListView ID="lvPhapLuat1" runat="server"
                                                EnableModelValidation="True">
                                                <ItemTemplate>
                                                    <li>
                                                        <p class="news-smmall"><a href="http://dantri.com.vn">Báo Dân Trí</a></p>
                                                        <h3 class="small-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></h3>
                                                    </li>
                                                </ItemTemplate>
                                                <LayoutTemplate>
                                                    <span runat="server" id="itemPlaceholder" />
                                                </LayoutTemplate>
                                            </asp:ListView>
                                            <%--   <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>--%>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="newsb-wrap">
                        <h2 class="title-news"><a class="mtc-9" href="doc-de-biet-suc-manh-tri-thuc.aspx" target="_blank">Súc mạnh tri thức</a></h2>
                        <div class="newsb-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="newsb">
                                        <a class="news-img" href="#">
                                            <img class="img-responsive" src="assets/images/news-img-1.jpg" alt="" /></a>
                                        <div class="news-content">
                                            <h3 class="news-name"><a href="#" target="_blank">Báo Trung Quốc: Quân đội Việt Nam đủ sức ...</a></h3>
                                            <p class="news-smmall"><a href="#" target="_blank">Lao Độngc</a></p>
                                            <div class="description">Tổ chức Giám sát Nhân quyền Human Rights Watch cho biết họ có bằng chứng mới về việc nhóm Nhà…</div>
                                        </div>
                                    </div>
                                    <asp:ListView ID="lvSucManh" runat="server"
                                        EnableModelValidation="True" Visible="true">
                                        <ItemTemplate>
                                            <div class="box-rss">
                                                <div class="box-rss-img"><%# Eval("description") %></div>
                                                <div class="box-rss-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></div>
                                                <div class="box-rss-tit"><a href="http://dantri.com.vn">Báo Dân Trí</a></div>
                                            </div>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <span runat="server" id="itemPlaceholder" />
                                        </LayoutTemplate>
                                    </asp:ListView>
                                </div>
                                <div class="col-md-4">
                                    <div class="small-news">
                                        <ul class="news-list">
                                            <asp:ListView ID="lvSucManh1" runat="server"
                                                EnableModelValidation="True">
                                                <ItemTemplate>
                                                    <li>
                                                        <p class="news-smmall"><a href="http://dantri.com.vn">Báo Dân Trí</a></p>
                                                        <h3 class="small-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></h3>
                                                    </li>
                                                </ItemTemplate>
                                                <LayoutTemplate>
                                                    <span runat="server" id="itemPlaceholder" />
                                                </LayoutTemplate>
                                            </asp:ListView>
                                            <%--   <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>--%>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="newsb-wrap">
                        <h2 class="title-news"><a class="mtc-10" href="doc-de-biet-giai-tri.aspx" target="_blank">Giải trí</a></h2>
                        <div class="newsb-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="newsb">
                                        <a class="news-img" href="#">
                                            <img class="img-responsive" src="assets/images/news-img-1.jpg" alt="" /></a>
                                        <div class="news-content">
                                            <h3 class="news-name"><a href="#" target="_blank">Báo Trung Quốc: Quân đội Việt Nam đủ sức ...</a></h3>
                                            <p class="news-smmall"><a href="#" target="_blank">Lao Độngc</a></p>
                                            <div class="description">Tổ chức Giám sát Nhân quyền Human Rights Watch cho biết họ có bằng chứng mới về việc nhóm Nhà…</div>
                                        </div>
                                    </div>
                                    <asp:ListView ID="lvGiaiTri" runat="server"
                                        EnableModelValidation="True" Visible="true">
                                        <ItemTemplate>
                                            <div class="box-rss">
                                                <div class="box-rss-img"><%# Eval("description") %></div>
                                                <div class="box-rss-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></div>
                                                <div class="box-rss-tit"><a href="http://dantri.com.vn">Báo Dân Trí</a></div>
                                            </div>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <span runat="server" id="itemPlaceholder" />
                                        </LayoutTemplate>
                                    </asp:ListView>
                                </div>
                                <div class="col-md-4">
                                    <div class="small-news">
                                        <ul class="news-list">
                                            <asp:ListView ID="lvGiaiTri1" runat="server"
                                                EnableModelValidation="True">
                                                <ItemTemplate>
                                                    <li>
                                                        <p class="news-smmall"><a href="http://dantri.com.vn">Báo Dân Trí</a></p>
                                                        <h3 class="small-name"><a href='<%# Eval("link") %>'><%# Eval("title") %></a></h3>
                                                    </li>
                                                </ItemTemplate>
                                                <LayoutTemplate>
                                                    <span runat="server" id="itemPlaceholder" />
                                                </LayoutTemplate>
                                            </asp:ListView>
                                            <%--   <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>
                                            <li>
                                                <p class="news-smmall"><a href="#" target="_blank">Báo GTVT</a></p>
                                                <h3 class="small-name"><a href="#" target="_blank">tàu ngầm súy gây thảm họa ở rản sâu 30000</a></h3>
                                            </li>--%>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphPopup" runat="Server">
</asp:Content>

