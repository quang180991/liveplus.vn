﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site-sub.master" AutoEventWireup="true" CodeFile="doc-de-biet-the-gioi.aspx.cs" Inherits="doc_de_biet_chi_tiet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="site">
        <a href="doc-de-biet.aspx">Đọc để biết</a><span>Thế Giới</span>
    </div>
    <h1 class="title tit-icon">
        <img src="assets/images/icon-img-1.png" alt="" />Thế Giới</h1>
    <div class="detail-news">
        <div id="wnews-detail">

            <%-- <div class="news-dbox">
                <a class="news-img" href="#">
                    <img class="img-responsive" src="assets/images/news-img-1.jpg" alt="" /></a>
                <div class="news-content">
                    <h3 class="news-name"><a href="#">Báo Trung Quốc: Quân đội Việt Nam đủ sức giáng trả Trung Quốc</a></h3>
                    <p class="date">VTC News - 2 giờ trước</p>
                    <div class="description">Tổ chức Giám sát Nhân quyền Human Rights Watch cho biết họ có bằng chứng mới về việc nhóm Nhà…</div>
                </div>
            </div>--%>
        </div>
        <div id="wnews">
            <asp:ListView ID="rptRSS" runat="server"
                EnableModelValidation="True" Visible="true">
                <ItemTemplate>
                    <div class="news-ns">
                        <a class="box-rss-name" href='<%# Eval("link") %>' target="_blank"><%# Eval("title") %></a>
                        <p class="date"><%# Eval("pubDate") %></p>
                        <div class="box-rss-img"><%# Eval("description") %></div>
                    </div>

                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>

        </div>
    </div>
    <div class="pager">
        <asp:DataPager ID="DataPager1" runat="server" PageSize="4" PagedControlID="rptRSS">
            <Fields>
                <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="numer-paging" CurrentPageLabelCssClass="current" />
            </Fields>
        </asp:DataPager>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphSibar" runat="Server">
    <div class="boxbar-rand">
        <h2>Xem nhiều nhất</h2>
        <ul class="list-rand">
            <asp:ListView ID="rptRSS1" runat="server"
                EnableModelValidation="True">
                <ItemTemplate>
                    <li><a href='<%# Eval("link") %>'><%# Eval("title") %></a></li>
                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>
        </ul>
    </div>
   
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphPopup" runat="Server">

</asp:Content>

