﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site-sub.master" AutoEventWireup="true" CodeFile="tin-tuc.aspx.cs" Inherits="hoc_de_lam_tin_tuc_chi_tiet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:ListView ID="ListView1" runat="server"
        DataSourceID="ObjectDataSource2"
        EnableModelValidation="True">
        <ItemTemplate>
            <div id="site">
                <span><%# Eval("AggregatedTitle") %></span>
            </div>
            <div class="head-box">
                <div class="head-box-b">
                    <h1 class="title"><%# Eval("AggregatedTitle") %></h1>
                </div>
            </div>
           <%--  <div class="box-like">
                <div class="box-like-b">
                   <div class="fb-like fb-like-1" data-href='<%# Page.Request.Url.AbsolutePath.ToString() %>'
                        data-send="false" data-layout="button_count" data-width="50" data-show-faces="false">
                    </div>
                    <div class="fb-share-button" data-href='<%# Page.Request.Url.AbsolutePath.ToString() %>' data-width="70"></div>

                </div>
            </div>--%>
            <div class="wrap-detail">
                <div class="wrap-text">
                    <%# Eval("Content") %>
                </div>
                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("AggregatedID") %>' />
                <div class="wrap-order">
                    <h2 class="order-title">Tin liên quan</h2>
                    <ul class="list-order">
                        <asp:ListView ID="ListView1" runat="server"
                            DataSourceID="ObjectDataSource3"
                            EnableModelValidation="True">
                            <ItemTemplate>
                                <li><a href='<%# progressTitle(Eval("AggregatedTitle")) + "-a-" + Eval("AggregatedID") +".aspx" %>'><%# Eval("AggregatedTitle") %></a></li>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                    </ul>
                    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server"
                        SelectMethod="AggregatedSameSelectAll" TypeName="TLLib.Aggregated">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="10" Name="RerultCount" Type="String" />
                            <asp:ControlParameter ControlID="hdnID" Name="AggregatedID" PropertyName="Value" Type="String" DefaultValue="" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
                <%-- <div class="wrap-comment">
                    <h2 class="order-title">Bình luận</h2>
                    <div class="fb-comments" data-href='<%# Page.Request.Url.AbsolutePath.ToString() %>' data-width="760"></div>
                </div>--%>
            </div>
        </ItemTemplate>
        <LayoutTemplate>
            <span runat="server" id="itemPlaceholder" />
        </LayoutTemplate>
    </asp:ListView>

    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server"
        SelectMethod="AggregatedSelectOne" TypeName="TLLib.Aggregated">
        <SelectParameters>
            <asp:QueryStringParameter Name="AggregatedID" QueryStringField="a" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphSibar" runat="Server">
    <%-- <div class="box-res box-m">
        <h2><strong>đăng ký tham gia dự án</strong>(dành cho doanh nghiệp)</h2>
        <a class="link-popup" href="javascript:void(0);" data-toggle="modal" data-target="#popupp"></a>
    </div>--%>
    <div class="box-wp" style="margin-top:-5px;">
        <h2 class="title-b">TÌM HIỂU VỀ DỰ ÁN VÀ CÁC CHƯƠNG TRÌNH</h2>
        <div class="studybox">
            <div class="row">
                <div class="col-xs-6 col-md-12">
                    <div class="box-ques ma">
                        <a href="hoc-de-lam-tin-tuc.aspx" class="box-aques">
                            <span class="box-img">
                                <img class="img-responsive" src="assets/images/site-img-1.jpg" alt="" /></span>
                            <span class="box-name">Tin tức</span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-md-12">
                    <div class="box-ques md">
                        <a href="hoc-de-lam.aspx" class="box-aques">
                            <span class="box-img">
                                <img class="img-responsive" src="assets/images/site-img-2.jpg" alt="" /></span>
                            <span class="box-name">Giới thiệu dự án GOsmart</span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-md-12">
                    <div class="box-ques mv">
                        <a href="hoc-de-lam-ISO.aspx" class="box-aques">
                            <span class="box-img">
                                <img class="img-responsive" src="assets/images/site-img-3.jpg" alt="" /></span>
                            <span class="box-name">ISO 39001</span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-md-12">
                    <div class="box-ques mx">
                        <a href="hoc-de-lam-tu-van.aspx" class="box-aques">
                            <span class="box-img">
                                <img class="img-responsive" src="assets/images/site-img-4.jpg" alt="" /></span>
                            <span class="box-name">Tư vấn hệ thống quản lý ATGT</span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-md-12">
                    <div class="box-ques ml">
                        <a href='#' class="box-aques">
                            <span class="box-img">
                                <img class="img-responsive" src="assets/images/site-img-5.jpg" alt="" /></span>
                            <span class="box-name">Đào tạo ATGT cho doanh nghiệp</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphPopup" runat="Server">

    <!-- Modal -->
    <%--   <div class="modal fade popup2" id="popupp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg contest-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h2 class="modal-title" id="myModalLabel">đăng kí thông tin doanh nghiệp</h2>
                </div>
                <div class="modal-body">
                    <fieldset class="send-wrap">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tên công ty</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtFullName" CssClass="form-control" placeholder="-- Tên công ty --" runat="server" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator7" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtFullName"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Địa chỉ</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtAddress" CssClass="form-control" placeholder="-- Địa chỉ --" runat="server" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator5" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtAddress"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Lĩnh vực hoạt động</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtLinhVuc" CssClass="form-control" placeholder="-- Lĩnh Vực Hoạt Động --" runat="server" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator6" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtLinhVuc"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tên người liên hệ</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtName" CssClass="form-control" placeholder="-- Tên người liên hệ --" runat="server" Text=""></asp:TextBox>
                                        <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator2" runat="server"
                                            Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtName"
                                            ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-offset-1 col-sm-4 control-label">Chức vụ</label>
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtChucVu" CssClass="form-control" placeholder="-- Chức vụ --" runat="server" Text=""></asp:TextBox>
                                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator3" runat="server"
                                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtChucVu"
                                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" Text="" placeholder="-- Nhập email --"></asp:TextBox>
                                <asp:RegularExpressionValidator CssClass="lb-error" ID="RegularExpressionValidator1"
                                    runat="server" ValidationGroup="Information" ControlToValidate="txtEmail" ErrorMessage="Sai định dạng email!"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"
                                    ForeColor="Red"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator1" runat="server"
                                    ValidationGroup="Information" ControlToValidate="txtEmail" ErrorMessage="Thông tin bắt buộc!"
                                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Điện thoại</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtPhone" CssClass="form-control" runat="server" Text="" placeholder="-- Điện thoại --"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator4" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtPhone"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <div class="wrap-btn">
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <asp:Button ID="btnUpLoad" OnClick="btnUpLoad_Click" CssClass="btn-send" ValidationGroup="Information" runat="server" Text="đăng ký thông tin" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>

