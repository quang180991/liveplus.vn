﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class site : System.Web.UI.MasterPage
{
    private static readonly Regex RegexBetweenTags = new Regex(@">(?! )\s+", RegexOptions.Compiled);
    private static readonly Regex RegexLineBreaks = new Regex(@"([\n\s])+?(?<= {2,})<", RegexOptions.Compiled);
    public static string RemoveWhitespaceFromHtml(string html)
    {
        html = RegexBetweenTags.Replace(html, ">");
        html = RegexLineBreaks.Replace(html, "<");
        return html.Trim();
    }
    protected override void Render(HtmlTextWriter writer)
    {
        using (HtmlTextWriter htmlwriter = new HtmlTextWriter(new System.IO.StringWriter()))
        {
            base.Render(htmlwriter);
            string html = htmlwriter.InnerWriter.ToString();
            writer.Write(RemoveWhitespaceFromHtml(html));
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {


        //if (!IsPostBack)
        //{
        //    string page = Request.Url.PathAndQuery.ToLower();

        //    string startScript = "<script type='text/javascript'>";
        //    string endScript = "')</script>";
        //    string activePage = "";
        //    if (page.Contains("-s-"))
        //        activePage = "tim-se-thay.aspx";
        //    else if (page.Contains("-ppd-") || page.Contains("-pvd-") || page.Contains("-pqd-") || page.Contains("-pv-") || page.Contains("-pp-") || page.Contains("-pq-") || page.Contains("choi-de-hieu.aspx"))
        //        activePage = "Default.aspx#play-learn";
        //    else if (page.Contains("cong-tac-vien.aspx") || page.Contains("cong-tac-vien-quy-dinh.aspx") || page.Contains("-cb-") || page.Contains("-cq-"))
        //        activePage = "cong-tac-vien.aspx";
        //    else if (page.Contains("hoc-de-lam-tin-tuc.aspx") || page.Contains("hoc-de-lam-tu-van.aspx") || page.Contains("hoat-dong-cong-tac-vien.aspx") || page.Contains("-pl-") || page.Contains("-nf-") || page.Contains("hoc-de-lam-") || page.Contains("hoc-de-lam-ISO.aspx") || page.Contains("hoc-de-lam-dao-tao-doanh-nghiep.aspx") || page.Contains("-dt-"))
        //        activePage = "Default.aspx#students-work";
        //    else if (page.Contains("doc-de-biet.aspx") || page.Contains("doc-de-biet-giao-thong.aspx") || page.Contains("chia-se-kinh-nghiem.aspx") || page.Contains("-rs-") || page.Contains("-rsd-"))
        //        activePage = "Default.aspx#learn-know";
        //    else if (page.Contains("xem-de-cuoi.aspx") || page.Contains("-xc-"))
        //        activePage = "xem-de-cuoi.aspx";

        //    else if (page.Contains("?cd="))
        //        activePage = "financial.aspx";
        //    else if (!page.EndsWith("default.aspx"))
        //        activePage = Path.GetFileName(page);

        //    if (!string.IsNullOrEmpty(activePage))
        //        runScript.InnerHtml = startScript + "changeActiveMenu('" + activePage + endScript;
        //    runScript.InnerHtml += startScript + "changeActiveSubMenu('" + endScript;

        //}
    }
    protected void LoginStatus2_LoggedOut(object sender, EventArgs e)
    {
        Response.Redirect("default.aspx"); ;


    }
    protected string progressTitle(object input)
    {
        var convertTitle = new ConvertTitle();
        return convertTitle.convertToLowerCase(input.ToString());
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        var keyword = txtSearch.Text.Trim();
        Response.Redirect("~/tim-kiem.aspx?q=" + keyword);
    }
}
