﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="lien-he.aspx.cs" Inherits="lien_he" %>

<%@ Register TagPrefix="asp" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Liên hệ</title>
    <meta name="Description" content="Liên hệ" />
    <link href="assets/styles/contact.css" rel="stylesheet" />
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=vi"></script>
    <script src="assets/js/google-maps.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="wrapper-main">
        <div class="container">
            <div id="site">
                <span>Liên hệ</span>
            </div>

            <div class="contact">
                <div class="wrap-map">
                    <div class="map">
                        <input id="start" type="text" value="P803, tòa nhà Thăng Long, 105 Láng Hạ, quận Đống Đa, Hà nội, Việt Nam." />
                        <input id="getdiretion" class="getdiretion" type="button" onclick="calcRoute();"
                            value="Tìm đường đi" />
                        <div class="map-content">
                            <div id="map">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <!--address-->
                        <div class="address-company">
                            <h2>Ban quản lý dự án Go-Smart</h2>
                             <h2 style="text-transform: none;">Trụ sở Hà Nội</h2>
                            <p>Địa chỉ: P803, tòa nhà Thăng Long, 105 Láng Hạ, quận Đống Đa, Hà nội, Việt Nam.</p>
                            <p>Tel:  04-37722892 -  Fax: 04 -37722890</p>
                            <p>Hotline: 0906.260.928</p>
                            <p>Email:<a href="mailto:hotro@liveplus.vn">hotro@liveplus.vn</a> </p>
                        </div>
                        <div class="address-company">
                            <h2 style="text-transform: none;">Chi nhánh Tp.Hồ Chí Minh</h2>
                            <p>Địa chỉ: P410, tòa nhà Alpha, 151 Nguyễn Đình Chiểu, quận 3, Tp.HCM.</p>
                            <p>Hotline: 0906.260.928</p>
                            <p>Email:<a href="mailto:hotro@liveplus.vn">hotro@liveplus.vn</a> </p>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <!--sendmail map-->
                        <div class="send-mail">
                            <h2 class="title-cd">Vui lòng nhập nội dung</h2>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <div class="text-input">
                                        <asp:TextBox CssClass="form-control" ID="txtHoTen" runat="server" placeholder="Nhập họ tên..." Text=""></asp:TextBox>
                                        <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                            ValidationGroup="SendEmail" ControlToValidate="txtHoTen" ErrorMessage="Thông tin bắt buộc!"
                                            ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <div class="text-input">
                                        <div class="form-group">
                                            <asp:TextBox CssClass="form-control" ID="txtEmail" runat="server" placeholder="Nhập email..." Text=""></asp:TextBox>
                                            <asp:RegularExpressionValidator CssClass="lb-error" ID="RegularExpressionValidator1" runat="server" ValidationGroup="SendEmail"
                                                ControlToValidate="txtEmail" ErrorMessage="Sai định dạng email!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator2" runat="server" ValidationGroup="SendEmail"
                                                ControlToValidate="txtEmail" ErrorMessage="Thông tin bắt buộc!" Display="Dynamic"
                                                ForeColor="Red"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="text-input">
                                        <asp:TextBox CssClass="form-control" ID="txtPhone" runat="server" placeholder="Nhập điện thoại..." Text=""></asp:TextBox>
                                        <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator5" Display="Dynamic" runat="server"
                                            ControlToValidate="txtPhone" ValidationGroup="SendEmail" ErrorMessage="Thông tin bắt buộc!"
                                            ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="text-input">
                                        <asp:TextBox CssClass="form-control textnd" ID="txtNoiDung" runat="server" placeholder="Nhập nội dung..." TextMode="MultiLine" Text=""></asp:TextBox>
                                        <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator3" runat="server" ValidationGroup="SendEmail"
                                            Display="Dynamic" ControlToValidate="txtNoiDung" ErrorMessage="Thông tin bắt buộc!"
                                            ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="text-input">
                                        <asp:Label runat="server" ID="lblMessage" ForeColor="red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="text-input">
                                        <asp:Button ID="btGui" CssClass="bnt-button" runat="server" OnClick="btGui_Click" Text="Gửi" ValidationGroup="SendEmail" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphPopup" runat="Server">
</asp:Content>

