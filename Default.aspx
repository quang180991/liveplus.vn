﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" EnableViewState="false" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        body {
            background: #f4f4f4 url(assets/images/bg.png) repeat-x left top;
        }
    </style>
    <meta property="og:type" content="activity" />
    <%--đường link muốn share--%>
    <meta property="og:url" content='http://www.liveplus.vn/' />
    <%--tiêu đề muốn share--%>
    <meta property="og:title" content='Vì bạn còn có ngày mai' />
    <%--hình ảnh muốn share--%>
    <%--mô tả muốn share--%>
    <meta property="og:description" content='Nhằm nâng cao nhận thức an toàn giao thông cho người tham gia giao thông đường bộ, GOsmart tổ chức cuộc thi ảnh “KHOẢNH KHẮC ĐƯỜNG PHỐ - Traffic in Close-up”. Thông qua đó, GOsmart muốn mang đến cho bạn không chỉ có một sân... ' />
    <meta name="thumbnail" content='http://www.liveplus.vn/assets/images/liveplus.jpg' />
    <meta property="og:image" content='http://www.liveplus.vn/assets/images/liveplus.jpg' />
    <%--tên website muốn share--%>
    <meta property="og:site_name" content='http://www.liveplus.vn/' />

    <title>LivePlus</title>
    <meta name="Description" content="LivePlus" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <p class="more-game hidden">
        <a href="javascript:void(0);" data-toggle="modal" data-target="#gameplay">Click để chơi</a>
    </p>
    <div id="banner">
        <ul id="banner-slider">
            <asp:ListView ID="ListView1" runat="server" DataSourceID="ObjectDataSource1" EnableModelValidation="True">
                <ItemTemplate>
                    <li><a href='<%# Eval("Website") %> '>
                        <img class="img-responsive" id="Img1" alt='<%# Eval("FileName") %>' src='<%# "http://www.liveplus.vn/res/advertisement/" + Eval("FileName") %>'
                            visible='<%# string.IsNullOrEmpty( Eval("FileName").ToString()) ? false : 
true %>'
                            runat="server" />
                    </a></li>
                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>
        </ul>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="AdsBannerSelectAll"
            TypeName="TLLib.AdsBanner">
            <SelectParameters>
                <asp:Parameter Name="StartRowIndex" Type="String" />
                <asp:Parameter Name="EndRowIndex" Type="String" />
                <asp:Parameter Name="AdsCategoryID" Type="String" DefaultValue="1" />
                <asp:Parameter Name="CompanyName" Type="String" />
                <asp:Parameter Name="Website" Type="String" />
                <asp:Parameter Name="FromDate" Type="String" />
                <asp:Parameter Name="ToDate" Type="String" />
                <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                <asp:Parameter Name="Priority" Type="String" />
                <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
    <div class="container">
        <div id="marquensroll">
            <ul id="scroller">
                <asp:ListView ID="ListView20" runat="server" DataSourceID="ObjectDataSource12" EnableModelValidation="True">
                    <ItemTemplate>
                        <li>
                            <div class="text-sroll">
                                <span class="text-srolls">
                                    <%# Eval("Content") %></span>
                            </div>
                        </li>
                    </ItemTemplate>
                    <LayoutTemplate>
                        <span runat="server" id="itemPlaceholder" />
                    </LayoutTemplate>
                </asp:ListView>
                <asp:ObjectDataSource ID="ObjectDataSource12" runat="server" SelectMethod="AggregatedSelectAll"
                    TypeName="TLLib.Aggregated">
                    <SelectParameters>
                        <asp:Parameter Name="StartRowIndex" Type="String" />
                        <asp:Parameter Name="EndRowIndex" Type="String" />
                        <asp:Parameter Name="Keyword" Type="String" />
                        <asp:Parameter Name="AggregatedTitle" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:Parameter Name="AggregatedCategoryID" Type="String" DefaultValue="1" />
                        <asp:Parameter Name="Tag" Type="String" />
                        <asp:Parameter DefaultValue="" Name="IsShowOnHomePage" Type="String" />
                        <asp:Parameter Name="IsHot" Type="String" />
                        <asp:Parameter Name="IsNew" Type="String" />
                        <asp:Parameter Name="FromDate" Type="String" />
                        <asp:Parameter Name="ToDate" Type="String" />
                        <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                        <asp:Parameter DefaultValue="" Name="Priority" Type="String" />
                        <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </ul>
        </div>
    </div>
    <div id="worsk-wrap">
        <div class="container">
            <h2 class="title-link">
                <a href="javarscript:void(0);" style="cursor: none;"><span>xem để cười</span></a></h2>
            <div class="group-wrap">
                <div class="watch-flaughs">
                    <!-- Nav tabs -->
                    <ul class="nav-control" role="tablist" id="myTab">
                        <li><a href="#tabs1" role="tab">hài nhất tuần</a></li>
                        <li><a href="#tabs2" role="tab">tác phẩm mới nhất</a></li>
                        <li><a href="#tabs3" role="tab">nội quy</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-contents">
                        <h3 class="tit-tabs show-mobile">
                            <a href="#tabs1">hài nhất tuần</a></h3>
                        <div class="tab-panes" id="tabs1">
                            <div class="wrap-content">
                                <div class="jcarousel-wrapper">
                                    <div id="jaca1" class="jcarousel">
                                        <ul class="list-tabs">
                                            <asp:ListView ID="ListView13" runat="server" DataSourceID="ObjectDataSource10" EnableModelValidation="True">
                                                <ItemTemplate>
                                                    <li>
                                                        <div class='<%# Eval("WatchLaughCategoryID").ToString() =="1"?"box-articles textb" : "box-articles" %>'>
                                                            <a class="articles-img bcover" href='<%# progressTitle(Eval("Title")) + "-xc-" + Eval("WatchLaughID") + ".aspx" %>'>
                                                                <img id="Img3" alt='<%# Eval("ImagePath") %>' src='<%# (Eval("WatchLaughCategoryID").ToString() =="1" ? (Eval("ImagePath") != DBNull.Value ? "http://www.liveplus.vn/res/watchlaugh/quote/" : "http://www.liveplus.vn/Uploads/Background/") : Eval("WatchLaughCategoryID").ToString() =="2" ? "http://www.liveplus.vn/res/watchlaugh/picture/": "http://www.liveplus.vn/res/watchlaugh/video/thumbs/") + (Eval("WatchLaughCategoryID").ToString() =="1" ? (Eval("ImagePath") != DBNull.Value ? Eval("ImagePath") : SiteCode.RamdomBackground()) : Eval("ImagePath")) %>'
                                                                    visible='<%# Eval("WatchLaughCategoryID").ToString() !="1" && string.IsNullOrEmpty( Eval("ImagePath").ToString()) ? false : 
true %>'
                                                                    runat="server" class="img-responsive" />
                                                                <span class='<%# Eval("WatchLaughCategoryID").ToString() =="1"?"text-art": "hidden" %>'>
                                                                    <strong>
                                                                        <%#Eval("WatchLaughQuote") %></strong></span> <span class="box-text-see" style='<%# Eval("Views").ToString() == "0"? "display: none;": ""%>'>
                                                                            <%# Eval("Views") +" lượt xem"%>
                                                                        </span>
                                                                <%# Eval("WatchLaughCategoryID").ToString() =="3"? "<span class='mask-video'></span>" : ""%>
                                                            </a>
                                                            <div class="articles-content articles-h">
                                                                <%-- <div class="like-box">
                                                                    <div class="fb-like" data-href='<%# Page.Request.Url.Host.ToString()+ progressTitle(Eval("Title")) + "-xc-" + Eval("WatchLaughID") +".aspx" %>'
                                                                        data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                                                                    </div>
                                                                </div>--%>
                                                                <p class="line">
                                                                    Đăng tin: <span>
                                                                        <%# Eval("Createby") %></span>
                                                                </p>
                                                                <p>
                                                                    Tiêu đề: <span>
                                                                        <%# Eval("Title") %></span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                </EmptyDataTemplate>
                                                <LayoutTemplate>
                                                    <span runat="server" id="itemPlaceholder" />
                                                </LayoutTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                    <a href="#" class="jcarousel-control-prev">&lsaquo;</a> <a href="#" class="jcarousel-control-next">&rsaquo;</a>
                                </div>
                                <p class="more-d">
                                    <a href="xem-de-cuoi.aspx"><span>vào trang</span></a>
                                </p>
                            </div>
                        </div>
                        <asp:ObjectDataSource ID="ObjectDataSource10" runat="server" SelectMethod="WatchLaughSelectViews"
                            TypeName="TLLib.WatchLaugh">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="1" Name="StartRowIndex" Type="String" />
                                <asp:Parameter DefaultValue="30" Name="EndRowIndex" Type="String" />
                                <asp:Parameter Name="Title" Type="String" />
                                <asp:Parameter Name="Description" Type="String" />
                                <asp:Parameter Name="WatchLaughCategoryID" Type="String" />
                                <asp:Parameter Name="IsAvailable" Type="String" DefaultValue="true" />
                                <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                                <asp:Parameter Name="IsNew" Type="String" />
                                <asp:Parameter Name="Createby" Type="String" />
                                <asp:Parameter Name="Priority" Type="String" />
                                <asp:Parameter Name="SortByPriority" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <h3 class="tit-tabs show-mobile">
                            <a href="#tabs2">tác phẩm mới nhất</a></h3>
                        <div class="tab-panes " id="tabs2">
                            <div class="wrap-content">
                                <div class="jcarousel-wrapper">
                                    <div id="jaca2" class="jcarousel">
                                        <ul class="list-tabs">
                                            <asp:ListView ID="ListView14" runat="server" DataSourceID="ObjectDataSource11" EnableModelValidation="True">
                                                <ItemTemplate>
                                                    <li>
                                                        <div class='<%# Eval("WatchLaughCategoryID").ToString() =="1"?"box-articles textb" : "box-articles" %>'>
                                                            <a class="articles-img bcover" href='<%# progressTitle(Eval("Title")) + "-xc-" + Eval("WatchLaughID") + ".aspx" %>'>
                                                                <img id="Img3" alt='<%# Eval("ImagePath") %>' src='<%# (Eval("WatchLaughCategoryID").ToString() =="1" ? (Eval("ImagePath") != DBNull.Value ? "http://www.liveplus.vn/res/watchlaugh/quote/" : "http://www.liveplus.vn/Uploads/Background/") : Eval("WatchLaughCategoryID").ToString() =="2" ? "http://www.liveplus.vn/res/watchlaugh/picture/": "http://www.liveplus.vn/res/watchlaugh/video/thumbs/") + (Eval("WatchLaughCategoryID").ToString() =="1" ? (Eval("ImagePath") != DBNull.Value ? Eval("ImagePath") : SiteCode.RamdomBackground()) : Eval("ImagePath")) %>'
                                                                    visible='<%# Eval("WatchLaughCategoryID").ToString() !="1" && string.IsNullOrEmpty( Eval("ImagePath").ToString()) ? false : 
true %>'
                                                                    runat="server" class="img-responsive" />
                                                                <span class='<%# Eval("WatchLaughCategoryID").ToString() =="1"?"text-art": "hidden" %>'>
                                                                    <strong>
                                                                        <%#Eval("WatchLaughQuote") %></strong></span> <span class="box-text-see" style='<%# Eval("Views").ToString() == "0"? "display: none;": ""%>'>
                                                                            <%# Eval("Views") +" lượt xem"%>
                                                                        </span>
                                                                <%# Eval("WatchLaughCategoryID").ToString() =="3"? "<span class='mask-video'></span>" : ""%>
                                                            </a>
                                                            <div class="articles-content articles-h">
                                                                <%-- <div class="like-box">
                                                                    <div class="fb-like" data-href='<%#  Page.Request.Url.Host.ToString()+ progressTitle(Eval("Title")) + "-xc-" + Eval("WatchLaughID") +".aspx"  %>'
                                                                        data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                                                                    </div>
                                                                </div>--%>
                                                                <p class="line">
                                                                    Đăng tin: <span>
                                                                        <%# Eval("Createby") %></span>
                                                                </p>
                                                                <p>
                                                                    Tiêu đề: <span>
                                                                        <%# Eval("Title") %></span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                </EmptyDataTemplate>
                                                <LayoutTemplate>
                                                    <span runat="server" id="itemPlaceholder" />
                                                </LayoutTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                    <a href="#" class="jcarousel-control-prev">&lsaquo;</a> <a href="#" class="jcarousel-control-next">&rsaquo;</a>
                                </div>
                                <p class="more-d">
                                    <a href="xem-de-cuoi.aspx"><span>vào trang</span></a>
                                </p>
                            </div>
                        </div>
                        <asp:ObjectDataSource ID="ObjectDataSource11" runat="server" SelectMethod="WatchLaughSelectNews"
                            TypeName="TLLib.WatchLaugh">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="1" Name="StartRowIndex" Type="String" />
                                <asp:Parameter DefaultValue="30" Name="EndRowIndex" Type="String" />
                                <asp:Parameter Name="Title" Type="String" />
                                <asp:Parameter Name="Description" Type="String" />
                                <asp:Parameter Name="WatchLaughCategoryID" Type="String" />
                                <asp:Parameter Name="IsAvailable" Type="String" DefaultValue="true" />
                                <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                                <asp:Parameter Name="IsNew" Type="String" />
                                <asp:Parameter Name="Createby" Type="String" />
                                <asp:Parameter Name="Priority" Type="String" />
                                <asp:Parameter Name="SortByPriority" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <h3 class="tit-tabs show-mobile">
                            <a href="#tabs3">nội quy</a></h3>
                        <div class="tab-panes" id="tabs3">
                            <div class="wrap-content">
                                <asp:ListView ID="ListView2" runat="server" DataSourceID="ObjectDataSource2" EnableModelValidation="True">
                                    <ItemTemplate>
                                        <div class="wrap-w">
                                            <%# Eval("Content") %>
                                        </div>
                                    </ItemTemplate>
                                    <LayoutTemplate>
                                        <span runat="server" id="itemPlaceholder" />
                                    </LayoutTemplate>
                                </asp:ListView>
                                <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="AggregatedSelectAll"
                                    TypeName="TLLib.Aggregated">
                                    <SelectParameters>
                                        <asp:Parameter Name="StartRowIndex" Type="String" DefaultValue="1" />
                                        <asp:Parameter Name="EndRowIndex" Type="String" DefaultValue="1" />
                                        <asp:Parameter Name="Keyword" Type="String" />
                                        <asp:Parameter Name="AggregatedTitle" Type="String" />
                                        <asp:Parameter Name="Description" Type="String" />
                                        <asp:Parameter Name="AggregatedCategoryID" Type="String" DefaultValue="2" />
                                        <asp:Parameter Name="Tag" Type="String" />
                                        <asp:Parameter DefaultValue="" Name="IsShowOnHomePage" Type="String" />
                                        <asp:Parameter Name="IsHot" Type="String" />
                                        <asp:Parameter Name="IsNew" Type="String" />
                                        <asp:Parameter Name="FromDate" Type="String" />
                                        <asp:Parameter Name="ToDate" Type="String" />
                                        <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                                        <asp:Parameter DefaultValue="" Name="Priority" Type="String" />
                                        <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                                <p class="more-d">
                                    <a href="xem-de-cuoi.aspx"><span>vào trang</span></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-works">
                    <div class="works-bb">
                        <div class="works-bc">
                            <div class="works-bl">
                                <div class="works-br">
                                    <div class="works-bt">
                                        <div class="works-head">
                                            <h3>Gửi tác phẩm</h3>
                                            <p>
                                                GO XEM ĐỂ CƯỜI
                                            </p>
                                        </div>
                                        <div class="works-content">
                                            Với mỗi tác phẩm hài (có hoặc không liên quan đến giao thông), bạn sẽ là nhân tố quyết định trong việc xây dựng GOsmart
                                        </div>
                                        <p class="more">  <a href='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ?"popuppage/UploadXemDeCuoi.aspx" : "popuppage/Login.aspx?cmd=XDC" %>' data-target="#popupPages" data-toggle="modal">Gửi tác phẩm</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="adver">
            <div class="adver-1">
                <asp:ListView ID="ListView3" runat="server" DataSourceID="ObjectDataSource3" EnableModelValidation="True">
                    <ItemTemplate>
                        <img class="img-responsive" id="Img1" alt='<%# Eval("FileName") %>' src='<%# "http://www.liveplus.vn/res/advertisement/" + Eval("FileName") %>'
                            visible='<%# string.IsNullOrEmpty( Eval("FileName").ToString()) ? false : 
true %>'
                            runat="server" />
                    </ItemTemplate>
                    <LayoutTemplate>
                        <span runat="server" id="itemPlaceholder" />
                    </LayoutTemplate>
                </asp:ListView>
                <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" SelectMethod="AdsBannerSelectAll"
                    TypeName="TLLib.AdsBanner">
                    <SelectParameters>
                        <asp:Parameter Name="StartRowIndex" Type="String" DefaultValue="1" />
                        <asp:Parameter Name="EndRowIndex" Type="String" DefaultValue="1" />
                        <asp:Parameter Name="AdsCategoryID" Type="String" DefaultValue="2" />
                        <asp:Parameter Name="CompanyName" Type="String" />
                        <asp:Parameter Name="Website" Type="String" />
                        <asp:Parameter Name="FromDate" Type="String" />
                        <asp:Parameter Name="ToDate" Type="String" />
                        <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                        <asp:Parameter Name="Priority" Type="String" />
                        <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
            <div class="adver-2">
                <asp:ListView ID="ListView4" runat="server" DataSourceID="ObjectDataSource4" EnableModelValidation="True">
                    <ItemTemplate>
                        <img class="img-responsive" id="Img1" alt='<%# Eval("FileName") %>' src='<%# "http://www.liveplus.vn/res/advertisement/" + Eval("FileName") %>'
                            visible='<%# string.IsNullOrEmpty( Eval("FileName").ToString()) ? false : 
true %>'
                            runat="server" />
                    </ItemTemplate>
                    <LayoutTemplate>
                        <span runat="server" id="itemPlaceholder" />
                    </LayoutTemplate>
                </asp:ListView>
                <asp:ObjectDataSource ID="ObjectDataSource4" runat="server" SelectMethod="AdsBannerSelectAll"
                    TypeName="TLLib.AdsBanner">
                    <SelectParameters>
                        <asp:Parameter Name="StartRowIndex" Type="String" DefaultValue="1" />
                        <asp:Parameter Name="EndRowIndex" Type="String" DefaultValue="1" />
                        <asp:Parameter Name="AdsCategoryID" Type="String" DefaultValue="3" />
                        <asp:Parameter Name="CompanyName" Type="String" />
                        <asp:Parameter Name="Website" Type="String" />
                        <asp:Parameter Name="FromDate" Type="String" />
                        <asp:Parameter Name="ToDate" Type="String" />
                        <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                        <asp:Parameter Name="Priority" Type="String" />
                        <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </div>
    </div>
    <div id="play-learn">
        <div class="container">
            <h2 class="title-link">
                <a href="javarscript:void(0);" style="cursor: none;"><span>chơi để hiểu</span></a></h2>
            <div class="group-wrap">
                <div id="commenttext" class="comment-video">
                    <div data-id="#video1" class="commnet-wrap">
                        <asp:HiddenField ID="hdnPictureID" runat="server" />
                        <asp:ListView ID="ListView7" runat="server" DataSourceID="ObjectDataSource7" EnableModelValidation="True">
                            <ItemTemplate>
                                <h2 class="title-1">
                                    <strong><%# Eval("LearnsCategoryName") %></strong>Kỳ
                                    <%# Eval("Periodic") %></h2>
                                <div class="description show-desktop">
                                    <%# TLLib.Common.SplitSummary(Eval("ContentEn").ToString(),1000) %>
                                </div>
                                <div class="description show-mobile">
                                    <div class="text-mobile">
                                        <%# TLLib.Common.SplitSummary(Eval("ContentEn").ToString(),1000) %>
                                    </div>
                                </div>
                                <p class="more-see">
                                    <a href='<%# progressTitle(Eval("LearnsCategoryName")) + "-pp-" + Eval("LearnsCategoryID") +".aspx" %>'>xem thể lệ </a>
                                </p>
                                <p class="more-game">
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#gameplay">Click để chơi</a>
                                </p>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                    </div>
                    <asp:ObjectDataSource ID="ObjectDataSource7" runat="server" SelectMethod="LearnsCategorySelectAllFirst"
                        TypeName="TLLib.LearnsCategory">
                        <SelectParameters>
                            <asp:Parameter Name="ParentID1" Type="String" DefaultValue="2" />
                            <asp:Parameter Name="parentID" Type="Int32" DefaultValue="2" />
                            <asp:Parameter Name="increaseLevelCount" Type="Int32" DefaultValue="1" />
                            <asp:Parameter Name="IsShowOnMenu" Type="String" DefaultValue="" />
                            <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <div data-id="#video2" class="commnet-wrap">
                        <asp:HiddenField ID="hdnVideoID" runat="server" />
                        <asp:ListView ID="ListView8" runat="server" DataSourceID="ObjectDataSource8" EnableModelValidation="True">
                            <ItemTemplate>
                                <h2 class="title-1">
                                    <strong><%# Eval("LearnsCategoryName") %></strong>Kỳ
                                    <%# Eval("Periodic") %></h2>
                                <div class="description show-desktop">
                                    <%# TLLib.Common.SplitSummary(Eval("ContentEn").ToString(),1000) %>
                                </div>
                                <div class="description show-mobile">
                                    <div class="text-mobile">
                                        <%# TLLib.Common.SplitSummary(Eval("ContentEn").ToString(),1000) %>
                                    </div>
                                </div>
                                <p class="more-see">
                                    <a href='<%# progressTitle(Eval("LearnsCategoryName")) + "-pv-" + Eval("LearnsCategoryID") +".aspx" %>'>xem thể lệ </a>
                                </p>
                                <p class="more-game">
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#gameplay">Click để chơi</a>
                                </p>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                    </div>
                    <asp:ObjectDataSource ID="ObjectDataSource8" runat="server" SelectMethod="LearnsCategorySelectAllFirst"
                        TypeName="TLLib.LearnsCategory">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="3" Name="ParentID1" Type="String" />
                            <asp:Parameter Name="parentID" Type="Int32" DefaultValue="3" />
                            <asp:Parameter Name="increaseLevelCount" Type="Int32" DefaultValue="1" />
                            <asp:Parameter Name="IsShowOnMenu" Type="String" DefaultValue="" />
                            <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <div data-id="#video3" class="commnet-wrap">
                        <asp:HiddenField ID="hdnQuoteID" runat="server" />
                        <asp:ListView ID="ListView9" runat="server" DataSourceID="ObjectDataSource9" EnableModelValidation="True">
                            <ItemTemplate>
                                <h2 class="title-1">
                                    <strong><%# Eval("LearnsCategoryName") %></strong>Kỳ
                                    <%# Eval("Periodic") %></h2>
                                <div class="description show-desktop">
                                    <%# TLLib.Common.SplitSummary(Eval("ContentEn").ToString(),1000) %>
                                </div>
                                <div class="description show-mobile">
                                    <div class="text-mobile">
                                        <%# TLLib.Common.SplitSummary(Eval("ContentEn").ToString(),1000) %>
                                    </div>
                                </div>
                                <p class="more-see">
                                    <a href='<%# progressTitle(Eval("LearnsCategoryName")) + "-pq-" + Eval("LearnsCategoryID") +".aspx" %>'>xem thể lệ </a>
                                </p>
                                <p class="more-game">
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#gameplay">Click để chơi</a>
                                </p>
                                </div>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                    </div>
                    <asp:ObjectDataSource ID="ObjectDataSource9" runat="server" SelectMethod="LearnsCategorySelectAllFirst"
                        TypeName="TLLib.LearnsCategory">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="1" Name="ParentID1" Type="String" />
                            <asp:Parameter Name="parentID" Type="Int32" DefaultValue="1" />
                            <asp:Parameter Name="increaseLevelCount" Type="Int32" DefaultValue="1" />
                            <asp:Parameter Name="IsShowOnMenu" Type="String" DefaultValue="" />
                            <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
                <div class="wrap-video">
                    <!-- Nav tabs -->
                    <ul class="nav-control" role="tablist" id="tabsVideo">
                        <li class="md"><a href="#video1" role="tab"><span class="name-v">Bình chọn <strong>ẢNH</strong> dự thi<span
                            class="icon-v"></span></span></a></li>
                        <li class="mv" style="display: none"><a href="#video2" role="tab"><span class="name-v">Bình chọn <strong>VIDEO</strong> dự thi<span
                            class="icon-v"></span></span></a></li>
                        <li class="mx" style="display: none"><a href="#video3" role="tab"><span class="name-v">Bình chọn <strong>THÔNG
                            ĐIỆP</strong> dự thi<span class="icon-v"></span></span></a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-contents">
                        <h3 class="tit-video show-mobile">
                            <a class="md" href="#video1"><span>Bình chọn <strong>ẢNH</strong> dự thi</span></a></h3>
                        <div class="tab-panes" id="video1">
                            <div class="wrap-content">
                                <div class="row">
                                    <asp:ListView ID="ListView12" runat="server" EnableModelValidation="True">
                                        <ItemTemplate>
                                            <div class="col-xs-6 col-md-4 box-v">
                                                <div class="box-articles">
                                                    <div class="articles-content articles-h">
                                                        <p class="line">
                                                            Tác giả: <span>
                                                                <%# Eval("Name") %></span>
                                                        </p>
                                                        <p>
                                                            Chủ đề: <span>
                                                                <%# Eval("Title") %></span>
                                                        </p>
                                                    </div>
                                                    <a class="articles-img bcover" href='<%# progressTitle(Eval("Title")) + "-ppd-" + Eval("LearnsID") +".aspx" %>'>
                                                        <img id="Img2" alt='<%# Eval("ImagePath") %>' src='<%#  "http://www.liveplus.vn/res/learns/picture/" + Eval("ImagePath") %>' visible='<%# string.IsNullOrEmpty( Eval("ImagePath").ToString()) ? false : 
true %>'
                                                            runat="server" class="img-responsive" />
                                                        <span class="box-text"><span class="text-1" style='<%# Eval("Voted").ToString() == "0"? "display: none;": ""%>'>
                                                            <strong>
                                                                <%# Eval("Voted")%></strong> bình chọn</span> <span class="text-2" style='<%# Eval("Views").ToString() == "0"? "display: none;": ""%>'>
                                                                    <strong>
                                                                        <%# Eval("Views")%></strong> lượt xem</span> </span></a>
                                                    <div class="articles-content articles-b">
                                                        <a href='<%# progressTitle(Eval("Title")) + "-ppd-" + Eval("LearnsID") +".aspx" %>'
                                                            class="link-com" style="margin-right: 85px;">Bình chọn</a>
                                                        <%-- <div class="like-box">
                                                            <a name="fb_share" type="button_count" expr:share_url="data:post.url" share_url='<%# Page.Request.Url.Host.ToString() + ""+ progressTitle(Eval("Title")) + "-ppd-" + Eval("LearnsID") +".aspx"  %>'>Chia Sẻ</a>

                                                             <div class="fb-like" data-href='<%# Page.Request.Url.Host.ToString() + ""+ progressTitle(Eval("Title")) + "-ppd-" + Eval("LearnsID") +".aspx"  %>'
                                                                data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                                                            </div>
                                                        </div>--%>
                                                        <div class="clr">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <span runat="server" id="itemPlaceholder" />
                                        </LayoutTemplate>
                                    </asp:ListView>
                                </div>
                            </div>
                        </div>
                        <h3 class="tit-video show-mobile" style="display: none  !important;">
                            <a class="mv" href="#video2"><span>Bình chọn <strong>VIDEO</strong> dự thi</span></a></h3>
                        <div class="tab-panes" id="video2">
                            <div class="wrap-content">
                                <div class="row">
                                    <asp:ListView ID="ListView10" runat="server" EnableModelValidation="True">
                                        <ItemTemplate>
                                            <div class="col-xs-6 col-md-4 box-v">
                                                <div class="box-articles">
                                                    <div class="articles-content articles-h">
                                                        <p class="line">
                                                            Tác giả: <span>
                                                                <%# Eval("Name") %></span>
                                                        </p>
                                                        <p>
                                                            Chủ đề: <span>
                                                                <%# Eval("Title") %></span>
                                                        </p>
                                                    </div>
                                                    <a class="articles-img bcover" href='<%# progressTitle(Eval("Title")) + "-pvd-" + Eval("LearnsID") +".aspx" %>'>
                                                        <img id="Img2" alt='<%# Eval("ImagePath") %>' src='<%#  "http://www.liveplus.vn/res/learns/video/thumbs/" + Eval("ImagePath") %>'
                                                            visible='<%# string.IsNullOrEmpty( Eval("ImagePath").ToString()) ? false : 
true %>'
                                                            runat="server" class="img-responsive" />
                                                        <span class="mask-video"></span><span class="box-text"><span class="text-1" style='<%# Eval("Voted").ToString() == "0"? "display: none;": ""%>'>
                                                            <strong>
                                                                <%# Eval("Voted")%></strong> bình chọn</span> <span class="text-2" style='<%# Eval("Views").ToString() == "0"? "display: none;": ""%>'>
                                                                    <strong>
                                                                        <%# Eval("Views")%></strong> lượt xem</span> </span></a>
                                                    <div class="articles-content articles-b">
                                                        <a href='<%# progressTitle(Eval("Title")) + "-pvd-" + Eval("LearnsID") +".aspx" %>'
                                                            class="link-com" style="margin-right: 85px;">Bình chọn</a>
                                                        <%-- <div class="like-box">
                                                            <a name="fb_share" type="button_count" expr:share_url="data:post.url" share_url='<%# Page.Request.Url.Host.ToString() + ""+ progressTitle(Eval("Title")) + "-pvd-" + Eval("LearnsID") +".aspx"  %>'>Chia Sẻ</a>

                                                            <div class="fb-like" data-href='<%# Page.Request.Url.Host.ToString() + ""+ progressTitle(Eval("Title")) + "-pqd-" + Eval("LearnsID") +".aspx"  %>'
                                                                data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                                                            </div>
                                                        </div>--%>
                                                        <div class="clr">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <span runat="server" id="itemPlaceholder" />
                                        </LayoutTemplate>
                                    </asp:ListView>
                                </div>
                            </div>
                        </div>
                        <h3 class="tit-video show-mobile" style="display: none !important;">
                            <a class="mx" href="#video3"><span>Bình chọn <strong>THÔNG ĐIỆP</strong> dự thi</span></a></h3>
                        <div class="tab-panes" id="video3">
                            <div class="wrap-content">
                                <div class="row">
                                    <asp:ListView ID="ListView11" runat="server" EnableModelValidation="True">
                                        <ItemTemplate>
                                            <div class="col-xs-6 col-md-4 box-v">
                                                <div class="box-articles">
                                                    <div class="articles-content articles-h">
                                                        <p class="line">
                                                            Tác giả: <span>
                                                                <%# Eval("Name") %></span>
                                                        </p>
                                                        <p>
                                                            Chủ đề: <span>
                                                                <%# Eval("Title") %></span>
                                                        </p>
                                                    </div>
                                                    <a class="articles-img bcover" href='<%# progressTitle(Eval("Title")) + "-pqd-" + Eval("LearnsID") +".aspx" %>'>
                                                        <img id="Img2" alt='<%# Eval("ImagePath") %>' src='<%# string.IsNullOrEmpty( Eval("ImagePath").ToString())?"assets/images/logo.png":  "http://www.liveplus.vn/res/learns/quote/" + Eval("ImagePath") %>'
                                                            runat="server" class="img-responsive" />
                                                        <span class="text-art"><span><strong>
                                                            <%# Eval("LearnsQuote") %></strong></span> </span><span class="box-text"><span class="text-1"
                                                                style='<%# Eval("Voted").ToString() == "0"? "display: none;": ""%>'><strong>
                                                                    <%# Eval("Voted")%></strong> bình chọn</span> <span class="text-2" style='<%# Eval("Views").ToString() == "0"? "display: none;": ""%>'>
                                                                        <strong>
                                                                            <%# Eval("Views")%></strong> lượt xem</span> </span></a>
                                                    <div class="articles-content articles-b">
                                                        <a href='<%# progressTitle(Eval("Title")) + "-pqd-" + Eval("LearnsID") +".aspx" %>'
                                                            class="link-com" style="margin-right: 85px;">Bình chọn</a>
                                                        <%--<div class="like-box">
                                                            <a name="fb_share" type="button_count" expr:share_url="data:post.url" share_url='<%# Page.Request.Url.Host.ToString() + ""+ progressTitle(Eval("Title")) + "-pqd-" + Eval("LearnsID") +".aspx"  %>'>Chia Sẻ</a>

                                                            <div class="fb-like" data-href='<%# Page.Request.Url.Host.ToString() + ""+ progressTitle(Eval("Title")) + "-pvd-" + Eval("LearnsID") +".aspx"  %>'
                                                                data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                                                            </div>
                                                        </div>--%>
                                                        <div class="clr">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <span runat="server" id="itemPlaceholder" />
                                        </LayoutTemplate>
                                    </asp:ListView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="learn-know">
        <div class="container">
            <h2 class="title-link">
                <a href="javarscript:void(0);" style="cursor: none;"><span>đọc để biết</span></a></h2>
            <div class="group-wrap">
                <div class="row">
                    <div class="col-md-9">
                        <div class="learn-box">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="big-news">
                                        <a class="news-img" href="#" target="_blank">
                                            <img class="img-responsive" src="assets/images/news-img-1.jpg" alt="" /></a>
                                        <div class="news-content">
                                            <h3 class="news-name">
                                                <a href="#" target="_blank">title</a>
                                            </h3>
                                            <p class="date">
                                            </p>
                                            <div class="description">
                                            </div>
                                        </div>
                                    </div>
                                    <asp:ListView ID="rptRSS" runat="server" EnableModelValidation="True">
                                        <ItemTemplate>
                                            <div id="bigboxs">
                                                <div class="big-rss-imgdes">
                                                    <%# Eval("description") %>
                                                </div>
                                                <div class="big-rss-date">
                                                    <%# Eval("pubDate") %>
                                                </div>
                                                <div class="big-rss-name">
                                                    <a href='<%# Eval("link") %>' target="_blank">
                                                        <%# Eval("title") %></a>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <span runat="server" id="itemPlaceholder" />
                                        </LayoutTemplate>
                                    </asp:ListView>
                                </div>
                                <div class="col-md-2">
                                    <div class="small-news">
                                        <ul class="news-list">
                                            <asp:ListView ID="rptRSS1" runat="server" EnableModelValidation="True">
                                                <ItemTemplate>
                                                    <li>
                                                        <p class="news-smmall">
                                                            <a href="http://vnexpress.net/">VnExpress</a>
                                                            <%--<a href="http://dantri.com.vn/giao-thong.htm">Báo Dân Trí</a>--%>
                                                        </p>
                                                        <h3 class="small-name">
                                                            <a href='<%# Eval("link") %>' target="_blank">
                                                                <%# Eval("title") %></a></h3>
                                                    </li>
                                                </ItemTemplate>
                                                <LayoutTemplate>
                                                    <span runat="server" id="itemPlaceholder" />
                                                </LayoutTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="know-box show-desktop">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="box-newsl">
                                            <a href="#" class="newsl-img" target="_blank"></a>
                                            <div class="newsl-content">
                                                <p class="news-smmall">
                                                    <a href="#" target="_blank"></a>
                                                </p>
                                                <h3 class="small-name">
                                                    <a href="#" target="_blank">title</a>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="box-newsl">
                                            <a href="#" class="newsl-img" target="_blank"></a>
                                            <div class="newsl-content">
                                                <p class="news-smmall">
                                                    <a href="#" target="_blank"></a>
                                                </p>
                                                <h3 class="small-name">
                                                    <a href="#" target="_blank">title</a>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="box-newsl">
                                            <a href="#" class="newsl-img" target="_blank"></a>
                                            <div class="newsl-content">
                                                <p class="news-smmall">
                                                    <a href="#" target="_blank"></a>
                                                </p>
                                                <h3 class="small-name">
                                                    <a href="#" target="_blank">title</a>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="smallboxs">
                                        <asp:ListView ID="rptRSS2" runat="server" EnableModelValidation="True">
                                            <ItemTemplate>
                                                <div class="box-rss">
                                                    <div class="box-rss-img">
                                                        <%# Eval("description") %>
                                                    </div>
                                                    <div class="box-rss-name">
                                                        <a href='<%# Eval("link") %>'>
                                                            <%# Eval("title") %></a>
                                                    </div>
                                                    <div class="box-rss-tit">
                                                        <a href="http://vnexpress.net/">VnExpress</a>
                                                        <%-- <a href="http://dantri.com.vn">Báo Dân Trí</a>--%>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                            <LayoutTemplate>
                                                <span runat="server" id="itemPlaceholder" />
                                            </LayoutTemplate>
                                        </asp:ListView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="box-experience">
                            <h3 class="title-2">Bạn đọc chia sẻ - Trải nghiệm</h3>
                            <ul class="list-experience">
                                <asp:ListView ID="ListView5" runat="server" DataSourceID="ObjectDataSource5" EnableModelValidation="True">
                                    <ItemTemplate>
                                        <li>
                                            <h4 class="experience-name">
                                                <a href='<%# progressTitle(Eval("ReadersShareTitle")) + "-rsd-" + Eval("ReadersShareID") +".aspx" %>'>
                                                    <%# Eval("ReadersShareTitle") %>
                                                </a>
                                            </h4>
                                            <div class="description">
                                                <%# TLLib.Common.SplitSummary(Eval("Description").ToString(),100) %>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                    <LayoutTemplate>
                                        <span runat="server" id="itemPlaceholder" />
                                    </LayoutTemplate>
                                </asp:ListView>
                                <asp:ObjectDataSource ID="ObjectDataSource5" runat="server" SelectMethod="ReadersShareSelectAll"
                                    TypeName="TLLib.ReadersShare">
                                    <SelectParameters>
                                        <asp:Parameter Name="StartRowIndex" Type="String" DefaultValue="1" />
                                        <asp:Parameter Name="EndRowIndex" Type="String" DefaultValue="3" />
                                        <asp:Parameter Name="Keyword" Type="String" />
                                        <asp:Parameter Name="ReadersShareTitle" Type="String" />
                                        <asp:Parameter Name="Description" Type="String" />
                                        <asp:Parameter Name="ReadersShareCategoryID" Type="String" />
                                        <asp:Parameter Name="Tag" Type="String" />
                                        <asp:Parameter Name="IsShowOnHomePage" Type="String" DefaultValue="true" />
                                        <asp:Parameter Name="IsHot" Type="String" />
                                        <asp:Parameter Name="IsNew" Type="String" />
                                        <asp:Parameter Name="FromDate" Type="String" />
                                        <asp:Parameter Name="ToDate" Type="String" />
                                        <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                                        <asp:Parameter Name="Priority" Type="String" />
                                        <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="students-work">
        <div class="container">
            <h2 class="title-link">
                <a href="javarscript:void(0);" style="cursor: none;"><span>học để làm</span></a></h2>
            <div class="group-wrap">
                <div class="box-col newline">
                    <div class="box-sw">
                        <asp:ListView ID="ListView6" runat="server" DataSourceID="ObjectDataSource6" EnableModelValidation="True">
                            <ItemTemplate>
                                <a href='<%# progressTitle(Eval("NewsFeaturedTitle")) + "-nf-" + Eval("NewsFeaturedID") + ".aspx" %>'
                                    class="box-a"><span class="mask-box"></span><span class="icon-hot">
                                        <img class="img-responsive" src="assets/images/icon-hot.png" alt="" /></span>
                                    <span class="box-img">
                                        <img class="img-responsive" id="Img1" alt='' src='<%# "http://www.liveplus.vn/res/newsfeatured/" + Eval("ImageName") %>'
                                            visible='<%# string.IsNullOrEmpty( Eval("ImageName").ToString()) ? false : 
true %>'
                                            runat="server" /></span> <span class="box-name">
                                                <%# Eval("NewsFeaturedTitle") %>
                                            </span></a>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                        <asp:ObjectDataSource ID="ObjectDataSource6" runat="server" SelectMethod="NewsFeaturedSelectAll"
                            TypeName="TLLib.NewsFeatured">
                            <SelectParameters>
                                <asp:Parameter Name="StartRowIndex" Type="String" DefaultValue="1" />
                                <asp:Parameter Name="EndRowIndex" Type="String" DefaultValue="1" />
                                <asp:Parameter Name="Keyword" Type="String" />
                                <asp:Parameter Name="NewsFeaturedTitle" Type="String" />
                                <asp:Parameter Name="Description" Type="String" />
                                <asp:Parameter Name="NewsFeaturedCategoryID" Type="String" />
                                <asp:Parameter Name="Tag" Type="String" />
                                <asp:Parameter DefaultValue="true" Name="IsShowOnHomePage" Type="String" />
                                <asp:Parameter Name="IsHot" Type="String" />
                                <asp:Parameter Name="IsNew" Type="String" />
                                <asp:Parameter Name="FromDate" Type="String" />
                                <asp:Parameter Name="ToDate" Type="String" />
                                <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                                <asp:Parameter Name="Priority" Type="String" />
                                <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </div>
                </div>
                <div class="box-col md">
                    <div class="box-sw">
                        <a href="hoc-de-lam.aspx" class="box-a box-sc"><span class="box-img">
                            <img class="img-responsive" src="assets/images/site-img-2.jpg" alt="Giới thiệu dự án Go Smart" /></span>
                            <span class="box-name">Giới thiệu dự án GOsmart</span> </a>
                    </div>
                </div>
                <div class="box-col mv">
                    <div class="box-sw">
                        <a href="hoc-de-lam-ISO.aspx" class="box-a box-sc"><span class="box-img">
                            <img class="img-responsive" src="assets/images/site-img-3.jpg" alt="ISO 39001" /></span>
                            <span class="box-name">ISO 39001</span> </a>
                    </div>
                </div>
                <div class="box-col mx">
                    <div class="box-sw">
                        <a href="hoc-de-lam-tu-van.aspx" class="box-a box-sc"><span class="box-img">
                            <img class="img-responsive" src="assets/images/site-img-4.jpg" alt="Tư vấn quản lý hệ thống ATGT" /></span>
                            <span class="box-name">Tư vấn hệ thống quản lý ATGT</span> </a>
                    </div>
                </div>
                <div class="box-col ml">
                    <div class="box-sw">
                        <a href="#" class="box-a box-sc"><span class="box-img">
                            <img class="img-responsive" src="assets/images/site-img-5.jpg" alt="Đào tạo ATGT cho doanh nghiệp" /></span>
                            <span class="box-name">Đào tạo ATGT cho doanh nghiệp</span> </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphPopup" runat="Server">
    <!-- Modal -->
    <div class="modal fade popupgame" id="gameplay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="false">
        <div class="modal-dialog  modal-lg">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <div class="box-l">
                    <div class="box-r">
                        <div class="box-bb">
                            <div class="box-bl">
                                <div class="box-br">
                                    <div class="logo-game">
                                        <img src="assets/images/logo.gif" alt="" />
                                    </div>
                                    <div class="wrap-text-game">
                                        <asp:ListView ID="ListView15" runat="server"
                                            DataSourceID="ObjectDataSource13"
                                            EnableModelValidation="True">
                                            <ItemTemplate>
                                                <h1 class="title-7"><%# Eval("AggregatedTitle") %></h1>
                                                <%# Eval("Description") %>
                                            </ItemTemplate>
                                            <LayoutTemplate>
                                                <span runat="server" id="itemPlaceholder" />
                                            </LayoutTemplate>
                                        </asp:ListView>
                                        <asp:ObjectDataSource ID="ObjectDataSource13" runat="server"
                                            SelectMethod="AggregatedSelectAll" TypeName="TLLib.Aggregated">
                                            <SelectParameters>
                                                <asp:Parameter Name="StartRowIndex" Type="String" />
                                                <asp:Parameter Name="EndRowIndex" Type="String" />
                                                <asp:Parameter Name="Keyword" Type="String" />
                                                <asp:Parameter Name="AggregatedTitle" Type="String" />
                                                <asp:Parameter Name="Description" Type="String" />
                                                <asp:Parameter Name="AggregatedCategoryID" Type="String" DefaultValue="16" />
                                                <asp:Parameter Name="Tag" Type="String" />
                                                <asp:Parameter DefaultValue="" Name="IsShowOnHomePage" Type="String" />
                                                <asp:Parameter Name="IsHot" Type="String" />
                                                <asp:Parameter Name="IsNew" Type="String" />
                                                <asp:Parameter Name="FromDate" Type="String" />
                                                <asp:Parameter Name="ToDate" Type="String" />
                                                <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                                                <asp:Parameter DefaultValue="" Name="Priority" Type="String" />
                                                <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                                            </SelectParameters>
                                        </asp:ObjectDataSource>
                                    </div>
                                    <p class="more-gamesp">
                                        <a class='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "hidden" : "" %>'
                                            data-toggle="modal" data-target='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "" : "#registerlogingames" %>'
                                            href="javascript:void(0);">tham gia trò chơi</a> <a class='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "" : "hidden" %>'
                                                href='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "game-play.aspx" : "javascript:void(0);" %>'>tham gia trò chơi </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="clientScript" runat="Server">
  
</asp:Content>
