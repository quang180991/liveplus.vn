﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site-sub.master" AutoEventWireup="true" CodeFile="doc-de-biet-chi-tiet.aspx.cs" Inherits="doc_de_biet_chi_tiet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="site">
        <a href="doc-de-biet.aspx">Đọc để biết</a><span>Pháp luật</span>
    </div>
    <h1 class="title tit-icon">
        <img src="assets/images/icon-img-1.png" alt="" />pháp luật</h1>
    <div class="detail-news">
        <div id="wnews-detail">
         
           <%-- <div class="news-dbox">
                <a class="news-img" href="#">
                    <img class="img-responsive" src="assets/images/news-img-1.jpg" alt="" /></a>
                <div class="news-content">
                    <h3 class="news-name"><a href="#">Báo Trung Quốc: Quân đội Việt Nam đủ sức giáng trả Trung Quốc</a></h3>
                    <p class="date">VTC News - 2 giờ trước</p>
                    <div class="description">Tổ chức Giám sát Nhân quyền Human Rights Watch cho biết họ có bằng chứng mới về việc nhóm Nhà…</div>
                </div>
            </div>--%>
        </div>
        <div id="wnews">
            <asp:ListView ID="rptRSS" runat="server"
                EnableModelValidation="True" Visible="true">
                <ItemTemplate>
                    <div class="news-ns">
                        <a class="box-rss-name" href='<%# Eval("link") %>' target="_blank"><%# Eval("title") %></a>
                        <p class="date"><%# Eval("pubDate") %></p>
                        <div class="box-rss-img"><%# Eval("description") %></div>
                    </div>

                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphSibar" runat="Server">
    <div class="boxbar-rand">
        <h2>Xem nhiều nhất</h2>
        <ul class="list-rand">
            <asp:ListView ID="rptRSS1" runat="server"
                EnableModelValidation="True">
                <ItemTemplate>
                    <li><a href='<%# Eval("link") %>'><%# Eval("title") %></a></li>
                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>
        </ul>
    </div>
    <div class="boxbar-list">
        <div class="box-newsg">
            <h3 class="news-name"><a href="#">Xác định chất lạ được vớt cùng thi thể chị Huyền</a></h3>
            <p class="news-smmall"><a href="#">Lao Động</a></p>
            <div class="description">Tổ chức Giám sát Nhân quyền Human Rights Watch cho biết họ có bằng chứng mới về việc nhóm Nhà…</div>
        </div>
        <div class="box-newsg">
            <h3 class="news-name"><a href="#">Xác định chất lạ được vớt cùng thi thể chị Huyền</a></h3>
            <p class="news-smmall"><a href="#">Lao Động</a></p>
            <div class="description">Tổ chức Giám sát Nhân quyền Human Rights Watch cho biết họ có bằng chứng mới về việc nhóm Nhà…</div>
        </div>
        <div class="box-newsg">
            <h3 class="news-name"><a href="#">Xác định chất lạ được vớt cùng thi thể chị Huyền</a></h3>
            <p class="news-smmall"><a href="#">Lao Động</a></p>
            <div class="description">Tổ chức Giám sát Nhân quyền Human Rights Watch cho biết họ có bằng chứng mới về việc nhóm Nhà…</div>
        </div>
    </div>
    
                                <p class="more-game">
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#gameplay">Click để chơi</a>
                                </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphPopup" runat="Server">
    <!-- Modal -->
    <div class="modal fade popupgame" id="gameplay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog  modal-lg">
        <div class="modal-content">
             <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
             <div class="box-l">
                <div class="box-r">
                    <div class="box-bb">
                        <div class="box-bl">
                            <div class="box-br">
                                <div class="logo-game"><img src="assets/images/logo.gif" alt=""/></div>
                                <div class="wrap-text-game">
                                    <h3 class="title-7">THAM GIA TRÒ CHƠI, rinh ngay quà lớn từ chương trình</h3>
                                    <p class="node-game">Trong vòng 5 phút, chỉ với 5 câu hỏi ngẫu nhiên từ hệ thống, bạn có thể tích lũy được kiến thức giao thông, và có cơ hội nhận giải thưởng giá trị.</p>
                                    <p class="thi1"><a href="#">Xem thể lệ &amp; giải thưởng</a></p>
                                    <div style="text-align: center;"><img src="assets/images/images-game.jpg" alt=""/></div>
                                    
                                </div>
                                <p class="more-gamesp"><a href="game-play.aspx">tham gia trò chơi</a></p>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        </div>
      </div>
    </div>
</asp:Content>

