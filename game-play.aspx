﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true"
    CodeFile="game-play.aspx.cs" Inherits="game_play" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="wrapper-main">
        <div class="container">
            <div id="site">
                <a href="cong-tac-vien.aspx">Chơi để hiểu</a><span>Game</span>
            </div>
            <div class="head-game">
                <div class="row">
                    <div class="col-sm-6">
                        <h3>
                            Thời gian trò chơi sẽ được tính khi bạn click vào nút này</h3>
                    </div>
                    <div class="col-sm-6">
                        <div id="startgame" class="more-games">
                            <div class="btn-starts">
                                <a href="javascript:void(0);">bắt đầu chơi</a> <span class="dshow">đang chơi</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="head-clock">
                <div class="wrap-block">
                    <div id="clockBox">
                        <span id="mm">5</span> <span id="ss">0</span>
                    </div>
                </div>
                <div id="time-ms">
                    <asp:TextBox ID="txtMM" CssClass="time-mm" runat="server"></asp:TextBox>
                    <asp:TextBox ID="txtSS" CssClass="time-ss" runat="server"></asp:TextBox>
                </div>
                <h4>
                    Đồng hồ đếm ngược</h4>
            </div>
            <div id="wrapgame">
                <asp:ListView ID="lstGames" runat="server" DataSourceID="odsGames" EnableModelValidation="True">
                    <ItemTemplate>
                        <li>
                            <div class="game-text">
                                <h3 class="game-title">
                                    <span class="game-num"></span>
                                    <%--<%# Eval("GamesTitle")%>--%>
                                    <asp:HiddenField ID="hdnGamesID" Value='<%# Eval("GamesID") %>' runat="server" />
                                    <asp:HiddenField ID="hdnGamesTitleEn" Value='<%# Eval("GamesTitleEn") %>' runat="server" />
                                </h3>
                                <div class="game-contents">
                                    <%# Eval("Content")%>
                                </div>
                            </div>
                            <div class="anwer-game">
                                <ul class="list-radio">
                                    <li class='<%# string.IsNullOrEmpty(Eval("Tag").ToString()) ? "hidden" : "" %>'>
                                        <div class="radio-check">
                                            <asp:RadioButton ID="rdbAnswer1" CssClass="radio-checks" runat="server" GroupName='<%# "question-" + Eval("GamesID") %>' />
                                            <asp:HiddenField ID="hdnAnswer1" Value="1" runat="server" />
                                            <asp:Label ID="lblAnswer1" AssociatedControlID="rdbAnswer1" runat="server">
                                                <span class="i-content"><%# Eval("Tag") %></span>
                                            </asp:Label>
                                        </div>
                                    </li>
                                    <li class='<%# string.IsNullOrEmpty(Eval("TagEn").ToString()) ? "hidden" : "" %>'>
                                        <div class="radio-check">
                                            <asp:RadioButton ID="rdbAnswer2" CssClass="radio-checks" runat="server" GroupName='<%# "question-" + Eval("GamesID") %>' />
                                            <asp:HiddenField ID="hdnAnswer2" Value="2" runat="server" />
                                            <asp:Label ID="lblAnswer2" AssociatedControlID="rdbAnswer2" runat="server">
                                                <span class="i-content"><%# Eval("TagEn")%></span>
                                            </asp:Label>
                                        </div>
                                    </li>
                                    <li class='<%# string.IsNullOrEmpty(Eval("MetaTittle").ToString()) ? "hidden" : "" %>'>
                                        <div class="radio-check">
                                            <asp:RadioButton ID="rdbAnswer3" CssClass="radio-checks" runat="server" GroupName='<%# "question-" + Eval("GamesID") %>' />
                                            <asp:HiddenField ID="hdnAnswer3" Value="3" runat="server" />
                                            <asp:Label ID="lblAnswer3" AssociatedControlID="rdbAnswer3" runat="server">
                                                <span class="i-content"><%# Eval("MetaTittle")%></span>
                                            </asp:Label>
                                        </div>
                                    </li>
                                    <li class='<%# string.IsNullOrEmpty(Eval("MetaDescription").ToString()) ? "hidden" : "" %>'>
                                        <div class="radio-check">
                                            <asp:RadioButton ID="rdbAnswer4" CssClass="radio-checks" runat="server" GroupName='<%# "question-" + Eval("GamesID") %>' />
                                            <asp:HiddenField ID="hdnAnswer4" Value="4" runat="server" />
                                            <asp:Label ID="lblAnswer4" AssociatedControlID="rdbAnswer4" runat="server">
                                                <span class="i-content"><%# Eval("MetaDescription")%></span>
                                            </asp:Label>
                                        </div>
                                    </li>
                                    <li class='<%# string.IsNullOrEmpty(Eval("MetaTittleEn").ToString()) ? "hidden" : "" %>'>
                                        <div class="radio-check">
                                            <asp:RadioButton ID="rdbAnswer5" CssClass="radio-checks" runat="server" GroupName='<%# "question-" + Eval("GamesID") %>' />
                                            <asp:HiddenField ID="hdnAnswer5" Value="5" runat="server" />
                                            <asp:Label ID="lblAnswer5" AssociatedControlID="rdbAnswer5" runat="server">
                                                <span class="i-content"><%# Eval("MetaTittleEn")%></span>
                                            </asp:Label>
                                            <%--<label>
                                                <%# Eval("MetaTittleEn")%></label>--%>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ItemTemplate>
                    <LayoutTemplate>
                        <div class="jcarousel-wrapper">
                            <div id="game-list" class="jcarousel">
                                <ul>
                                    <span runat="server" id="itemPlaceholder" />
                                </ul>
                            </div>
                        </div>
                    </LayoutTemplate>
                </asp:ListView>
                <asp:ObjectDataSource ID="odsGames" runat="server" SelectMethod="GamesSelectAll1"
                    TypeName="TLLib.Games">
                    <SelectParameters>
                        <asp:Parameter Name="StartRowIndex" Type="String" />
                        <asp:Parameter Name="EndRowIndex" Type="String" />
                        <asp:Parameter Name="Keyword" Type="String" />
                        <asp:Parameter Name="GamesTitle" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:Parameter Name="GamesCategoryID" Type="String" />
                        <asp:Parameter Name="Tag" Type="String" />
                        <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                        <asp:Parameter Name="IsHot" Type="String" />
                        <asp:Parameter Name="IsNew" Type="String" />
                        <asp:Parameter Name="FromDate" Type="String" />
                        <asp:Parameter Name="ToDate" Type="String" />
                        <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                        <asp:Parameter Name="Priority" Type="String" />
                        <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <div id="ques-btn">
                    <div class="box-ques">
                        <a class="btn-link" href="#"><span>câu tiếp theo</span></a>
                        <%--<a class="result" href="javascript:volid(0)">
                            Kết quả</a>--%>
                        <asp:Button ID="btnResults" CssClass="result" runat="server" Text="Kết quả" OnClick="btnResults_Click" />
                        <span class="mask-ques">câu cuối</span> <span class="end-time">hết thời gian</span>
                    </div>
                </div>
            </div>
            <%--<div id="wrapgame">
                <div class="jcarousel-wrapper">
                    <div id="game-list" class="jcarousel">
                        <ul>
                            <li>
                                <div class="game-text">
                                    <img src="assets/images/ques-game-1.jpg" alt="" />
                                </div>
                                <div class="anwer-game">
                                    <ul class="list-radio">
                                        <li>
                                            <div class="radio-check">
                                                <asp:RadioButton ID="RadioButton1" runat="server" GroupName="question-1" />
                                                <asp:Label ID="Label1" AssociatedControlId="RadioButton1" runat="server">Biển 1</asp:Label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="radio-check">
                                                <asp:RadioButton ID="RadioButton2" runat="server" GroupName="question-1" />
                                                <label>Biển 1</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="radio-check">
                                                <asp:RadioButton ID="RadioButton3" runat="server" GroupName="question-1" />
                                                <label>Biển 1</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="radio-check">
                                                <asp:RadioButton ID="RadioButton4" runat="server" GroupName="question-1" />
                                                <label>Biển 1</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="radio-check">
                                                <asp:RadioButton ID="RadioButton5" runat="server" GroupName="question-1" />
                                                <label>Biển 1</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div class="game-text">
                                    <img src="assets/images/ques-game-1.jpg" alt="" />
                                </div>
                                <div class="anwer-game">
                                    <ul class="list-radio">
                                        <li>
                                            <div class="radio-check">
                                                <asp:RadioButton ID="RadioButton1" runat="server" GroupName="question-1" />
                                                <label>Biển 1</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="radio-check">
                                                <asp:RadioButton ID="RadioButton2" runat="server" GroupName="question-1" />
                                                <label>Biển 1</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="radio-check">
                                                <asp:RadioButton ID="RadioButton3" runat="server" GroupName="question-1" />
                                                <label>Biển 1</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="radio-check">
                                                <asp:RadioButton ID="RadioButton4" runat="server" GroupName="question-1" />
                                                <label>Biển 1</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="ques-btn">
                    <div class="box-ques">
                        <a class="btn-link" href="#">câu tiếp theo</a><a class="result" href="javascript:volid(0)">Kết
                            quả</a>
                    </div>
                </div>
            </div>--%>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphPopup" runat="Server">
    <script type="text/javascript">
        (function ($) {
            $(function () {
                if ($('#game-list').size() == 1) {
                    $('#game-list > ul > li:last').addClass("last");
                    var jcarousel = $('#game-list').jcarousel();
                    jcarousel.on('jcarousel:reload jcarousel:create', function () {
                        var width = jcarousel.innerWidth();
                        jcarousel.jcarousel('items').css('width', width + 'px');
                    }).jcarousel();
                    $('#ques-btn .btn-link').on('jcarouselcontrol:active', function () {
                        $(this).removeClass('inactive');
                    }).on('jcarouselcontrol:inactive', function () {
                        $(this).addClass('inactive');
                    }).jcarouselControl({
                        target: '+=1'
                    });
                    $("#ques-btn .btn-link").click(function () {
                        if ($(this).hasClass("inactive")) {
                            $(this).hide();
                            $(this).parents(".box-ques").find(".result").show();
                        }
                    });
                    jcarousel.on('jcarousel:targetin', 'li', function () {
                        jcarousel.jcarousel('items').removeClass('active activeh');
                        $(this).prev().addClass('active');
                        $(this).addClass('activeh');
                        var sss = 0;
                        var $thisj = $(this);
                        $("#game-list .active").find("input:radio").each(function () {
                            if ($(this).is(":checked")) {
                                sss = sss + 1;
                            } else {
                                sss = sss;
                            }
                        });
                        if (sss == 0) {
                            var indexa = $("#game-list .active").index();
                            jcarousel.jcarousel('scroll', -1);
                        }
                        //                        else {
                        //                            $("#game-list").height($(this).height());
                        //                            var topsrol = $("#wrapgame").offset().top;
                        //                            $("html, body").animate({ scrollTop: topsrol }, 100);
                        //                        }
                        $("#game-list").height($(this).height());
                        var topsd = $("#game-list").offset().top;
                        $("html, body").animate({ scrollTop: topsd }, 500);
                    });
                    $("#prevs").click(function () {
                        $("#ques-btn .btn-link").show();
                        $("#ques-btn").find(".result").hide();
                    });
                    $('#game-list li.last').find("input:radio").change(function () {
                        if ($(this).is(":checked")) {
                            $("#ques-btn").find(".mask-ques").hide();
                        }
                    });
                    setTimeout(function () {
                        var hfirst = $("#game-list > ul > li:first").addClass("activeh").height();
                        $("#game-list").height(hfirst);
                    }, 300);
                    $(window).resize(function () {
                        $("#game-list").height("auto");
                        setTimeout(function () {
                            var hfirst = $("#game-list > ul > li.activeh").height();
                            $("#game-list").height(hfirst);
                        }, 300);
                    });
                }
            });
        })(jQuery);
        //$(document).ready(function () {popupgamesd("<p>Bạn trả lời đúng <strong>4</strong>/5 câu hỏi trong <strong>4</strong> phút <strong>49</strong> giây</p><p>Sau 2 tuần, Live Plus sẽ tổng kết và trao quà cho người chơi có số câu trả lời đúng cao nhất trong thời gian ngắn nhất. </p><h5>HÃY ĐỢI KẾT QUẢ TỪ CHƯƠNG TRÌNH BẠN NHÉ! </h5><h4>Liveplus.vn</h4>");});
        //$(document).ready(function () {
        //            var texthtmls = "";
        //            texthtmls += '<p>Bạn trả lời đúng <strong>4</strong>/5 câu hỏi trong <strong>4</strong> phút <strong>23</strong> giây</p>';
        //            texthtmls += '<p>Sau 2 tuần, Live Plus sẽ tổng kết và trao quà cho người chơi có số câu trả lời đúng cao nhất trong thời gian ngắn nhất. </p>';
        //            texthtmls += '<h5>HÃY ĐỢI KẾT QUẢ TỪ CHƯƠNG TRÌNH BẠN NHÉ! </h5>';
        //            texthtmls += '<h4>Liveplus.vn</h4>';
        //popupgamesd(texthtmls);

        //});
        //game
        function popupgamesd(texthtml) {
            $("#game-contentp").html(texthtml);
            setTimeout(function () {
                $(".game-asscess").trigger("click");
            }, 1000);
        }
    </script>
   <script type="text/javascript">
       (function ($) {
           $.fn.disableSelection = function () {
               return this
				.attr('unselectable', 'on')
				.css('user-select', 'none')
				.css('-moz-user-select', 'none')
				.css('-khtml-user-select', 'none')
				.css('-webkit-user-select', 'none')
				.on('selectstart', false)
				.on('contextmenu', false)
               //				.on('keydown', false)
				.on('mousedown', false);
           };
       })(jQuery);
       $(function () {
           var noactive = "input, select, textarea, input:radio, input:checkbox, input:file, .wrap-select *"; //những tag hoặc class, id cho phủ khối vd: input, select, textarea cho click vào
          $('body').not(noactive).disableSelection();
       });
    </script>
    <a href="javascript:void(0);" class="game-asscess" data-toggle="modal" data-target="#gameassess">
    </a>
    <div class="modal fade popupbox" id="gameassess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h2 class="modal-title">
                        Chúc mừng bạn đã hoàn thành phần chơi của mình!
                    </h2>
                </div>
                <div class="modal-body">
                    <div id="game-contentp">
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-default" href="game.aspx">Close</a>
                   <%-- <button type="button" class="joom-game btn btn-default" data-dismiss="modal">
                        Close</button>--%>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
