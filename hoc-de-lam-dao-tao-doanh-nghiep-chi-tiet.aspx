﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site-sub.master" AutoEventWireup="true" CodeFile="hoc-de-lam-dao-tao-doanh-nghiep-chi-tiet.aspx.cs" Inherits="hoc_de_lam_tin_tuc_chi_tiet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
 <meta property="og:type" content="activity" />
    <%--đường link muốn share--%>
    <meta property="og:url" content='<%= Request.Url.Scheme + "://" + Page.Request.Url.Host.ToString()+ "/"+ Request.Url.AbsolutePath.Substring(Request.Url.AbsolutePath.LastIndexOf("/") + 1) %>' />
    <%--tiêu đề muốn share--%>
    <meta property="og:title" content='<%= hdnTitle.Value %>' />
    <%--hình ảnh muốn share--%>
    <%--mô tả muốn share--%>
    <meta property="og:description" content='<%= hdnDescription.Value %>' />
     <meta property="og:image" content='<%= Request.Url.Scheme + "://" + Page.Request.Url.Host.ToString() + "/res/aggregated/" + hdnImageName.Value  %>' />
    <%--tên website muốn share--%>
    <meta property="og:site_name" content='<%= Request.Url.Scheme + "://" + Page.Request.Url.Host.ToString()%>' />
    <meta name="thumbnail" content='<%= Request.Url.Scheme + "://" +Page.Request.Url.Host.ToString() + "/res/aggregated/" + hdnImageName.Value  %>' />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <asp:HiddenField ID="hdnImageName" runat="server" />
    <asp:HiddenField ID="hdnTitle" runat="server" />
    <asp:HiddenField ID="hdnDescription" runat="server" />
     <asp:ListView ID="ListView1" runat="server"
        DataSourceID="ObjectDataSource2"
        EnableModelValidation="True">
        <ItemTemplate>
            <div id="site">
               <a href="Default.aspx#play-learn">Học để làm</a>  <a href="hoc-de-lam-dao-tao-doanh-nghiep.aspx">Học để làm đào tạo ATGT cho doanh nghiệp</a><span><%# Eval("TrainBusinessTitle") %></span></div>
            <h1 class="title"><%# Eval("TrainBusinessTitle") %></h1>
            <div class="box-like">
                <div class="box-like-b">
                    <div class="fb-like" data-href='<%= Page.Request.Url.AbsolutePath %>'
                                    data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                                </div>
                </div>
            </div>
            <div class="wrap-detail">
                <div class="wrap-text">
                    <%# Eval("Content") %>
                </div>
                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("TrainBusinessID") %>' />
                <div class="wrap-order">
                    <h2 class="order-title">Tin liên quan</h2>
                    <ul class="list-order">
                        <asp:ListView ID="ListView1" runat="server"
                            DataSourceID="ObjectDataSource3"
                            EnableModelValidation="True">
                            <ItemTemplate>
                                <li><a href='<%# progressTitle(Eval("TrainBusinessTitle")) + "-dt-" + Eval("TrainBusinessID") +".aspx" %>'><%# Eval("TrainBusinessTitle") %></a></li>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                    </ul>
                    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server"
                        SelectMethod="TrainBusinessSameSelectAll" TypeName="TLLib.TrainBusiness">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="10" Name="RerultCount" Type="String" />
                            <asp:ControlParameter ControlID="hdnID" Name="TrainBusinessID" PropertyName="Value" Type="String" DefaultValue="" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
                <div class="wrap-comment">
                    <h2 class="order-title">Bình luận</h2>
                    <div class="fb-comments" data-href='<%=  Page.Request.Url.Host.ToString()+ Page.Request.Url.AbsolutePath.ToString()%>' data-width="860"></div>
                </div>
            </div>
        </ItemTemplate>
        <LayoutTemplate>
            <span runat="server" id="itemPlaceholder" />
        </LayoutTemplate>
    </asp:ListView>

    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server"
        SelectMethod="TrainBusinessSelectOne" TypeName="TLLib.TrainBusiness">
        <SelectParameters>
            <asp:QueryStringParameter Name="TrainBusinessID" QueryStringField="dt" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphSibar" runat="Server">
   <%-- <div class="box-res box-m">
        <h2><strong>đăng ký tham gia dự án</strong>(dành cho doanh nghiệp)</h2>
        <a class="link-popup" href="javascript:void(0);" data-toggle="modal" data-target="#popupp"></a>
    </div>--%>
    <h2 class="title-a">tin tức</h2>
    <div class="row">
        <asp:ListView ID="ListView2" runat="server"
            DataSourceID="ObjectDataSource3"
            EnableModelValidation="True">
            <ItemTemplate>
                <div class="col-xs-6 col-md-12">
                    <div class="box-ques">
                        <a href='<%# progressTitle(Eval("NewsFeaturedTitle")) + "-nf-" + Eval("NewsFeaturedID") +".aspx" %>' class="box-aques">
                            <span class="box-img">
                                <img class="img-responsive" id="Img1" alt='' src='<%# "http://www.liveplus.vn/res/newsfeatured/" + Eval("ImageName") %>' visible='<%# string.IsNullOrEmpty( Eval("ImageName").ToString()) ? false : 
true %>'
                                    runat="server" /></span>
                            <span class="box-name"><%# Eval("NewsFeaturedTitle") %></span>
                        </a>
                    </div>
                </div>
            </ItemTemplate>
            <LayoutTemplate>
                <span runat="server" id="itemPlaceholder" />
            </LayoutTemplate>
        </asp:ListView>
        <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" SelectMethod="NewsFeaturedSelectAll"
            TypeName="TLLib.NewsFeatured">
            <SelectParameters>
                <asp:Parameter Name="StartRowIndex" Type="String" DefaultValue="1" />
                <asp:Parameter Name="EndRowIndex" Type="String" DefaultValue="5" />
                <asp:Parameter Name="Keyword" Type="String" DefaultValue="" />
                <asp:Parameter Name="NewsFeaturedTitle" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter Name="NewsFeaturedCategoryID" Type="String" DefaultValue="4" />
                <asp:Parameter Name="Tag" Type="String" />
                <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                <asp:Parameter Name="IsHot" Type="String" />
                <asp:Parameter Name="IsNew" Type="String" />
                <asp:Parameter Name="FromDate" Type="String" />
                <asp:Parameter Name="ToDate" Type="String" />
                <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                <asp:Parameter Name="Priority" Type="String" />
                <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphPopup" runat="Server">

    <!-- Modal -->
 <%--   <div class="modal fade popup2" id="popupp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg contest-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h2 class="modal-title" id="myModalLabel">đăng kí thông tin doanh nghiệp</h2>
                </div>
                <div class="modal-body">
                    <fieldset class="send-wrap">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tên công ty</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtFullName" CssClass="form-control" placeholder="-- Tên công ty --" runat="server" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator7" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtFullName"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Địa chỉ</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtAddress" CssClass="form-control" placeholder="-- Địa chỉ --" runat="server" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator5" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtAddress"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Lĩnh vực hoạt động</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtLinhVuc" CssClass="form-control" placeholder="-- Lĩnh Vực Hoạt Động --" runat="server" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator6" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtLinhVuc"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tên người liên hệ</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtName" CssClass="form-control" placeholder="-- Tên người liên hệ --" runat="server" Text=""></asp:TextBox>
                                        <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator2" runat="server"
                                            Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtName"
                                            ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-offset-1 col-sm-4 control-label">Chức vụ</label>
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtChucVu" CssClass="form-control" placeholder="-- Chức vụ --" runat="server" Text=""></asp:TextBox>
                                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator3" runat="server"
                                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtChucVu"
                                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" Text="" placeholder="-- Nhập email --"></asp:TextBox>
                                <asp:RegularExpressionValidator CssClass="lb-error" ID="RegularExpressionValidator1"
                                    runat="server" ValidationGroup="Information" ControlToValidate="txtEmail" ErrorMessage="Sai định dạng email!"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"
                                    ForeColor="Red"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator1" runat="server"
                                    ValidationGroup="Information" ControlToValidate="txtEmail" ErrorMessage="Thông tin bắt buộc!"
                                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Điện thoại</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtPhone" CssClass="form-control" runat="server" Text="" placeholder="-- Điện thoại --"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator4" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtPhone"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <div class="wrap-btn">
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <asp:Button ID="btnUpLoad" OnClick="btnUpLoad_Click" CssClass="btn-send" ValidationGroup="Information" runat="server" Text="đăng ký thông tin" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>

