﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="hoc-de-lam-dao-tao-doanh-nghiep.aspx.cs" Inherits="hoc_de_lam_tin_tuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Học để làm - Đào tạo ATGT cho doanh nghiệp</title>
    <meta name="Description" content="Học để làm - Đào tạo ATGT cho doanh nghiệp" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="wrapper-main">
        <div class="container">
            <div id="site">
                <a href="Default.aspx#play-learn">Học để làm</a><span>Đào tạo ATGT cho doanh nghiệp</span>
            </div>
            <div class="head-box">
                <div class="head-box-b">
                    <h1 class="title">Đào tạo ATGT cho doanh nghiệp</h1>
                </div>
            </div>
            <div id="newslistx" class="wrapper-section news-box">
                <div class="row">
                    <asp:ListView ID="ListView1" runat="server"
                        DataSourceID="ObjectDataSource1"
                        EnableModelValidation="True">
                        <ItemTemplate>
                            <div class="col-xs-6 item-border-bottom">
                                <div class="colla-box">
                                    <a href='<%# progressTitle(Eval("TrainBusinessTitle")) + "-dt-" + Eval("TrainBusinessID") +".aspx" %>' class="colla-img bcover">
                                        <img class="img-responsive" id="Img1" alt='' src='<%# "res/trainbusiness/" + Eval("ImageName") %>'
                                            visible='<%# string.IsNullOrEmpty( Eval("ImageName").ToString()) ? false : 
true %>'
                                            runat="server" />
                                    </a>
                                    <div class="colla-content">
                                        <h3 class="colla-name"><a href='<%# progressTitle(Eval("TrainBusinessTitle")) + "-dt-" + Eval("TrainBusinessID") +".aspx" %>'  title='<%# Eval("TrainBusinessTitle") %>'><%# TLLib.Common.SplitSummary(Eval("TrainBusinessTitle").ToString(),80) %></a></h3>
                                        <div class="description"><%# TLLib.Common.SplitSummary(Eval("Description").ToString(),170) %></div>
                                        <div class="colla-bottom">
                                            <%--<span class="admin">Admin</span>--%>
                                            <span class="date"><%# string.Format("{0:dd.MM.yyyy}", Eval("CreateDate"))%></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <span runat="server" id="itemPlaceholder" />
                        </LayoutTemplate>
                    </asp:ListView>
                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
                        SelectMethod="TrainBusinessSelectAll" TypeName="TLLib.TrainBusiness">
                        <SelectParameters>
                            <asp:Parameter Name="StartRowIndex" Type="String" />
                            <asp:Parameter Name="EndRowIndex" Type="String" />
                            <asp:Parameter DefaultValue="" Name="Keyword" Type="String" />
                            <asp:Parameter Name="TrainBusinessTitle" Type="String" />
                            <asp:Parameter Name="Description" Type="String" />
                            <asp:Parameter Name="TrainBusinessCategoryID" Type="String" />
                            <asp:Parameter Name="Tag" Type="String" />
                            <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                            <asp:Parameter Name="IsHot" Type="String" />
                            <asp:Parameter Name="IsNew" Type="String" />
                            <asp:Parameter Name="FromDate" Type="String" />
                            <asp:Parameter Name="ToDate" Type="String" />
                            <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                            <asp:Parameter Name="Priority" Type="String" />
                            <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
            </div>

            <div class="pager">

                <asp:DataPager ID="DataPager1" runat="server" PageSize="8" PagedControlID="ListView1">
                    <Fields>
                        <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="numer-paging" CurrentPageLabelCssClass="current-page" />

                    </Fields>
                </asp:DataPager>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphPopup" runat="Server">
</asp:Content>