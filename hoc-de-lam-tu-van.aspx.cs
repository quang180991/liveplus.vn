﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class hoc_de_lam_tu_van : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (((DataView)ObjectDataSource1.Select()).Count <= DataPager1.PageSize)
        {
            DataPager1.Visible = false;
        }
    }
    protected string progressTitle(object input)
    {
        var convertTitle = new ConvertTitle();
        return convertTitle.convertToLowerCase(input.ToString());
    }
}