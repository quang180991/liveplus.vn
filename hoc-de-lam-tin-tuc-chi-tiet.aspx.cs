﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class hoc_de_lam_tin_tuc_chi_tiet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            HtmlMeta meta = new HtmlMeta();
            meta.Name = "description";
            var dv = (DataView)ObjectDataSource2.Select();
            if (dv.Count > 0)
            {

                Page.Title = dv[0]["MetaTittle"].ToString();
                meta.Content = dv[0]["MetaDescription"].ToString();
                Header.Controls.Add(meta);
                var row = dv[0];

                var strImageName = Server.HtmlDecode(row["ImageName"].ToString());
                var strTitle = Server.HtmlDecode(row["NewsFeaturedTitle"].ToString());
                var strDescription = Server.HtmlDecode(row["Description"].ToString());

                Header.Controls.Add(meta);
                hdnImageName.Value = strImageName;
                hdnTitle.Value = strTitle;
                hdnDescription.Value = strDescription;
            }

        }
    }
    protected string progressTitle(object input)
    {
        var convertTitle = new ConvertTitle();
        return convertTitle.convertToLowerCase(input.ToString());
    }
    //private void sendEmail()
    //{
    //    SmtpClient smtpClient = new SmtpClient();
    //    MailMessage message = new MailMessage();
    //    MailAddress fromAddress = new MailAddress("0961129@gmail.com");
    //    smtpClient.Host = "smtp.gmail.com";
    //    smtpClient.Port = 587;
    //    smtpClient.EnableSsl = true;
    //    message.From = fromAddress;
    //    message.To.Add("0961129@gmail.com");
    //    message.Subject = "Live Plus: ĐĂNG KÍ THÔNG TIN DOANH NGHIỆP";
    //    message.IsBodyHtml = true;

    //    string msg = "<h3>Live Plus: ĐĂNG KÍ THÔNG TIN DOANH NGHIỆP</h3><br /><br />";
    //    msg += "<b>Tên công ty: </b>" + txtFullName.Text.Trim().ToString() + "<br /><br />";
    //    msg += "<b>Địa chỉ: </b>" + txtAddress.Text.Trim().ToString() + "<br /><br />";
    //    msg += "<b>Lĩnh vực hoạt động: </b>" + txtLinhVuc.Text.Trim().ToString() + "<br /><br />";
    //    msg += "-----------------------------------------------------------------------<br /><br />";
    //    msg += "<b>Tên người liên hệ: </b>" + txtName.Text.Trim().ToString() + "<br /><br />";
    //    msg += "<b>Chức vụ: </b>" + txtChucVu.Text.Trim().ToString() + "<br /><br />";
    //    msg += "<b>Email: </b>" + txtEmail.Text.Trim().ToString() + "<br /><br />";
    //    msg += "<b>Điện thoại: </b>" + txtPhone.Text.Trim().ToString() + "<br /><br />";
    //    msg += "-----------------------------------------------------------------------<br /><br />";
    //    message.Body = msg;
    //    smtpClient.Credentials = new System.Net.NetworkCredential("0961129@gmail.com", "ThanhVit18099146");
    //    smtpClient.Send(message);
    //}
    //protected void btnUpLoad_Click(object sender, EventArgs e)
    //{
    //    sendEmail();
    //}
}