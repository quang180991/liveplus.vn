﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class hoc_de_lam_tin_tuc : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            //HtmlMeta meta = new HtmlMeta();
            //meta.Name = "description";
            var dv = (DataView)ObjectDataSource1.Select();
            if (dv.Count <= DataPager1.PageSize)
            {
                DataPager1.Visible = false;
                //Page.Title = dv[0]["MetaTittle"].ToString();
                //meta.Content = dv[0]["MetaDescription"].ToString();
                //Header.Controls.Add(meta);
            }

        }
    }
    protected string progressTitle(object input)
    {
        var convertTitle = new ConvertTitle();
        return convertTitle.convertToLowerCase(input.ToString());
    }
}