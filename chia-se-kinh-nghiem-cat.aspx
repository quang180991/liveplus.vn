﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site-sub.master" AutoEventWireup="true" CodeFile="chia-se-kinh-nghiem-cat.aspx.cs" Inherits="doc_de_biet_kinh_nghiem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="site">
        <a href="Default.aspx#learn-know">Đọc để biết</a><a href="chia-se-kinh-nghiem.aspx">Chia sẻ &amp; trải nghiệm</a><span><asp:Label ID="lbName" runat="server"></asp:Label></span>
    </div>
    <div class="head-tit">
        <h2 class="title-5"><%= lbName.Text %></h2>
        <%--<a href="#" class="link-all">xem tất cả</a>--%>
    </div>
    <div id="boxcat" class="sknow-big">
        <div class="row">
            <asp:ListView ID="ListView1" runat="server"
                DataSourceID="ObjectDataSource2"
                EnableModelValidation="True">
                <ItemTemplate>
                    <div class="col-sm-4">
                        <a href='<%# progressTitle(Eval("ReadersShareTitle")) + "-rsd-" + Eval("ReadersShareID") +".aspx" %>' class="sknow-img">
                            <img id="Img1" alt='' src='<%# "http://www.liveplus.vn/res/readersshare/" + Eval("ImageName") %>' class="img-responsive"
                                visible='<%# string.IsNullOrEmpty( Eval("ImageName").ToString()) ? false : 
true %>' /></a>
                    </div>
                    <div class="col-sm-8">
                        <div class="sknow-content">
                            <h2 class="sknow-name"><a href='<%# progressTitle(Eval("ReadersShareTitle")) + "-rsd-" + Eval("ReadersShareID") +".aspx" %>'><%# Eval("ReadersShareTitle") %></a></h2>
                            <p class="sknow-date">Ngày đăng:   <%# string.Format("{0:dd.MM.yyyy}", Eval("CreateDate"))%></p>
                            <div class="like-box">
                                <div class="fb-like" data-href='<%# Page.Request.Url.Host.ToString()+ progressTitle(Eval("ReadersShareTitle")) + "-rsd-" + Eval("ReadersShareID") +".aspx"%>'
                                    data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                                </div>
                            </div>
                            <div class="description">
                                <%# Eval("Description") %><a href='<%# progressTitle(Eval("ReadersShareTitle")) + "-rsd-" + Eval("ReadersShareID") +".aspx" %>' class="more-detail show-mobile">Chi tiết</a>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>
            <asp:ObjectDataSource ID="ObjectDataSource2" runat="server"
                SelectMethod="ReadersShareSelectAll" TypeName="TLLib.ReadersShare">
                <SelectParameters>
                    <asp:Parameter DefaultValue="1" Name="StartRowIndex" Type="String" />
                    <asp:Parameter DefaultValue="1" Name="EndRowIndex" Type="String" />
                    <asp:Parameter DefaultValue="" Name="Keyword" Type="String" />
                    <asp:Parameter Name="ReadersShareTitle" Type="String" />
                    <asp:Parameter Name="Description" Type="String" />
                    <asp:QueryStringParameter Name="ReadersShareCategoryID" QueryStringField="rs" Type="String" />
                    <asp:Parameter Name="Tag" Type="String" />
                    <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                    <asp:Parameter Name="IsHot" Type="String" />
                    <asp:Parameter Name="IsNew" Type="String" />
                    <asp:Parameter Name="FromDate" Type="String" />
                    <asp:Parameter Name="ToDate" Type="String" />
                    <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                    <asp:Parameter Name="Priority" Type="String" />
                    <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>

        </div>
    </div>
    <%--<div class="pagerde">
        <a href="#" class="prev">Bài trước</a>
        <a href="#" class="next">Bài kế tiếp</a>
    </div>--%>
    <div class="wrap-boxa">
        <asp:ListView ID="ListView2" runat="server"
            DataSourceID="ObjectDataSource3"
            EnableModelValidation="True">
            <ItemTemplate>
                <a href='<%# progressTitle(Eval("ReadersShareTitle")) + "-rsd-" + Eval("ReadersShareID") +".aspx" %>' class="boxa">
                    <span class="boxa-b show-desktop">
                        <strong>xem
                    <br />
                            chi tiết</strong>
                    </span>
                    <span class="boxa-w">
                        <span class="boxa-img">
                            <img id="Img1" alt='' src='<%# "http://www.liveplus.vn/res/readersshare/" + Eval("ImageName") %>' class="img-responsive"
                                visible='<%# string.IsNullOrEmpty( Eval("ImageName").ToString()) ? false : 
true %>'/></span>
                        <span class="boxa-content">
                            <span class="boxa-name"><%# Eval("ReadersShareTitle") %></span>
                            <span class="description"><%# TLLib.Common.SplitSummary(Eval("Description").ToString(),200) %></span>
                        </span>
                    </span>
                </a>
            </ItemTemplate>
            <LayoutTemplate>
                <span runat="server" id="itemPlaceholder" />
            </LayoutTemplate>
        </asp:ListView>
        <asp:ObjectDataSource ID="ObjectDataSource3" runat="server"
            SelectMethod="ReadersShareSelectAll" TypeName="TLLib.ReadersShare">
            <SelectParameters>
                <asp:Parameter DefaultValue="2" Name="StartRowIndex" Type="String" />
                <asp:Parameter Name="EndRowIndex" Type="String" />
                <asp:Parameter DefaultValue="" Name="Keyword" Type="String" />
                <asp:Parameter Name="ReadersShareTitle" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:QueryStringParameter Name="ReadersShareCategoryID" QueryStringField="rs" Type="String" />
                <asp:Parameter Name="Tag" Type="String" />
                <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                <asp:Parameter Name="IsHot" Type="String" />
                <asp:Parameter Name="IsNew" Type="String" />
                <asp:Parameter Name="FromDate" Type="String" />
                <asp:Parameter Name="ToDate" Type="String" />
                <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                <asp:Parameter Name="Priority" Type="String" />
                <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
    <p id="link-add" class="more-add"><a href="#">Xem các tin khác</a></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphSibar" runat="Server">
    <h2 class="title-c">Danh mục</h2>
    <ul class="list-nav nav-line">
        <asp:ListView ID="ListView3" runat="server"
            DataSourceID="ObjectDataSource1"
            EnableModelValidation="True">
            <ItemTemplate>
                <li><a href='<%# progressTitle(Eval("ReadersShareCategoryName")) + "-rs-" + Eval("ReadersShareCategoryID") +".aspx" %>'><%# Eval("ReadersShareCategoryName") %></a></li>
            </ItemTemplate>
            <LayoutTemplate>
                <span runat="server" id="itemPlaceholder" />
            </LayoutTemplate>
        </asp:ListView>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
            SelectMethod="ReadersShareCategorySelectAll" TypeName="TLLib.ReadersShareCategory"></asp:ObjectDataSource>
    </ul>
    <div class="box-wp">
        <h2 class="title-b">các bài viết mới nhất</h2>
        <ul class="list-experience">
            <asp:ListView ID="ListView5" runat="server"
                DataSourceID="ObjectDataSource4"
                EnableModelValidation="True">
                <ItemTemplate>
                    <li>
                        <h4 class="experience-name"><a href='<%# progressTitle(Eval("ReadersShareCategoryName")) + "-rs-" + Eval("ReadersShareCategoryID") +".aspx" %>'><%# Eval("ReadersShareCategoryName") %></a></h4>
                        <h3 class="exper-name"><a href='<%# progressTitle(Eval("ReadersShareTitle")) + "-rsd-" + Eval("ReadersShareID") +".aspx" %>'><%# Eval("ReadersShareTitle") %></h3>
                        <div class="description"><%# TLLib.Common.SplitSummary(Eval("Description").ToString(),100) %></div>
                    </li>
                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>

        </ul>
        <asp:ObjectDataSource ID="ObjectDataSource4" runat="server"
            SelectMethod="ReadersShareSelectAll" TypeName="TLLib.ReadersShare">
            <SelectParameters>
                <asp:Parameter DefaultValue="1" Name="StartRowIndex" Type="String" />
                <asp:Parameter Name="EndRowIndex" Type="String" DefaultValue="3" />
                <asp:Parameter DefaultValue="" Name="Keyword" Type="String" />
                <asp:Parameter Name="ReadersShareTitle" Type="String" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter DefaultValue="" Name="ReadersShareCategoryID" Type="String" />
                <asp:Parameter Name="Tag" Type="String" />
                <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                <asp:Parameter Name="IsHot" Type="String" />
                <asp:Parameter Name="IsNew" Type="String" DefaultValue="true" />
                <asp:Parameter Name="FromDate" Type="String" />
                <asp:Parameter Name="ToDate" Type="String" />
                <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                <asp:Parameter Name="Priority" Type="String" />
                <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphPopup" runat="Server">
</asp:Content>

