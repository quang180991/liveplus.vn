﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TLLib;

public partial class choi_de_hieu : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            HtmlMeta meta = new HtmlMeta();
            meta.Name = "description";
            var dv = (DataView)ObjectDataSource2.Select();
            if (dv.Count > 0)
            {
                if (dv[0]["FinishDate"] != DBNull.Value)
                {
                    DateTime dateNow = DateTime.Now.Date;
                    DateTime dateOld = (Convert.ToDateTime(dv[0]["FinishDate"])).Date;

                    if (dateNow > dateOld)
                        hdndateold.Value = "0";
                }

                lbName.Text = dv[0]["LearnsCategoryName"].ToString();
                Page.Title = dv[0]["MetaTitle"].ToString();
                meta.Content = dv[0]["MetaDescription"].ToString();
                Header.Controls.Add(meta);

                string Periodic = dv[0]["Periodic"].ToString();
                var dv1 = new LearnsCategory().LearnsCategorySelectOldPeriodic("1", Periodic).DefaultView;
                if (dv1.Count > 0)
                {
                    var dv2 = new Learns().LearnsSelectAll("1", "3", "", "", dv1[0]["LearnsCategoryID"].ToString(), "true", "", "", "", "", "false", "", "", "", "", "true").DefaultView;
                    ListView4.DataSource = dv2;
                    ListView4.DataBind();
                    hdnLinkOld.Value = progressTitle(dv2[0]["LearnsCategoryName"]) + "-pq-" + dv2[0]["LearnsCategoryID"] + ".aspx";
                }
            }
            if (((DataView)ObjectDataSource3.Select()).Count <= DataPager1.PageSize)
            {
                DataPager1.Visible = false;
            }
        }
    }
    protected string progressTitle(object input)
    {
        var convertTitle = new ConvertTitle();
        return convertTitle.convertToLowerCase(input.ToString());
    }
    protected void btnUpLoad_Click(object sender, EventArgs e)
    {
        bool IsLogin = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
        if (IsLogin == true)
        {
            string UserName = System.Web.HttpContext.Current.User.Identity.Name;
            var dv = (DataView)ObjectDataSource2.Select();
            string CID = dv[0]["LearnsCategoryID"].ToString();

            
            var oLearns = new Learns();
            string Name = txtFullName.Text.ToString().Trim();
            string Phone = txtPhone.Text.ToString().Trim();
            string Email = txtEmail.Text.ToString().Trim();
            string Title = txtTitle.Text.ToString().Trim();
            string Description = txtDescription.Text.ToString().Trim();
            string Quote = txtQuote.Text.ToString().Trim();
            var strConvertedTitle = Common.ConvertTitle(Title);


            oLearns.LearnsInsert(Title, Description, "", "", strConvertedTitle, "", "", Quote, "", CID, "false", "", "", "0", "0", "0", "0", UserName, "", "false", "", Name, Email, Phone, "");

            Session["Success"] = "true";

            Response.Redirect(Page.Request.UrlReferrer.ToString());
            new TLLib.AddressBook().UserProfile_UpdatePoint_Address(UserName);
        }
        else
        {
            Response.Redirect("Login.aspx?ReturnUrl=" + Page.Request.Url.AbsolutePath.ToString());
        }
    }
}