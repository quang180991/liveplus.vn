﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class lien_he : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btGui_Click(object sender, EventArgs e)
    {
        string msg = "<h3>Live Plus : LIÊN HỆ</h3><br/>";
        msg += "<b>Họ và tên: </b>" + txtHoTen.Text.Trim().ToString() + "<br /><br />";
        msg += "<b>Điện thoại: </b>" + txtPhone.Text.Trim().ToString() + "<br /><br />";
        msg += "<b>Email: </b>" + txtEmail.Text.Trim().ToString() + "<br /><br />";
        msg += "<b>Nội dung: </b>" + txtNoiDung.Text.Trim().ToString() + "<br /><br />";

        var fileMap = new ExeConfigurationFileMap() { ExeConfigFilename = Server.MapPath("~/config/app.config") };
        var config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
        var section = (AppSettingsSection)config.GetSection("appSettings");
        var strUseSSL = section.Settings["UseSSL"].Value;
        var strPort = section.Settings["Port"].Value;
        var strHost = section.Settings["Host"].Value;
        int n;
        bool isNumeric = int.TryParse(strPort, out n);
        var iPort = isNumeric ? n : 0;
        var strMailFrom = section.Settings["Email"].Value;
        var strUserName = section.Settings["UserName"].Value;
        var strPassword = TLLib.MD5Hash.Decrypt(section.Settings["Password"].Value, true);
        var strReceivedEmails = section.Settings["ReceivedEmails"].Value;
        var bEnableSsl = strUseSSL.ToLower() == "true" ? true : false;
        var strSubject = "Contact from Live Plus website";
        var strBody = msg;
        var lstMailTo = strReceivedEmails.Replace(',', ';').Split(';').ToList<string>();
        var lstCC = new List<string>();
        var lstAttachment = new List<string>();

        var bSendSuccess = TLLib.Common.SendMail(
                strHost,
                iPort,
                strMailFrom,
                strUserName,
                strPassword,
                lstMailTo,
                lstCC,
                lstAttachment,
                strSubject,
                strBody,
                bEnableSsl
            );
        lblMessage.Text = "Cám ơn bạn đã liên lạc với chúng tôi. Thông tin của bạn đã được gửi. Chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất!";
           
        txtHoTen.Text = txtPhone.Text = txtEmail.Text = txtNoiDung.Text = "";
    }
}