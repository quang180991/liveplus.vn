﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="choi-de-hieu-video.aspx.cs" Inherits="choi_de_hieu" %>

<%@ Register Src="~/uc/successCDH.ascx" TagPrefix="uc1" TagName="successCDH" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share"
        type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <p class="site-node" id="site">
            <a href="Default.aspx#play-learn">Chơi để hiểu</a><span>Thi video</span>
        </p>
        <div class="wrap-main">
            <div id="col-right" class="desktop-s980">
                <div class="box-asibar">
                    <div class="box-resgister desktop-s980">
                        <h2>Đăng kí tham gia cuộc thi</h2>
                        <div class="description">
                            <p>Hãy là những người đầu tiên tham gia cuộc thi </p>
                            <p><strong>HÃY THỂ HIỆN TÀI NĂNG CỦA BẠN - CÙNG GOsmart XÂY DỰNG CUỘC SỐNG TỐT ĐẸP</strong></p>
                        </div>
                        <p class="more-gis"><a href='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ?"javarscript:void(0);" : "popuppage/Login.aspx?cmd=CDH" %>' data-toggle="modal" data-target='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ?"#registerbox" : "#popupPages" %>'>Đăng kí</a></p>
                    </div>
                    <h2 class="title-3">Tác phẩm đoạt giải</h2>
                    <div class="list-month">
                        <div class="row">
                            <asp:ListView ID="ListView2" runat="server"
                                DataSourceID="ObjectDataSource1"
                                EnableModelValidation="True">
                                <ItemTemplate>
                                    <div class="col-md-12 col-xs-6">
                                        <div class="box-month">
                                            <a class="month-a" href='<%# progressTitle(Eval("Title")) + "-pvd-" + Eval("LearnsID") +".aspx" %>'>
                                                <span class="month-name"><span class="month-ro">Kỳ <strong><%# Eval("Periodic") %></strong></span></span>
                                                <span class="month-img">
                                                    <img id="Img1" alt='' src='<%# "http://www.liveplus.vn/res/learns/video/thumbs/" + Eval("ImagePath") %>'
                                                        visible='<%# string.IsNullOrEmpty( Eval("ImagePath").ToString()) ? false : 
true %>'
                                                        runat="server" class="img-responsive" />
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <LayoutTemplate>
                                    <span runat="server" id="itemPlaceholder" />
                                </LayoutTemplate>
                            </asp:ListView>
                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
                                SelectMethod="LearnsSelectAll" TypeName="TLLib.Learns">
                                <SelectParameters>
                                    <asp:Parameter DefaultValue="1" Name="StartRowIndex" Type="String" />
                                    <asp:Parameter Name="EndRowIndex" Type="String" DefaultValue="5" />
                                    <asp:Parameter Name="Title" Type="String" />
                                    <asp:Parameter Name="Description" Type="String" />
                                    <asp:Parameter DefaultValue="3" Name="LearnsCategoryID" Type="String" />
                                    <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                                    <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                                    <asp:Parameter DefaultValue="" Name="IsNew" Type="String" />
                                    <asp:Parameter Name="Createby" Type="String" />
                                    <asp:Parameter Name="Priority" Type="String" />
                                    <asp:Parameter Name="InGallery" Type="String" />
                                    <asp:Parameter DefaultValue="true" Name="IsWinner" Type="String" />
                                    <asp:Parameter Name="Name" Type="String" />
                                    <asp:Parameter Name="Email" Type="String" />
                                    <asp:Parameter Name="Phone" Type="String" />
                                    <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </div>
                    </div>
                </div>
            </div>
            <div id="col-left">
                <h1 class="title">
                    <asp:Label ID="lbName" runat="server"></asp:Label></h1>
                <div class="box-like">
                    <div class="box-like-b">
                        <div class="fb-like" data-href='<%= Page.Request.Url.AbsolutePath %>'
                            data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                        </div>
                    </div>
                </div>
                <div class="wrap-des">
                    <div class="tab-content content-info">
                        <asp:ListView ID="ListView3" runat="server"
                            DataSourceID="ObjectDataSource2"
                            EnableModelValidation="True">
                            <ItemTemplate>
                                <h3 class="tit-des show-mobile"><a href="#desTab1">Thể lệ cuộc thi</a></h3>
                                <div class="tab-pane" id="desTab1">
                                    <div class="description">
                                        <div class="text-box">
                                            <%# Eval("Description") %>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="tit-des show-mobile"><a href="#desTab2">Giải thưởng</a></h3>
                                <div class="tab-pane" id="desTab2">
                                    <div class="description">
                                        <div class="text-box">
                                            <%# Eval("Content") %>
                                        </div>
                                    </div>
                                </div>
                                <h3 class="tit-des show-mobile"><a href="#desTab3">Hình thức tham gia</a></h3>
                                <div class="tab-pane" id="desTab3">
                                    <div class="description">
                                        <div class="text-box">
                                            <%# Eval("DescriptionEn") %>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server"
                            SelectMethod="LearnsCategorySelectOne" TypeName="TLLib.LearnsCategory">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="LearnsCategoryID" QueryStringField="pv" Type="String" DefaultValue="3" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" id="desTab">
                        <li><a href="#desTab1" role="tab" data-toggle="tab">Thể lệ cuộc thi</a></li>
                        <li><a href="#desTab2" role="tab" data-toggle="tab">Giải thưởng</a></li>
                        <li><a href="#desTab3" role="tab" data-toggle="tab">Hình thức tham gia</a></li>
                    </ul>
                    <p class="node-i">( Click vào button link trên để xem thêm nội dung )</p>
                </div>
                <div class="head  heah-t">
                    <h2 class="title-4"><span>Tác phẩm dự thi mới nhất</span></h2>
                </div>

                <div class="pro-table">
                    <div class="row">
                        <asp:ListView ID="ListView1" runat="server"
                            DataSourceID="ObjectDataSource3"
                            EnableModelValidation="True">
                            <ItemTemplate>
                                <div class="col-xs-6 col-lg-4">
                                    <div class="box-articles">
                                        <div class="articles-content articles-h">
                                            <p class="line">Tác giả: <span><%# Eval("Name") %></span></p>
                                            <p>Chủ đề: <span><%# Eval("Title") %></span></p>
                                        </div>
                                        <a class="articles-img bcover" href='<%# progressTitle(Eval("Title")) + "-pvd-" + Eval("LearnsID") +".aspx" %>'>
                                            <img id="Img2" alt='' src='<%# "http://www.liveplus.vn/res/learns/video/thumbs/" + Eval("ImagePath") %>'
                                                visible='<%# string.IsNullOrEmpty( Eval("ImagePath").ToString()) ? false : 
true %>'
                                                runat="server" class="img-responsive" />
                                            <span class="mask-video"></span>
                                            <span class="box-text">
                                                <span class="text-1" style='<%# Eval("Voted").ToString() == "0"? "display: none;": ""%>'><strong><%# Eval("Voted")%></strong> bình chọn</span>
                                                <span class="text-2" style='<%# Eval("Views").ToString() == "0"? "display: none;": ""%>'><strong><%# Eval("Views")%></strong> lượt xem</span>

                                            </span>
                                        </a>
                                        <p class="date"><%# string.Format("{0:dd.MM.yyyy}", Eval("CreateDate"))%> </p>
                                        <div class="articles-content articles-b">
                                            <a href="#" class="reques <%# DateTime.Now.Date > (Eval("FinishDate") != DBNull.Value ? (Convert.ToDateTime(Eval("FinishDate"))).Date : DateTime.Now.AddDays(-1)).Date ? "hidden":""  %>">báo trùng</a>
                                            <a href='<%# progressTitle(Eval("Title")) + "-pvd-" + Eval("LearnsID") +".aspx" %>' class="link-com <%# DateTime.Now.Date > (Eval("FinishDate") != DBNull.Value ? Convert.ToDateTime(Eval("FinishDate")) : DateTime.Now.AddDays(-1)).Date ? "hidden":""  %>">Bình chọn</a>
                                            <%-- <div class="like-box">
                                                <a name="fb_share" type="button_count" expr:share_url="data:post.url" share_url='<%# Page.Request.Url.Host.ToString() + ""+ progressTitle(Eval("Title")) + "-pvd-" + Eval("LearnsID") +".aspx"  %>'>Chia Sẻ</a>
                                                <div class="fb-like" data-href='<%# Page.Request.Url.Host.ToString() + ""+ progressTitle(Eval("Title")) + "-pvd-" + Eval("LearnsID") +".aspx"  %>'
                                                    data-layout="button_count" data-action="like" data-show-faces="true" data-share="true">
                                                </div>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                        <asp:ObjectDataSource ID="ObjectDataSource3" runat="server"
                            SelectMethod="LearnsSelectAll" TypeName="TLLib.Learns">
                            <SelectParameters>
                                <asp:Parameter Name="StartRowIndex" Type="String" />
                                <asp:Parameter Name="EndRowIndex" Type="String" />
                                <asp:Parameter Name="Title" Type="String" />
                                <asp:Parameter Name="Description" Type="String" />
                                <asp:QueryStringParameter Name="LearnsCategoryID" QueryStringField="pv" Type="String" DefaultValue="3" />
                                <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                                <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                                <asp:Parameter Name="IsNew" Type="String" />
                                <asp:Parameter Name="Createby" Type="String" />
                                <asp:Parameter Name="Priority" Type="String" />
                                <asp:Parameter Name="InGallery" Type="String" />
                                <asp:Parameter Name="IsWinner" Type="String" />
                                <asp:Parameter Name="Name" Type="String" />
                                <asp:Parameter Name="Email" Type="String" />
                                <asp:Parameter Name="Phone" Type="String" />
                                <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </div>
                </div>
                <div class="pager">

                    <asp:DataPager ID="DataPager1" runat="server" PageSize="6" PagedControlID="ListView1">
                        <Fields>
                            <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="numer-paging" CurrentPageLabelCssClass="current" />

                        </Fields>
                    </asp:DataPager>
                </div>
                <div class="mobile-s980">
                    <div class="head heah-t">
                        <h2 class="title-4"><span>Tác phẩm đoạt giải</span></h2>
                    </div>

                    <div class="pro-table">
                        <div class="row">
                            <asp:ListView ID="ListView5" runat="server"
                                DataSourceID="ObjectDataSource1"
                                EnableModelValidation="True">
                                <ItemTemplate>
                                    <div class="col-md-12 col-xs-6">
                                        <div class="box-month box-articles">
                                            <a class="month-a" href='<%# progressTitle(Eval("Title")) + "-pvd-" + Eval("LearnsID") +".aspx" %>'>
                                                <span class="month-name articles-img bcover"><span class="month-ro">Kỳ <strong><%# Eval("Periodic") %></strong></span></span>
                                                <span class="month-img">
                                                    <img id="Img1" alt='' src='<%# "http://www.liveplus.vn/res/learns/video/thumbs/" + Eval("ImagePath") %>'
                                                        visible='<%# string.IsNullOrEmpty( Eval("ImagePath").ToString()) ? false : 
true %>'
                                                        runat="server" class="img-responsive" />
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <LayoutTemplate>
                                    <span runat="server" id="itemPlaceholder" />
                                </LayoutTemplate>
                            </asp:ListView>
                            <asp:ObjectDataSource ID="ObjectDataSource4" runat="server"
                                SelectMethod="LearnsSelectAll" TypeName="TLLib.Learns">
                                <SelectParameters>
                                    <asp:Parameter DefaultValue="1" Name="StartRowIndex" Type="String" />
                                    <asp:Parameter Name="EndRowIndex" Type="String" DefaultValue="5" />
                                    <asp:Parameter Name="Title" Type="String" />
                                    <asp:Parameter Name="Description" Type="String" />
                                    <asp:Parameter DefaultValue="3" Name="LearnsCategoryID" Type="String" />
                                    <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                                    <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                                    <asp:Parameter DefaultValue="" Name="IsNew" Type="String" />
                                    <asp:Parameter Name="Createby" Type="String" />
                                    <asp:Parameter Name="Priority" Type="String" />
                                    <asp:Parameter Name="InGallery" Type="String" />
                                    <asp:Parameter DefaultValue="true" Name="IsWinner" Type="String" />
                                    <asp:Parameter Name="Name" Type="String" />
                                    <asp:Parameter Name="Email" Type="String" />
                                    <asp:Parameter Name="Phone" Type="String" />
                                    <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hdndateold" runat="server" />
                <div class="head  heah-t head-bg <%= hdndateold.Value.ToString()=="0" ? "hidden":""  %>">
                    <h2 class="title-4"><span>Tác phẩm cuộc thi trước</span></h2>
                    <a class="tit-link show-desktop" href='<%= hdnLinkOld.Value %>'>Xem tất cả tác phẩm</a>
                </div>
                <div class="pro-table">
                    <asp:HiddenField ID="hdnLinkOld" runat="server" />
                    <asp:ListView ID="ListView4" runat="server"
                        EnableModelValidation="True">
                        <ItemTemplate>
                            <div class="col-xs-6 col-lg-4">

                                <div class="box-articles">
                                    <div class="articles-content articles-h">
                                        <p class="line">Tác giả: <span><%# Eval("Name") %></span></p>
                                        <p>Chủ đề: <span><%# Eval("Title") %></span></p>
                                    </div>
                                    <a class="articles-img bcover" href='<%# progressTitle(Eval("Title")) + "-pvd-" + Eval("LearnsID") +".aspx" %>'>
                                        <img id="Img2" alt='' src='<%# "http://www.liveplus.vn/res/learns/video/thumbs/" + Eval("ImagePath") %>'
                                            visible='<%# string.IsNullOrEmpty( Eval("ImagePath").ToString()) ? false : 
true %>'
                                            runat="server" class="img-responsive" />
                                        <span class="mask-video"></span>
                                        <span class="box-text">
                                            <span class="text-1" style='<%# Eval("Voted").ToString() == "0"? "display: none;": ""%>'><strong><%# Eval("Voted")%></strong> bình chọn</span>
                                            <span class="text-2" style='<%# Eval("Views").ToString() == "0"? "display: none;": ""%>'><strong><%# Eval("Views")%></strong> lượt xem</span>
                                        </span>
                                    </a>
                                    <p class="date"><%# string.Format("{0:dd.MM.yyyy}", Eval("CreateDate"))%> </p>
                                    <div class="articles-content articles-b">
                                    </div>
                                </div>

                            </div>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <span runat="server" id="itemPlaceholder" />
                        </LayoutTemplate>
                    </asp:ListView>

                    <p class="more-all show-mobile"><a class="tit-link" href='<%= hdnLinkOld.Value %>'>Xem tất cả tác phẩm</a></p>
                </div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="main-bottom mobile-s980">
            <div class="box-asibar">
                <div class="box-resgister">
                    <h2>Đăng kí tham gia cuộc thi</h2>
                    <div class="description">
                        <p>Hãy là những người đầu tiên tham gia cuộc thi </p>
                        <p><strong>HÃY THỂ HIỆN TÀI NĂNG CỦA BẠN - CÙNG GOsmart XÂY DỰNG CUỘC SỐNG TỐT ĐẸP</strong></p>
                    </div>
                    <p class="more-gis">
                        <a href="javarscript:void(0);" data-toggle="modal" data-target='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "#registerbox" : "#risterlogins" %>'>Đăng kí</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphPopup" runat="Server">
    <a class="click-loginbox" href="javascript:void(0);" data-toggle="modal" data-target="#registerbox"></a>
    <a href="javascript:void(0);" class="show-accss" data-toggle="modal" data-target="#CDHAcc"></a>
    <!-- Modal -->
    <div class="modal fade" id="registerbox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg contest-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h2 class="modal-title" id="myModalLabel">gửi tác phẩm tham gia cuộc thi</h2>
                </div>
                <div class="modal-body">
                    <fieldset class="send-wrap">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tác giả/ Nhóm</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtFullName" CssClass="form-control" placeholder="-- Nhập họ tên --" runat="server" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator7" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtFullName"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Số điện thoại</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtPhone" CssClass="form-control" runat="server" Text="" placeholder="-- Nhập số điện thoại --"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator4" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtPhone"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator CssClass="lb-error" ID="RegularExpressionValidator2"
                                    runat="server" ValidationGroup="Information" ControlToValidate="txtPhone" ErrorMessage="Vui lòng nhập số!"
                                    ValidationExpression="^[0-9]+$" Display="Dynamic"
                                    ForeColor="Red"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" Text="" placeholder="-- Nhập email --"></asp:TextBox>
                                <asp:RegularExpressionValidator CssClass="lb-error" ID="RegularExpressionValidator1"
                                    runat="server" ValidationGroup="Information" ControlToValidate="txtEmail" ErrorMessage="Sai định dạng email!"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"
                                    ForeColor="Red"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator1" runat="server"
                                    ValidationGroup="Information" ControlToValidate="txtEmail" ErrorMessage="Thông tin bắt buộc!"
                                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Chủ đề tác phẩm</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtTitle" CssClass="form-control" runat="server" placeholder="-- Chủ đề không quá 50 ký tự --" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator2" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtTitle"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mô tả</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtDescription" CssClass="form-control text-area" runat="server" TextMode="MultiLine" placeholder="-- Bạn hãy mô tả tác phẩm của bạn. Nội dung không quá 300 ký tự --" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator3" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtDescription"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Clips/ Video</label>
                            <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator5" runat="server"
                                Display="Dynamic" ValidationGroup="Information" ControlToValidate="FileUploadVideo"
                                ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            <div class="col-sm-9">
                                <asp:FileUpload ID="FileUploadVideo" CssClass="file-upload" runat="server" />
                                <%--<p class="node-send">( Bạn chịu trách nhiệm toàn bộ về thông tin bạn upload ở LivePlus )</p>--%>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ảnh đại diện Clips/ Video</label>
                            <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator6" runat="server"
                                Display="Dynamic" ValidationGroup="Information" ControlToValidate="FileUploadImage"
                                ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            <div class="col-sm-9">
                                <asp:FileUpload ID="FileUploadImage" CssClass="file-upload" runat="server" />
                                <p class="node-send">( Bạn chịu trách nhiệm toàn bộ về thông tin bạn upload ở LivePlus )</p>
                            </div>

                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <div class="wrap-btn">
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <asp:Button ID="btnUpLoad" OnClick="btnUpLoad_Click" CssClass="btn-send" ValidationGroup="Information" runat="server" Text="upload tác phẩm của bạn" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade popupbox" id="CDHAcc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <uc1:successCDH runat="server" ID="successCDH" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="clientScript" runat="Server">
<script type="text/javascript">
        $(document).ready(function () {
            //Goi Upload Choi De Hieu video
            <%if (Session["LoginCMD"] != null)
              { %>
              <%if (Session["LoginCMD"].ToString() == "CDH")
                { %>
            popuchoihieuporder();
            <% Session["LoginCMD"] = ""; %>
            <% }
                } %>
            //Goi Success Choi De Hieu video
            <%if (Session["Success"] != null)
              { %>
            <%if (Session["Success"].ToString() == "true")
              { %>
            popupchoihieuresult();
            <% Session["Success"] = ""; %>
            <% }
                } %>
        });</script>
</asp:Content>
