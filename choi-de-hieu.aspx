﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site.master" AutoEventWireup="true" CodeFile="choi-de-hieu.aspx.cs" Inherits="choi_de_hieu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <p class="site-node">Chơi để hiểu</p>
        <div class="wrap-main">
            <div id="col-right">
                <div class="box-asibar">
                    <div class="box-resgister desktop-s980">
                        <h2>Đăng kí tham gia cuộc thi</h2>
                        <div class="description">
                            <p>Hãy là những người đầu tiên tham gia cuộc thi </p>
                            <p><strong>HÃY THỂ HIỆN TÀI NĂNG CỦA BẠN - CÙNG LIVEPLUS XÂY DỰNG CUỘC SỐNG TỐT ĐẸP</strong></p>
                        </div>
                        <%--<p class="more-gis">
                            <a href='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "javarscript:void(0);" : "Login.aspx?ReturnUrl=" + Page.Request.Url.AbsolutePath.ToString() %>' data-toggle="modal" data-target='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "#registerbox" : "" %>'>Đăng kí</a>
                        </p>--%>
                        <p class="more-gis"><a href="javascript:void(0);" data-toggle="modal" data-target="#popuploginreg">Đăng kí</a></p>
                        <p class="more-gis"><a href="javascript:void(0);" data-toggle="modal" data-target="#registerbox">Đăng kí</a></p>
                    </div>
                    <h2 class="title-3">Tác phẩm đoạt giải</h2>
                    <div class="list-month">
                        <div class="row">
                            <asp:ListView ID="ListView2" runat="server"
                                DataSourceID="ObjectDataSource1"
                                EnableModelValidation="True">
                                <ItemTemplate>
                                    <div class="col-md-12 col-xs-6">
                                        <div class="box-month">
                                            <a class="month-a" href='<%# progressTitle(Eval("Title")) + "-ppd-" + Eval("LearnsID") +".aspx" %>'>
                                                <span class="month-name"><span class="month-ro">Kỳ <strong><%# Eval("Periodic") %></strong></span></span>
                                                <span class="month-img">
                                                    <img id="Img1" alt='<%# Eval("ImagePath") %>' src='<%# "http://www.liveplus.vn/res/learns/video/thumbs/" + Eval("ImagePath") %>'
                                                        visible='<%# string.IsNullOrEmpty( Eval("ImagePath").ToString()) ? false : 
true %>'
                                                        runat="server" class="img-responsive" />
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <LayoutTemplate>
                                    <span runat="server" id="itemPlaceholder" />
                                </LayoutTemplate>
                            </asp:ListView>
                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
                                SelectMethod="LearnsSelectAll" TypeName="TLLib.Learns">
                                <SelectParameters>
                                    <asp:Parameter DefaultValue="1" Name="StartRowIndex" Type="String" />
                                    <asp:Parameter Name="EndRowIndex" Type="String" DefaultValue="5" />
                                    <asp:Parameter Name="Title" Type="String" />
                                    <asp:Parameter Name="Description" Type="String" />
                                    <asp:Parameter DefaultValue="2" Name="LearnsCategoryID" Type="String" />
                                    <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                                    <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                                    <asp:Parameter DefaultValue="" Name="IsNew" Type="String" />
                                    <asp:Parameter Name="Createby" Type="String" />
                                    <asp:Parameter Name="Priority" Type="String" />
                                    <asp:Parameter Name="InGallery" Type="String" />
                                    <asp:Parameter DefaultValue="true" Name="IsWinner" Type="String" />
                                    <asp:Parameter Name="Name" Type="String" />
                                    <asp:Parameter Name="Email" Type="String" />
                                    <asp:Parameter Name="Phone" Type="String" />
                                    <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </div>
                    </div>
                </div>
            </div>
            <div id="col-left">
                <%--<p class="node-p">Đăng tin: <strong>Admin</strong></p>--%>
                <h1 class="title">
                    <asp:Label ID="lbName" runat="server"></asp:Label></h1>
                <asp:HiddenField ID="hdnCID" runat="server" />
                <div class="box-like">
                    <div class="box-like-b">
                        <div class="fb-like fb-like-1" data-href='<%= Page.Request.Url.AbsolutePath %>'
                            data-send="false" data-layout="button_count" data-width="50" data-show-faces="false">
                        </div>
                        <div class="fb-share-button" data-href='<%= Page.Request.Url.AbsolutePath %>' data-width="70"></div>
                    </div>
                </div>
                <div class="wrap-des">
                    <div class="tab-content content-info">

                        <asp:HiddenField ID="hdnPictureCategoryID" runat="server" />

                        <asp:ListView ID="ListView3" runat="server"
                            DataSourceID="ObjectDataSource2"
                            EnableModelValidation="True">
                            <ItemTemplate>
                                <h3 class="tit-des show-mobile"><a href="#desTab1">Thể lệ cuộc thi</a></h3>
                                <div class="tab-pane" id="desTab1">
                                    <div class="description">
                                        <div class="text-box">
                                            <%# Eval("Description") %>
                                        </div>
                                        <%--Ban Tổ chức xin thông báo <strong>Cuộc thi Giao thông thông minh</strong> trên internet năm học 2012-2013 (Quyết định trao giải thưởng của Ủy ban được gửi đính kèm cùng thông báo này). Giải thưởng của các thí sinh đạt giải được Ban Tổ chức chuyển qua đường bưu điện cho Hiệu trưởng của trường có thí sinh đạt giải (trong tháng 4/2014). Mọi thắc mắc xin liên hệ 0988122473 (đ/c Nguyễn Giao Linh -Văn phòng Ủy ban ATGT Quốc gia).--%>
                                    </div>
                                </div>
                                <h3 class="tit-des show-mobile"><a href="#desTab2">Giải thưởng</a></h3>
                                <div class="tab-pane" id="desTab2">
                                    <div class="description">
                                        <div class="text-box">
                                            <%# Eval("Content") %>
                                        <//div>
                                        <%--Ban Tổ chức xin thông báo Cuộc thi Giao thông thông minh trên internet năm học 2012-2013 (Quyết định trao giải thưởng của Ủy ban được gửi đính kèm cùng thông báo này). Giải thưởng của các thí sinh đạt giải được Ban Tổ chức chuyển qua đường bưu điện cho Hiệu trưởng của trường có thí sinh đạt giải (trong tháng 4/2014). Mọi thắc mắc xin liên hệ 0988122473 (đ/c Nguyễn Giao Linh -Văn phòng Ủy ban ATGT Quốc gia).--%>
                                    </div>
                                </div>
                                <h3 class="tit-des show-mobile"><a href="#desTab3">Hình thức tham gia</a></h3>
                                <div class="tab-pane" id="desTab3">
                                    <div class="description">
                                        <div class="text-box">
                                            <%# Eval("DescriptionEn") %>
                                        </div>
                                        <%--Ban Tổ chức xin thông báo Cuộc thi Giao thông thông minh trên internet năm học 2012-2013 (Quyết định trao giải thưởng của Ủy ban được gửi đính kèm cùng thông báo này). Giải thưởng của các thí sinh đạt giải được Ban Tổ chức chuyển qua đường bưu điện cho Hiệu trưởng của trường có thí sinh đạt giải (trong tháng 4/2014). Mọi thắc mắc xin liên hệ 0988122473 (đ/c Nguyễn Giao Linh -Văn phòng Ủy ban ATGT Quốc gia).--%>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server"
                            SelectMethod="LearnsCategorySelectAllFirst" TypeName="TLLib.LearnsCategory">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="2" Name="ParentID1" Type="String" />
                                <asp:Parameter DefaultValue="2" Name="parentID" Type="Int32" />
                                <asp:Parameter DefaultValue="1" Name="increaseLevelCount" Type="Int32" />
                                <asp:Parameter DefaultValue="" Name="IsShowOnMenu" Type="String" />
                                <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                    </div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" id="desTab">
                        <li><a href="#desTab1" role="tab" data-toggle="tab">Thể lệ cuộc thi</a></li>
                        <li><a href="#desTab2" role="tab" data-toggle="tab">Giải thưởng</a></li>
                        <li><a href="#desTab3" role="tab" data-toggle="tab">Hình thức tham gia</a></li>
                    </ul>
                    <p class="node-i">( Click vào button link trên để xem thêm nội dung )</p>
                </div>
                <div class="head">
                    <h2 class="title-4"><span>Tác phẩm dự thi mới nhất</span></h2>
                    <%--<a class="tit-link show-desktop" href='<%=  Page.Request.Url.AbsolutePath.ToString() %>'>Xem tất cả tác phẩm dự thi</a>--%>
                </div>
                <div class="pro-table">
                    <div class="row">
                        <asp:ListView ID="ListView1" runat="server"
                            EnableModelValidation="True">
                            <ItemTemplate>
                                <div class="col-xs-6 col-lg-4">
                                    <div class="box-articles">
                                        <div class="articles-content articles-h">
                                            <p class="line">Đăng tin: <span><%# Eval("Name") %></span></p>
                                            <p>Chủ đề: <span><%# Eval("Title") %></span></p>
                                        </div>
                                        <a class="articles-img" href='<%# progressTitle(Eval("Title")) + "-ppd-" + Eval("LearnsID") +".aspx" %>'>
                                            <img id="Img2" alt='<%# Eval("ImagePath") %>' src='<%# "http://www.liveplus.vn/res/learns/picture/" + Eval("ImagePath") %>'
                                                visible='<%# string.IsNullOrEmpty( Eval("ImagePath").ToString()) ? false : 
true %>'
                                                runat="server" class="img-responsive" />
                                            <span class="box-text">
                                                <span class="text-1" style='<%# Eval("Voted").ToString() == "0"? "display: none;": ""%>'><strong><%# Eval("Voted")%></strong> bình luận</span>
                                                <span class="text-2" style='<%# Eval("Views").ToString() == "0"? "display: none;": ""%>'><strong><%# Eval("Views")%></strong> lượt xem</span>
                                            </span>
                                        </a>
                                        <p class="date"><%# string.Format("{0:dd.MM.yyyy}", Eval("CreateDate"))%> </p>
                                        <div class="articles-content articles-b">
                                            <a href="#" class="reques">báo trùng</a>
                                            <a href='<%# progressTitle(Eval("Title")) + "-ppd-" + Eval("LearnsID") +".aspx" %>' class="link-com">Bình chọn</a>
                                            <div class="like-box">
                                                <div class="fb-share-button button" data-href='<%# Page.Request.Url.Host.ToString() + ""+ progressTitle(Eval("Title")) + "-ppd-" + Eval("LearnsID") +".aspx"%>' data-width="70" data-height="30"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                          
                        </div>
                    </div>

                    <%--<p class="more-all show-mobile"><a class="tit-link" href='<%=  Page.Request.Url.AbsolutePath.ToString() %>'>Xem tất cả tác phẩm dự thi</a></p>--%>
                </div>
                
                <asp:ListView ID="ListView4" runat="server"
                    EnableModelValidation="True">
                    <ItemTemplate>
                        <div class="head head-bg">
                            <h2 class="title-4"><span>Tác phẩm cuộc thi trước</span></h2>
                            <a class="tit-link show-desktop" href='<%# progressTitle(Eval("LearnsCategoryName")) + "-pp-" + Eval("LearnsCategoryID") +".aspx" %>'>Xem tất cả tác phẩm</a>
                        </div>
                        <div class="pro-table">
                            <div class="col-xs-6 col-lg-4">
                                <div class="box-articles">
                                    <div class="articles-content articles-h">
                                        <p class="line">Đăng tin: <span><%# Eval("Name") %></span></p>
                                        <p>Chủ đề: <span><%# Eval("Title") %></span></p>
                                    </div>
                                    <a class="articles-img bcover" href='<%# progressTitle(Eval("Title")) + "-ppd-" + Eval("LearnsID") +".aspx" %>'>
                                        <img id="Img2" alt='<%# Eval("ImagePath") %>' src='<%# "http://www.liveplus.vn/res/learns/picture/" + Eval("ImagePath") %>'
                                            visible='<%# string.IsNullOrEmpty( Eval("ImagePath").ToString()) ? false : 
true %>'
                                            runat="server" class="img-responsive" />
                                        <span class="box-text">
                                            <span class="text-1" style='<%# Eval("Voted").ToString() == "0"? "display: none;": ""%>'><strong><%# Eval("Voted")%></strong> bình luận</span>
                                            <span class="text-2" style='<%# Eval("Views").ToString() == "0"? "display: none;": ""%>'><strong><%# Eval("Views")%></strong> lượt xem</span>
                                        </span>
                                    </a>
                                    <p class="date"><%# string.Format("{0:dd.MM.yyyy}", Eval("CreateDate"))%> </p>
                                    <div class="articles-content articles-b">
                                        <a href="#" class="reques">báo trùng</a>
                                        <a href='<%# progressTitle(Eval("Title")) + "-ppd-" + Eval("LearnsID") +".aspx" %>' class="link-com">Bình chọn</a>
                                        <div class="like-box">
                                            <div class="fb-share-button button" data-href='<%# Page.Request.Url.Host.ToString() + ""+ progressTitle(Eval("Title")) + "-ppd-" + Eval("LearnsID") +".aspx"%>' data-width="70" data-height="30"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p class="more-all show-mobile"><a class="tit-link" href='<%# progressTitle(Eval("LearnsCategoryName")) + "-pp-" + Eval("LearnsCategoryID") +".aspx" %>'>Xem tất cả tác phẩm</a></p>
                        </div>

                    </ItemTemplate>
                    <LayoutTemplate>
                        <span runat="server" id="itemPlaceholder" />
                    </LayoutTemplate>
                </asp:ListView>
                <div class="clr"></div>
            </div>
        </div>
        <div class="main-bottom mobile-s980">
            <div class="box-asibar">
                <div class="box-resgister">
                    <h2>Đăng kí tham gia cuộc thi</h2>
                    <div class="description">
                        <p>Hãy là những người đầu tiên tham gia cuộc thi </p>
                        <p><strong>HÃY THỂ HIỆN TÀI NĂNG CỦA BẠN - CÙNG LIVEPLUS XÂY DỰNG CUỘC SỐNG TỐT ĐẸP</strong></p>
                    </div>
                    <p class="more-gis">
                        <a href='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "javarscript:void(0);" : "Login.aspx?ReturnUrl=" + Page.Request.Url.AbsolutePath.ToString() %>' data-toggle="modal" data-target='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "#registerbox" : "" %>'>Đăng kí</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphPopup" runat="Server">
    <!-- Modal -->
    <div class="modal fade" id="registerbox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg contest-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h2 class="modal-title" id="myModalLabel">gửi tác phẩm tham gia cuộc thi</h2>
                </div>
                <div class="modal-body">
                    <fieldset class="send-wrap">
                        <%--<div class="form-group">
                            <label class="col-sm-3 control-label">Tác phẩm dự thi</label>
                            <div class="col-sm-9">
                                <asp:RadioButtonList ID="RadioButtonList1" CssClass="check-list" runat="server"
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem>thông điệp</asp:ListItem>
                                    <asp:ListItem>ảnh</asp:ListItem>
                                    <asp:ListItem>clips/ video</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>--%>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tác giả/ Nhóm</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtFullName" CssClass="form-control" placeholder="-- Nhập họ tên --" runat="server" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator7" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtFullName"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Số điện thoại</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtPhone" CssClass="form-control" runat="server" Text="" placeholder="-- Nhập số điện thoại --"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator4" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtPhone"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" Text="" placeholder="-- Nhập email --"></asp:TextBox>
                                <asp:RegularExpressionValidator CssClass="lb-error" ID="RegularExpressionValidator1"
                                    runat="server" ValidationGroup="Information" ControlToValidate="txtEmail" ErrorMessage="Sai định dạng email!"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"
                                    ForeColor="Red"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator1" runat="server"
                                    ValidationGroup="Information" ControlToValidate="txtEmail" ErrorMessage="Thông tin bắt buộc!"
                                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Chủ đề tác phẩm</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtTitle" CssClass="form-control" runat="server" placeholder="-- Chủ đề không quá 50 ký tự --" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator2" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtTitle"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mô tả</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="txtDescription" CssClass="form-control text-area" runat="server" TextMode="MultiLine" placeholder="-- Bạn hãy mô tả tác phẩm của bạn. Nội dung không quá 300 ký tự --" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator3" runat="server"
                                    Display="Dynamic" ValidationGroup="Information" ControlToValidate="txtDescription"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ảnh dự thi</label>
                            <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator6" runat="server"
                                Display="Dynamic" ValidationGroup="Information" ControlToValidate="FileUploadImage"
                                ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            <div class="col-sm-9">
                                <asp:FileUpload ID="FileUploadImage" CssClass="file-upload" runat="server" maxRequestLength="5000000" />
                                <p class="node-send">( Bạn chịu trách nhiệm toàn bộ về thông tin bạn upload ở LivePlus )</p>
                            </div>

                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <div class="wrap-btn">
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3">
                                <asp:Button ID="btnUpLoad" OnClick="btnUpLoad_Click" CssClass="btn-send" ValidationGroup="Information" runat="server" Text="upload tác phẩm của bạn" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <a href="javascript:void(0);" class="show-popup-1" data-toggle="modal" data-target="#myModal"></a>
    <a class="click-link" href="javascript:void(0);" data-toggle="modal" data-target="#registerbox"></a>
    <div class="modal fade popupbox" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title">Bạn đã upload file thành công. Cảm ơn bạn đã chung tay xây dựng Live Plus.</h4>
          </div>
          <div class="modal-body">
            <p>Bạn được cộng một điểm thành viên tích cực....</p>
            <p><strong>Cố gắng lên, giải thưởng chuyến du lịch Singapore cho 3 thành viên tích cực nhất của năm đang chờ bạn</strong></p>
            <p><a href="#">Xem thêm về quyền lợi thành viên tại đây</a></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
</asp:Content>

