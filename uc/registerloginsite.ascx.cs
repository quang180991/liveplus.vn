﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TLLib;
using System.Web.Script.Serialization;
using ASPSnippets.FaceBookAPI;
using System.Configuration;

public partial class uc_register : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["cmd"] != null)
        {
            hdnCMD.Value = Request.QueryString["cmd"].ToString();
        }
        if (!IsPostBack)
        {
            FaceBookConnect.API_Key = "844943575526605";
            FaceBookConnect.API_Secret = "05514477cab9be7c4b98bc90dbe304c9";
            if (Request.QueryString["error"] == "access_denied")
            {
                //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('User has denied access.')", true);
                Common.ShowAlert("User has denied access.");
                return;
            }

            string code = Request.QueryString["code"];
            if (!string.IsNullOrEmpty(code))
            {
                string data = FaceBookConnect.Fetch(code, "me");
                FaceBookUser faceBookUser = new JavaScriptSerializer().Deserialize<FaceBookUser>(data);
                faceBookUser.pictureurl = string.Format("https://graph.facebook.com/{0}/picture", faceBookUser.id);
                var strFirstName = faceBookUser.name;
                var strEmail = faceBookUser.email;
                var strUserName = strEmail;

                var strAddress1 = "";
                var strIsPrimary = "True";
                var strIsPrimaryBilling = "True";
                var strIsPrimaryShipping = "True";
                var strRoleName = "users";
                var strFacebookID = faceBookUser.id;
                var strFacebookLink = faceBookUser.link;
                var strPassword = MD5Hash.Encrypt("123456", true);
                var oUser = new User();
                var oAddressBook = new AddressBook();
                if (!string.IsNullOrEmpty(strUserName))
                {
                    if (Membership.GetUser(strUserName) != null)
                    {
                        FormsAuthentication.SetAuthCookie(strUserName, true);
                        Session["UpLoadGoSee"] = "true";
                        Response.Redirect(Page.Request.Url.AbsolutePath.ToString());
                    }
                    else
                    {
                        if (Membership.FindUsersByEmail(strEmail).Count == 0)
                        {
                            oUser.UserInsert(strUserName, strEmail, strPassword, strRoleName);
                            oAddressBook.AddressBookInsert1(
                                strFirstName,
                                "",
                                strEmail,
                                "",
                                "",
                                "",
                                strUserName,
                                "",
                                strAddress1,
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                strIsPrimary,
                                strIsPrimaryBilling,
                                strIsPrimaryShipping,
                                strRoleName,
                                strFacebookID,
                                strFacebookLink,
                                strPassword
                                );
                            FormsAuthentication.SetAuthCookie(strUserName, true);
                            Session["LoginCMD"] = hdnCMD.Value;
                            Response.Redirect(Page.Request.UrlReferrer.ToString());
                        }
                        else
                            Response.Redirect(Page.Request.UrlReferrer.ToString());
                    }
                }

                //pnlFaceBookUser.Visible = true;
                //lblId.Text = faceBookUser.id;
                //lblUserName.Text = faceBookUser.username;
                //lblName.Text = faceBookUser.name;
                //lblEmail.Text = faceBookUser.email;
                //lblLink.Text = faceBookUser.link;
                //ProfileImage.ImageUrl = faceBookUser.pictureurl;
                //btnLogin.Enabled = false;
            }
        }
    }

    protected void btnDangKy_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string strUserName = txtUserName1.Text.Trim();
            string strPassWord = txtPassWord1.Text;
            string strEmail = txtEmail.Text.Trim();
            string strFullName = txtFullName.Text.Trim();
            string strAddress = txtAddress.Text.Trim();
            string strPhone = txtPhone.Text.Trim();
            string strImageName = System.IO.Path.GetFileName(fileImageName.FileName);
            bool bError = false;
            if (!string.IsNullOrEmpty(strUserName))
            {
                if (Membership.GetUser(strUserName) != null)
                {
                    CustomValidator1.ErrorMessage = "<b>+ Tên truy cập " + strUserName + " đã được đăng ký sử dụng, vui lòng chọn tên khác</b>";
                    CustomValidator1.IsValid = false;
                    bError = true;
                }
                else
                    CustomValidator1.IsValid = true;
            }

            if (Membership.FindUsersByEmail(strEmail).Count > 0)
            {
                CustomValidator2.ErrorMessage = "<b>+ Email " + strEmail + " đã được đăng ký sử dụng, vui lòng chọn Email khác</b>";
                CustomValidator2.IsValid = false;
                bError = true;
            }
            else
                CustomValidator2.IsValid = true;

            if (!bError)
            {
                var oUser = new User();
                var oUserProfile = new UserProfile();

                oUser.UserInsert(strUserName, strEmail, strPassWord, "users");

                //string strUserID = Membership.GetUser(strUserName).ProviderUserKey.ToString();

                var oAddressBook = new AddressBook();
                strImageName = oAddressBook.AddressBookInsert2(
                    strFullName,
                    "",
                    strEmail,
                    strPhone,
                    "",
                    "",
                    strUserName,
                    "",
                    strAddress,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "True",
                    "True",
                    "True",
                    "users",
                    "",
                    "",
                    "",
                    strImageName,
                    "");

                string strFullPath = "~/res/userprofile/" + strImageName;
                if (!string.IsNullOrEmpty(strImageName))
                {
                    fileImageName.SaveAs(Server.MapPath(strFullPath));
                    ResizeCropImage.ResizeByCondition(strFullPath, 100, 100);
                }

                new Newsletter().NewsletterInsert(strEmail);
                FormsAuthentication.SetAuthCookie(strUserName, true);
                SendMail();
                Common.ShowAlert("Đăng ký thành công.");
                Session["RegisterAcc"] = "true";
                Response.Redirect(Page.Request.UrlReferrer.ToString());
            }
        }
    }
    protected void SendMail()
    {
        string strUserName1 = txtUserName1.Text.Trim();
        string strEmail = txtEmail.Text.Trim();
        string strFullName = txtFullName.Text.Trim();
        string strAddress = txtAddress.Text.Trim();
        string strPhone = txtPhone.Text.Trim();
        string msg = "<h3>Chúc mừng bạn đã là thành viên của website <a href='www.liveplus.vn'>www.liveplus.vn</a>.</h3>";
        msg += "<p>Cảm ơn bạn đã quan tâm đến dự án Live Plus, dự án triển khai nhằm giảm thiểu tỉ lệ tai nạn giao thông ở Việt Nam. Bằng cách đăng ký là thành viên của chương trình, bạn sẽ cùng chúng tôi chung sức xây dựng một Việt Nam văn minh và bền vững.</p>";
        msg += "-------------------------------------------------------------------------------------------<br/><br/>";
        msg += "<h3>Chúng tôi xin xác nhận thông tin của bạn như sau:</h3>";
        msg += "<b>UserName: </b>" + strUserName1 + "<br /><br />";
        msg += "<b>Email: </b>" + strEmail + "<br /><br />";
        msg += "<b>Họ tên: </b>" + strFullName + "<br /><br />";
        msg += "<b>Địa chỉ: </b>" + strAddress + "<br /><br />";
        msg += "<b>Điện thoại: </b>" + strPhone + "<br /><br />";
        msg += "-------------------------------------------------------------------------------------------<br/><br/>";
        msg += "<p>Nếu bạn có thắc mắc về các chương trình được triển khai hoặc các thông tin trên cổng thông tin LivePlus.vn, bạn có thể liên hệ trực tiếp với Ban Quản lý dự án theo thông tin sau:</p>";
        msg += "<h3>Thông tin liên hệ của dự án</h3>";
        msg += "<b>Email: </b><a href='mailto:hotro@liveplus.vn'>hotro@liveplus.vn</a><br /><br />";
        msg += "<b>Điện thoại: </b>090.626.0928<br />";
        msg += "<b>Địa chỉ: </b>Chương trình Gosmart<br />";
        msg += "<span>Thực hiện bởi Công ty TUV NORD Vietnam</span><br />";
        msg += "<span>P803 Tòa nhà Thăng Long - 105 Láng Hạ - Phường Láng Hạ - Quận Đống Đa - Hà Nội</span><br />";
        msg += "<b>Website chính thức của chương trình: </b> <a href='www.liveplus.vn'>www.liveplus.vn</a><br/>";
        msg += "<p style='text-align: center;'>Thực hiện bởi Công ty TUV NORD Vietnam</p>";
        msg += "<p style='text-align: center;'>P803 Tòa nhà Thăng Long - 105 Láng Hạ - Phường Láng Hạ - Quận Đống Đa - Hà Nội</p>";



        var fileMap = new ExeConfigurationFileMap() { ExeConfigFilename = Server.MapPath("~/config/app.config") };
        var config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
        var section = (AppSettingsSection)config.GetSection("appSettings");
        var strUseSSL = section.Settings["UseSSL"].Value;
        var strPort = section.Settings["Port"].Value;
        var strHost = section.Settings["Host"].Value;
        int n;
        bool isNumeric = int.TryParse(strPort, out n);
        var iPort = isNumeric ? n : 0;
        var strMailFrom = section.Settings["Email"].Value;
        var strUserName = section.Settings["UserName"].Value;
        var strPassword = TLLib.MD5Hash.Decrypt(section.Settings["Password"].Value, true);
        var strReceivedEmails = section.Settings["ReceivedEmails"].Value;
        var bEnableSsl = strUseSSL.ToLower() == "true" ? true : false;
        var strSubject = "Chúc mừng bạn đã trở thành thành viên Live Plus";
        var strBody = msg;
        var lstMailTo = new List<string>() { strReceivedEmails, txtEmail.Text.ToString() };
        var lstCC = "";
        var lstAttachment = new List<string>();
        foreach (string item in lstMailTo)
        {

            var bSendSuccess = TLLib.Common.SendMail(
                    strHost,
                    iPort,
                    strMailFrom,
                    strUserName,
                    strPassword,
                    item,
                    lstCC,
                    strSubject,
                    strBody,
                    bEnableSsl
                );
        }
    }
    protected void Login1_LoggedIn(object sender, EventArgs e)
    {
        string UserName = Login1.UserName;
        //MembershipUser mu = Membership.GetUser(UserName);
        //Session["PWD"] = Login1.Password;
        if (UserName != null)
        {
            Session["UserName"] = UserName;
            Session["LoginCMD"] = hdnCMD.Value;
            Response.Redirect(Page.Request.UrlReferrer.ToString());
        }
    }
    protected void lkbFacebook_Click(object sender, EventArgs e)
    {
        FaceBookConnect.Authorize("user_photos,email,user_location", Request.Url.AbsoluteUri.Split('?')[0]);
    }
    protected void txtUserName1_TextChanged(object sender, EventArgs e)
    {
        string strUserName = txtUserName1.Text.Trim();
        bool bError = false;
        if (!string.IsNullOrEmpty(strUserName))
        {
            if (Membership.GetUser(strUserName) != null)
            {
                CustomValidator1.ErrorMessage = "<b>Tên truy cập " + strUserName + " đã được đăng ký sử dụng, vui lòng chọn tên khác</b>";
                CustomValidator1.IsValid = false;
                bError = true;
            }
            else
                CustomValidator1.IsValid = true;
        }
        else
            CustomValidator2.IsValid = true;

    }
    protected void txtEmail_TextChanged(object sender, EventArgs e)
    {

        string strEmail = txtEmail.Text.Trim();
        bool bError = false;
        if (Membership.FindUsersByEmail(strEmail).Count > 0)
        {
            CustomValidator2.ErrorMessage = "<b>Email " + strEmail + " đã được đăng ký sử dụng, vui lòng chọn Email khác</b>";
            CustomValidator2.IsValid = false;
            bError = true;
        }
        else
            CustomValidator2.IsValid = true;
    }
}
public class FaceBookUser
{
    public string id { get; set; }
    public string name { get; set; }
    public string username { get; set; }
    public string pictureurl { get; set; }
    public string email { get; set; }
    public string link { get; set; }
    public FaceBookEntity location { get; set; }
}
public class FaceBookEntity
{
    public string id { get; set; }
    public string name { get; set; }
}