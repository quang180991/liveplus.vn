﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASPSnippets.FaceBookAPI;
using TLLib;

public partial class uc_registeredCTV : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        bool IsLogin = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
        if (IsLogin == true)
        {
            string UserName = System.Web.HttpContext.Current.User.Identity.Name;

            if (Roles.IsUserInRole(UserName, "partners") == false)
            {
                if (chkAgree.Checked == true)
                {
                    Roles.AddUserToRole(UserName, "partners");
                    Session["Success"] = "true";
                    Response.Redirect(Page.Request.UrlReferrer.ToString());
                }
                else
                {
                    lbCheck.Visible = true;
                    lbCheck.Text = "Thông tin bắt buộc";
                }
            }
            else
            {

                //lbCheck.Visible = true;
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "runtime", "Bạn đã trở thành cộng tác viên trước đó! ", true);

            }
        }
        else
        {
            Response.Redirect("Login.aspx?ReturnUrl=" + Page.Request.Url.AbsolutePath.ToString());
        }
        chkAgree.Checked = false;
    }
    protected void SendMail()
    {
        bool IsLogin = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
        if (IsLogin == true)
        {
            string UserName = System.Web.HttpContext.Current.User.Identity.Name;
            MembershipUser mu = Membership.GetUser(UserName);
            string userEmail = mu.Email != null ?mu.Email.ToString():"";

            string msg = "<h3>Xin chào" + UserName + ",</h3><br/>";
            msg += "<p>Cảm ơn bạn đã quan tâm đến dự án Live Plus, dự án triển khai nhằm giảm thiểu tỉ lệ tai nạn giao thông ở Việt Nam. Live Plus ghi nhận việc bạn trở thành cộng tác viên của chương trình và hy vọng bạn sẽ cùng chúng tôi chung sức vì một Việt Nam văn minh và bền vững.</p><br/>";
            msg += "<p>Chúng tôi sẽ thông tin đến bạn về các hoạt động của dự án. Nếu bạn có thắc mắc về các chương trình được triển khai, bạn có thể liên hệ trực tiếp với Ban Quản lý dự án theo thông tin sau:</p><br/>";
            msg += "<h3>Thông tin liên hệ của dự án</h3><br/>";
            msg += "<b>Email: </b><a href='mailto:hotro@liveplus.vn'>hotro@liveplus.vn</a><br /><br />";
            msg += "<b>Điện thoại: </b>090.626.0928<br />";
            msg += "<p><b>Địa chỉ: </b>Chương trình Gosmart<br />";
            msg += "Thực hiện bởi Công ty TUV NORD Vietnam<br />";
            msg += "P803 Tòa nhà Thăng Long - 105 Láng Hạ - Phường Láng Hạ - Quận Đống Đa - Hà Nội<br />";
            msg += "<b>Website chính thức của chương trình: </b> <a href='www.liveplus.vn'>www.liveplus.vn</a></p><br/>";
        

            var fileMap = new ExeConfigurationFileMap() { ExeConfigFilename = Server.MapPath("~/config/app.config") };
            var config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
            var section = (AppSettingsSection)config.GetSection("appSettings");
            var strUseSSL = section.Settings["UseSSL"].Value;
            var strPort = section.Settings["Port"].Value;
            var strHost = section.Settings["Host"].Value;
            int n;
            bool isNumeric = int.TryParse(strPort, out n);
            var iPort = isNumeric ? n : 0;
            var strMailFrom = section.Settings["Email"].Value;
            var strUserName = section.Settings["UserName"].Value;
            var strPassword = TLLib.MD5Hash.Decrypt(section.Settings["Password"].Value, true);
            var strReceivedEmails = section.Settings["ReceivedEmails"].Value;
            var bEnableSsl = strUseSSL.ToLower() == "true" ? true : false;
            var strSubject = "Chúc mừng bạn đã trở thành thành viên Live Plus";
            var strBody = msg;
            var lstMailTo = new List<string>() { strReceivedEmails, userEmail };
            var lstCC = "";
            var lstAttachment = new List<string>();
            foreach (string item in lstMailTo)
            {

                var bSendSuccess = TLLib.Common.SendMail(
                        strHost,
                        iPort,
                        strMailFrom,
                        strUserName,
                        strPassword,
                        item,
                        lstCC,
                        strSubject,
                        strBody,
                        bEnableSsl
                    );
            }
        }
    }
}
