﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="successBT.ascx.cs" Inherits="uc_successBT" %>
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Bạn đã báo trùng thành công. Cảm ơn bạn đã chung tay xây dựng Live Plus.</h4>
    </div>
    <div class="modal-body">
        <asp:ListView ID="ListView1" runat="server"
            DataSourceID="ObjectDataSource2"
            EnableModelValidation="True">
            <ItemTemplate>
                <%# Eval("Content") %>
            </ItemTemplate>
            <LayoutTemplate>
                <span runat="server" id="itemPlaceholder" />
            </LayoutTemplate>
        </asp:ListView>
        <p>
            <a href="cong-tac-vien-quy-dinh.aspx">Xem thêm về quyền lợi thành viên tại đây</a>
        </p>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">
            Close</button>
    </div>
</div>
<asp:ObjectDataSource ID="ObjectDataSource2" runat="server"
    SelectMethod="AggregatedSelectAll" TypeName="TLLib.Aggregated">
    <SelectParameters>
        <asp:Parameter Name="StartRowIndex" Type="String" />
        <asp:Parameter Name="EndRowIndex" Type="String" />
        <asp:Parameter Name="Keyword" Type="String" />
        <asp:Parameter Name="AggregatedTitle" Type="String" />
        <asp:Parameter Name="Description" Type="String" />
        <asp:Parameter Name="AggregatedCategoryID" Type="String" DefaultValue="13" />
        <asp:Parameter Name="Tag" Type="String" />
        <asp:Parameter DefaultValue="" Name="IsShowOnHomePage" Type="String" />
        <asp:Parameter Name="IsHot" Type="String" />
        <asp:Parameter Name="IsNew" Type="String" />
        <asp:Parameter Name="FromDate" Type="String" />
        <asp:Parameter Name="ToDate" Type="String" />
        <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
        <asp:Parameter DefaultValue="" Name="Priority" Type="String" />
        <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
