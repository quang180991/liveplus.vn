﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uploadGoSee.ascx.cs" Inherits="uc_uploadGoSee" %>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">
        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <p class="modal-title">
       Upload Go Xem Để Cười
    </p>
</div>
<div class="modal-body">
    <div class="wrap-radio">
        <div class="form-group">
            <label class="col-sm-5 control-label">Chọn lựa dạng file upload</label>
            <div class="col-sm-7">
                <asp:RadioButtonList ID="RadioButtonList2" CssClass="check-list" runat="server"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="2">Ảnh</asp:ListItem>
                    <asp:ListItem Value="1" Selected="True">Thông điệp</asp:ListItem>
                    <asp:ListItem Value="3">Video Clip</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
    </div>
    <fieldset class="send-login">
        <div class="form-group">
            <label class="col-sm-3 control-label">Tiêu đề</label>
            <div class="col-sm-8">
                <asp:TextBox ID="txtTitle" CssClass="form-control" placeholder="-- Tiêu đề --" runat="server" Text=""></asp:TextBox>
                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator19" runat="server"
                    Display="Dynamic" ValidationGroup="uploadgosee" ControlToValidate="txtTitle"
                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Nguồn</label>
            <div class="col-sm-8">
                <asp:DropDownList ID="txtOrigin" CssClass="form-control" runat="server">
                    <asp:ListItem>Sưu Tầm</asp:ListItem>
                    <asp:ListItem>Do Tui Làm</asp:ListItem>
                </asp:DropDownList>
                <%--<asp:TextBox ID="txtOrigin" CssClass="form-control" placeholder="-- Nguồn --" runat="server" Text=""></asp:TextBox>
            <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator20" runat="server"
                Display="Dynamic" ValidationGroup="uploadgosee" ControlToValidate="txtOrigin"
                ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>--%>
            </div>
        </div>
        <div class="wrap-uptext">
            <div class="file-upload">
                <div class="form-group upimg">
                    <p><strong>Chọn file ảnh</strong> flie upload không vượt quá 3MB</p>
                    <div class="col-sm-8 col-sm-offset-3">
                        <asp:FileUpload ID="FileImageName" CssClass="file-upload" runat="server" />
                    </div>
                </div>
            </div>
            <div class="file-upload upvideo">
                <div class="form-group">
                    <p><strong>Chọn file ảnh đại diện</strong> flie upload không vượt quá 3MB</p>
                    <div class="col-sm-8 col-sm-offset-3">
                        <asp:FileUpload ID="FileImageVideo" CssClass="file-upload" runat="server" />
                    </div>
                </div>
                <div class="form-group">
                    <p><strong>Chọn file video</strong> flie upload không vượt quá 20MB</p>
                    <div class="col-sm-8 col-sm-offset-3">
                        <asp:FileUpload ID="FileVideoName" CssClass="file-upload" runat="server" />
                    </div>
                </div>
            </div>
            <div class="up-text">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Thông điệp</label>
                    <div class="col-sm-8">
                        <asp:TextBox ID="txtQuote" CssClass="form-control" placeholder="-- Thông điệp --" runat="server" Text=""></asp:TextBox>
                        <%--<asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator21" runat="server"
                        Display="Dynamic" ValidationGroup="uploadgosee" ControlToValidate="txtQuote"
                        ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>--%>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-3">
                    <p class="node-send">( Bạn chịu trách nhiệm toàn bộ về thông tin bạn upload ở LivePlus )</p>
                </div>
            </div>

        </div>

        <div class="form-group">
            <div class="col-sm-8 col-sm-offset-3">
                <asp:Button ID="btnUpload" CssClass="btn-send" OnClick="btnUpload_Click" ValidationGroup="uploadgosee" runat="server" Text="Upload" />
            </div>
        </div>
    </fieldset>

</div>
