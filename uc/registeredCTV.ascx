﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="registeredCTV.ascx.cs" Inherits="uc_registeredCTV" %>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">
        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <p class="modal-title">
        Bạn đăng ký tham gia làm cộng tác viên
    </p>
</div>
<div class="modal-body">
    <div class="content-textd">
        <p>
            Bạn được cộng một điểm thành viên tích cực.
        </p>
        <p>
            <asp:ListView ID="ListView1" runat="server"
            DataSourceID="ObjectDataSource2"
            EnableModelValidation="True">
            <ItemTemplate>
                <%# Eval("Content") %>
            </ItemTemplate>
            <LayoutTemplate>
                <span runat="server" id="itemPlaceholder" />
            </LayoutTemplate>
        </asp:ListView>
        </p>
        <p>
            <a href="cong-tac-vien-quy-dinh.aspx" target="_blank">Xem thêm về quyền lợi thành viên tại đây</a>
        </p>
    </div>
    <asp:CheckBox ID="chkAgree" CssClass="check-a" runat="server" Text="Tôi đồng ý."  />
    <asp:Label ID="lbCheck" runat="server"  Visible="false"></asp:Label>
</div>
<div class="modal-footer">
    <asp:Button ID="btnRegister" OnClick="btnRegister_Click" CssClass="btn-send btn-b" runat="server" Text="Đăng kí" />
</div>
<asp:ObjectDataSource ID="ObjectDataSource2" runat="server"
    SelectMethod="AggregatedSelectAll" TypeName="TLLib.Aggregated">
    <SelectParameters>
        <asp:Parameter Name="StartRowIndex" Type="String" />
        <asp:Parameter Name="EndRowIndex" Type="String" />
        <asp:Parameter Name="Keyword" Type="String" />
        <asp:Parameter Name="AggregatedTitle" Type="String" />
        <asp:Parameter Name="Description" Type="String" />
        <asp:Parameter Name="AggregatedCategoryID" Type="String" DefaultValue="14" />
        <asp:Parameter Name="Tag" Type="String" />
        <asp:Parameter DefaultValue="" Name="IsShowOnHomePage" Type="String" />
        <asp:Parameter Name="IsHot" Type="String" />
        <asp:Parameter Name="IsNew" Type="String" />
        <asp:Parameter Name="FromDate" Type="String" />
        <asp:Parameter Name="ToDate" Type="String" />
        <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
        <asp:Parameter DefaultValue="" Name="Priority" Type="String" />
        <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>