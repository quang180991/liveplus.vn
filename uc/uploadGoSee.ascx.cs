﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TLLib;

public partial class uc_uploadGoSee : System.Web.UI.UserControl
{

    public static string ImageName;
    public static string ImageVideo;
    public static string VideoName;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        bool IsLogin = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
        if (IsLogin == true)
        {
            var oWatchLaugh = new WatchLaugh();
            string strWatchLaughID = "";
            string Title = txtTitle.Text.ToString().Trim();
            string Origin = txtOrigin.SelectedItem.Text.ToString().Trim();
            string Quote = txtQuote.Text.ToString().Trim();
            string UserName = System.Web.HttpContext.Current.User.Identity.Name;
            var strConvertedTitle = Common.ConvertTitle(Title);
            strWatchLaughID = !string.IsNullOrEmpty(strWatchLaughID) ? strWatchLaughID : oWatchLaugh.WatchLaughSelectMaxID();

            if (RadioButtonList2.SelectedValue == "1")
            {
                oWatchLaugh.WatchLaughInsert(Title, "", "", "", strConvertedTitle, "", "", Quote, "", "1", "false", "", "", "0", "0", "0", "0", UserName, Origin, "");

            }
            else if (RadioButtonList2.SelectedValue == "2")
            {
                string ImagePath = "";
                if (FileImageName.HasFile)
                {
                    ImageName = FileImageName.FileName;
                    ImageName = string.IsNullOrEmpty(ImageName) ? "" : (string.IsNullOrEmpty(strConvertedTitle) ? "" : strConvertedTitle + "-") + strWatchLaughID + ImageName.Substring(ImageName.LastIndexOf('.'));
                    ImagePath = Server.MapPath("~/res/watchlaugh/picture/" + ImageName);
                    string strImagePath = "~/res/watchlaugh/picture/" + ImageName;
                    FileImageName.SaveAs(ImagePath);
                    ResizeCropImage.ResizeByCondition(strImagePath, 800, 585);
                }
                oWatchLaugh.WatchLaughInsert(Title, "", "", "", strConvertedTitle, ImageName, "", "", "", "2", "false", "", "", "0", "0", "0", "0", UserName, Origin, "");

            }
            else if (RadioButtonList2.SelectedValue == "3")
            {
                string ImagePath = "";
                if (FileImageVideo.HasFile)
                {
                    ImageVideo = FileImageVideo.FileName;
                    ImageVideo = string.IsNullOrEmpty(ImageVideo) ? "" : (string.IsNullOrEmpty(strConvertedTitle) ? "" : strConvertedTitle + "-") + strWatchLaughID + ImageVideo.Substring(ImageVideo.LastIndexOf('.'));
                    ImagePath = Server.MapPath("~/res/watchlaugh/video/thumbs/" + ImageVideo);
                    string strImagePath = "~/res/watchlaugh/video/thumbs/" + ImageName;
                    FileImageVideo.SaveAs(ImagePath);
                    ResizeCropImage.ResizeByCondition(strImagePath, 800, 585);
                }
             
                string VideoPath = "";
                if (FileVideoName.HasFile)
                {
                    VideoName = FileVideoName.FileName;
                    VideoName = string.IsNullOrEmpty(VideoName) ? "" : (string.IsNullOrEmpty(strConvertedTitle) ? "" : strConvertedTitle + "-") + strWatchLaughID + VideoName.Substring(VideoName.LastIndexOf('.'));
                    VideoPath = Server.MapPath("~/res/watchlaugh/video/" + VideoName);
                    FileVideoName.SaveAs(VideoPath);
                }
                oWatchLaugh.WatchLaughInsert(Title, "", "", "", strConvertedTitle, ImageVideo, VideoName, "", "", "3", "false", "", "", "0", "0", "0", "0", UserName, Origin, "");

            }
            Session["Success"] = "true";
            new TLLib.AddressBook().UserProfile_UpdatePoint_Address(UserName);

            Response.Redirect(Page.Request.UrlReferrer.ToString());
        }
        else
        {
            Response.Redirect("Login.aspx?ReturnUrl=" + Page.Request.UrlReferrer.ToString());
        }
        txtTitle.Text = "";
        txtOrigin.Text = "";
        txtQuote.Text = "";
    }
}