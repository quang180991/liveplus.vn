﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="registerloginsite.ascx.cs" 
    Inherits="uc_register" %>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">
        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <p class="modal-title">
        Đăng nhập/Đăng ký
    </p>
</div>

<asp:UpdatePanel UpdateMode="Conditional" ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-5">
                    <div class="login-box">
                        <h4 class="p-tit">Đăng nhập nếu bạn đã có tài khoản</h4>

                        <asp:Login ID="Login1" Width="100%" runat="server" OnLoggedIn="Login1_LoggedIn" FailureText="Tài khoản hoặc mật khẩu không đúng. Vui lòng thử lại!">
                            <LayoutTemplate>
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnLogin">
                                    <span class="failureNotification">
                                        <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                                    </span>
                                    <fieldset class="send-login">
                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="UserName" CssClass="form-control" placeholder="-- Tên đăng nhập --"
                                                    runat="server" Text=""></asp:TextBox>
                                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator22" runat="server"
                                                    Display="Dynamic" ValidationGroup="loginXDC" ControlToValidate="UserName" ErrorMessage="Thông tin bắt buộc!"
                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="Password" CssClass="form-control" TextMode="Password" placeholder="-- Mật khẩu --"
                                                    runat="server" Text=""></asp:TextBox>
                                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator23" runat="server"
                                                    Display="Dynamic" ValidationGroup="loginXDC" ControlToValidate="Password" ErrorMessage="Thông tin bắt buộc!"
                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <asp:Button ID="btnLogin" CssClass="btn-send" ValidationGroup="loginXDC" CommandName="Login"
                                                    runat="server" Text="đăng nhập" />
                                            </div>
                                        </div>
                                    </fieldset>
                                </asp:Panel>
                            </LayoutTemplate>
                        </asp:Login>
                        <p class="p-node">
                            Click vào nút dưới đây để
                <asp:LinkButton ID="lkbFacebookLogin" OnClick="lkbFacebook_Click" runat="server">đăng nhập với tài khoản Facebook</asp:LinkButton>
                            của bạn. Tài khoản của bạn trên LivePlus sẽ tự động được tạo sau lần đăng nhập đầu
                tiên mà không cần đăng ký.
                        </p>
                        <p class="login-facebook">
                            <asp:LinkButton ID="lkbFacebook" OnClick="lkbFacebook_Click" runat="server"><img src="assets/images/like-login.png" alt="login facebook" /></asp:LinkButton>
                        </p>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="register-box">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="validation_summary" />
                        <h4 class="p-tit">Bạn chưa có tài khoản</h4>
                        <p class="p-node">
                            Vui lòng điền vào thông tin bên dưới
                        </p>
                        <fieldset class="send-login">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">
                                    Họ tên</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtFullName" CssClass="form-control" placeholder="-- Nguyễn Văn A --"
                                        runat="server" Text=""></asp:TextBox>
                                    <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator24" runat="server"
                                        Display="Dynamic" ValidationGroup="registerXDC" ControlToValidate="txtFullName"
                                        ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">
                                    Tên đăng nhập</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtUserName1" OnTextChanged="txtUserName1_TextChanged" AutoPostBack="true" CssClass="form-control" placeholder="-- xitin_1990  --"
                                        runat="server" Text=""></asp:TextBox>
                                    <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator25" runat="server"
                                        Display="Dynamic" ValidationGroup="registerXDC" ControlToValidate="txtUserName1"
                                        ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtUserName1"
                                        Display="None"></asp:CustomValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">
                                    Email</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtEmail" OnTextChanged="txtEmail_TextChanged" AutoPostBack="true" CssClass="form-control" runat="server" Text="" placeholder="-- anguyenvan@gmail.com --"></asp:TextBox>
                                    <asp:RegularExpressionValidator CssClass="lb-error" ID="RegularExpressionValidator3"
                                        runat="server" ValidationGroup="registerXDC" ControlToValidate="txtEmail" ErrorMessage="Sai định dạng email!"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"
                                        ForeColor="Red"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator26" runat="server"
                                        ValidationGroup="registerXDC" ControlToValidate="txtEmail" ErrorMessage="Thông tin bắt buộc!"
                                        Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="txtEmail"
                                        Display="None"></asp:CustomValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">
                                    Điện thoại</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtPhone" CssClass="form-control" runat="server" Text="" placeholder="-- 0909 967 579  --"></asp:TextBox>
                                    <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator27" runat="server"
                                        Display="Dynamic" ValidationGroup="registerXDC" ControlToValidate="txtPhone"
                                        ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator CssClass="lb-error" ID="RegularExpressionValidator1"
                                        runat="server" ValidationGroup="registerXDC" ControlToValidate="txtPhone" ErrorMessage="Vui lòng nhập số!"
                                        ValidationExpression="^[0-9]+$" Display="Dynamic" ForeColor="Red"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">
                                    Địa chỉ</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtAddress" CssClass="form-control" placeholder="-- 20A, Bình Trị Đông B, Bình Tân, TP.HCM  --"
                                        runat="server" Text=""></asp:TextBox>
                                    <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator28" runat="server"
                                        Display="Dynamic" ValidationGroup="registerXDC" ControlToValidate="txtAddress"
                                        ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">
                                    Mật khẩu</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtPassWord1" CssClass="form-control" TextMode="Password" placeholder="******"
                                        runat="server" Text=""></asp:TextBox>
                                    <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator29" runat="server"
                                        Display="Dynamic" ValidationGroup="registerXDC" ControlToValidate="txtPassWord1"
                                        ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">
                                    Nhập lại mật khẩu</label>
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtConfirmPassWord" TextMode="Password" CssClass="form-control"
                                        placeholder="******" runat="server" Text=""></asp:TextBox>
                                    <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator30" runat="server"
                                        Display="Dynamic" ValidationGroup="registerXDC" ControlToValidate="txtConfirmPassWord"
                                        ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="+ Xác nhận mật khẩu sai"
                                        ControlToCompare="txtPassWord1" ControlToValidate="txtConfirmPassWord" Display="None"></asp:CompareValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">
                                    Ảnh đại diện</label>
                                <div class="col-sm-8">
                                    <asp:FileUpload ID="fileImageName" runat="server" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-4">
                                    <asp:Button ID="btnDangKy" CssClass="btn-send" ValidationGroup="registerXDC" OnClick="btnDangKy_Click"
                                        runat="server" Text="đăng ký" />
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hdnCMD" runat="server" />
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
