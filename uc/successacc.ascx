﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="successacc.ascx.cs" Inherits="uc_successUpload" %>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <h4 class="modal-title">Bạn đã đăng ký thành công. Cảm ơn bạn đã chung tay xây dựng Live Plus.</h4>
</div>
<div class="modal-body">
    <%--<p>Bạn được cộng một điểm thành viên tích cực.</p>--%>
    <asp:ListView ID="ListView1" runat="server"
        DataSourceID="ObjectDataSource2"
        EnableModelValidation="True">
        <ItemTemplate>
            <%# Eval("Content") %>
        </ItemTemplate>
        <LayoutTemplate>
            <span runat="server" id="itemPlaceholder" />
        </LayoutTemplate>
    </asp:ListView>
    <p><a href="cong-tac-vien-quy-dinh.aspx" target="_blank">Xem thêm về quyền lợi thành viên tại đây</a></p>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
<asp:ObjectDataSource ID="ObjectDataSource2" runat="server"
    SelectMethod="AggregatedSelectAll" TypeName="TLLib.Aggregated">
    <SelectParameters>
        <asp:Parameter Name="StartRowIndex" Type="String" />
        <asp:Parameter Name="EndRowIndex" Type="String" />
        <asp:Parameter Name="Keyword" Type="String" />
        <asp:Parameter Name="AggregatedTitle" Type="String" />
        <asp:Parameter Name="Description" Type="String" />
        <asp:Parameter Name="AggregatedCategoryID" Type="String" DefaultValue="9" />
        <asp:Parameter Name="Tag" Type="String" />
        <asp:Parameter DefaultValue="" Name="IsShowOnHomePage" Type="String" />
        <asp:Parameter Name="IsHot" Type="String" />
        <asp:Parameter Name="IsNew" Type="String" />
        <asp:Parameter Name="FromDate" Type="String" />
        <asp:Parameter Name="ToDate" Type="String" />
        <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
        <asp:Parameter DefaultValue="" Name="Priority" Type="String" />
        <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
