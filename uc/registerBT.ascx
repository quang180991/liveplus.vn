﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="registerBT.ascx.cs" Inherits="uc_registerBT" %>
 <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <p class="modal-title">Báo Trùng</p>
                </div>
            <div class="modal-body">
                    <fieldset class="send-wrap">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tác phẩm trùng với</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="tptrung" CssClass="form-control" runat="server">
                                    <asp:ListItem>Tác phẩm của bạn</asp:ListItem>
                                    <asp:ListItem>Tác phẩm của người khác</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator7" runat="server"
                                    Display="Dynamic" ValidationGroup="InformationBT" ControlToValidate="tptrung"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Link trùng</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="linktrung" CssClass="form-control" placeholder="-- Nhập họ tên --" runat="server" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator6" runat="server"
                                    Display="Dynamic" ValidationGroup="InformationBT" ControlToValidate="linktrung"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Họ tên</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="textht" CssClass="form-control" placeholder="-- Nhập họ tên --" runat="server" Text=""></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator8" runat="server"
                                    Display="Dynamic" ValidationGroup="InformationBT" ControlToValidate="textht"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Số điện thoại</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="textdt" CssClass="form-control" runat="server" Text="" placeholder="-- Nhập số điện thoại --"></asp:TextBox>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator4" runat="server"
                                    Display="Dynamic" ValidationGroup="InformationBT" ControlToValidate="textdt"
                                    ErrorMessage="Thông tin bắt buộc!" ForeColor="Red"></asp:RequiredFieldValidator>
                                  <asp:RegularExpressionValidator CssClass="lb-error" ID="RegularExpressionValidator2"
                            runat="server" ValidationGroup="InformationBT" ControlToValidate="textdt" ErrorMessage="Vui lòng nhập số!"
                            ValidationExpression="^[0-9]+$" Display="Dynamic"
                            ForeColor="Red"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <asp:TextBox ID="textemail" CssClass="form-control" runat="server" Text="" placeholder="-- Nhập email --"></asp:TextBox>
                                <asp:RegularExpressionValidator CssClass="lb-error" ID="RegularExpressionValidator1"
                                    runat="server" ValidationGroup="InformationBT" ControlToValidate="textemail" ErrorMessage="Sai định dạng email!"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"
                                    ForeColor="Red"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator CssClass="lb-error" ID="RequiredFieldValidator1" runat="server"
                                    ValidationGroup="InformationBT" ControlToValidate="textemail" ErrorMessage="Thông tin bắt buộc!"
                                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </fieldset>
            </div>
            <div class="modal-footer">
                <div class="wrap-btn">
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <asp:Button ID="btnUpLoad" OnClick="btnUpLoad_Click" CssClass="btn-send" ValidationGroup="InformationBT" runat="server" Text="Báo Trùng" />
                        </div>
                    </div>
                </div>
            </div>
            </div>