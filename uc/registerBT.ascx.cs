﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI.Skins;

public partial class uc_registerBT : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnUpLoad_Click(object sender, EventArgs e)
    {
        string msg = "<h3>Live Plus : Báo trùng</h3><br/>";
        msg += "<b>Tác phẩm báo trùng: </b>" + Page.Request.Url.Host.ToString()+ Page.Request.Url.AbsolutePath.ToString() + "<br /><br />";
        msg += "<b>Tác phẩm trùng với: </b>" + tptrung.SelectedItem.Text.ToString().Trim() + "<br /><br />";
        msg += "<b>Link: </b>" + linktrung.Text.Trim().ToString() + "<br /><br />";
        msg += "<b>Họ tên: </b>" + textht.Text.Trim().ToString() + "<br /><br />";
        msg += "<b>Điện thoại: </b>" + textdt.Text.Trim().ToString() + "<br /><br />";
        msg += "<b>Email: </b>" + textemail.Text.Trim().ToString() + "<br /><br />";

        var fileMap = new ExeConfigurationFileMap() { ExeConfigFilename = Server.MapPath("~/config/app.config") };
        var config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
        var section = (AppSettingsSection)config.GetSection("appSettings");
        var strUseSSL = section.Settings["UseSSL"].Value;
        var strPort = section.Settings["Port"].Value;
        var strHost = section.Settings["Host"].Value;
        int n;
        bool isNumeric = int.TryParse(strPort, out n);
        var iPort = isNumeric ? n : 0;
        var strMailFrom = section.Settings["Email"].Value;
        var strUserName = section.Settings["UserName"].Value;
        var strPassword = TLLib.MD5Hash.Decrypt(section.Settings["Password"].Value, true);
        var strReceivedEmails = section.Settings["ReceivedEmails"].Value;
        var bEnableSsl = strUseSSL.ToLower() == "true" ? true : false;
        var strSubject = "Báo trùng từ website Live Plus";
        var strBody = msg;
        var lstMailTo = new List<string>() { strReceivedEmails, textemail.Text.ToString() };
        var lstCC = "";
        var lstAttachment = new List<string>();
        foreach (string item in lstMailTo)
        {

            var bSendSuccess = TLLib.Common.SendMail(
                    strHost,
                    iPort,
                    strMailFrom,
                    strUserName,
                    strPassword,
                    item,
                    lstCC,
                    strSubject,
                    strBody,
                    bEnableSsl
                );
        }
        linktrung.Text = textht.Text = textdt.Text = textemail.Text = "";

        Session["successBT"] = "true";

        Response.Redirect(Page.Request.Url.AbsolutePath.ToString());
    }
}