﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site-sub.master" AutoEventWireup="true" CodeFile="cong-tac-vien-quy-dinh-chi-tiet.aspx.cs" Inherits="cong_tac_vien_chi_tiet" %>
<%@ Register Src="~/uc/registeredCTV.ascx" TagPrefix="uc1" TagName="registeredCTV" %>
<%@ Register Src="~/uc/successCTV.ascx" TagPrefix="uc1" TagName="successCTV" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <meta name="thumbnail" content='<%= Page.Request.Url.Host.ToString() + "/res/aggregated/" + hdnImageName.Value  %>' />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <asp:HiddenField ID="hdnImageName" runat="server" />
    <div id="site">
        <a href="cong-tac-vien.aspx">Cộng tác viên</a><span>Thông tin dành cho cộng tác viên</span>
    </div>
    <div class="head-box">
        <div class="head-box-b">
            <h1 class="title"><asp:Label ID="lbname" runat="server"></asp:Label></h1>
        </div>
    </div>
    <div class="wrap-text">
        <asp:ListView ID="ListView1" runat="server"
            DataSourceID="ObjectDataSource1"
            EnableModelValidation="True">
            <ItemTemplate>
                <%# Eval("Content") %>
            </ItemTemplate>
            <LayoutTemplate>
                <span runat="server" id="itemPlaceholder" />
            </LayoutTemplate>
        </asp:ListView>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
            SelectMethod="AggregatedSelectOne" TypeName="TLLib.Aggregated">
            <SelectParameters>
                <asp:QueryStringParameter Name="AggregatedID" QueryStringField="cqd" Type="String" DefaultValue="" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphSibar" runat="Server">
    <h2 class="title-c">Danh mục</h2>
    <ul class="list-nav">
        <li><a href="cong-tac-vien-quy-dinh.aspx">Thông tin dành cho cộng tác viên</a></li>
        <li><a href="cong-tac-vien.aspx">Hoạt động cộng tác viên</a></li>
    </ul>
    <div class="box-res">
        <h2>Hãy tham gia vào cộng đồng <strong>CỘNG TÁC VIÊN LIVE PLUS </strong><span>để cuộc sống mỗi ngày trở nên ý nghĩa hơn</span></h2>
        <p class="more-gis">
            
              <a href='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ?"javarscript:void(0);" : "popuppage/Login.aspx?cmd=CTV" %>' data-toggle="modal" data-target='<%= Roles.IsUserInRole(System.Web.HttpContext.Current.User.Identity.Name,"partners") ==true ?"": System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "#popupctv" : "#popupPages" %>'><%= Roles.IsUserInRole(System.Web.HttpContext.Current.User.Identity.Name,"partners") ==true ?"Bạn đã là Cộng Tác Viên":"Đăng kí"%></a></p></div>
    <div class="asibar-slider">
        <ul id="slidera">
            <asp:ListView ID="ListView3" runat="server"
                DataSourceID="ObjectDataSource3"
                EnableModelValidation="True">
                <ItemTemplate>
                    <li>
                        <div class="box-s">
                            <img id="Img2" alt='' src='<%# "http://www.liveplus.vn/res/advertisement/" + Eval("FileName") %>'  class="img-responsive"
visible='<%# string.IsNullOrEmpty( Eval("FileName").ToString()) ? false : 
true %>' runat="server" /> 
                            <div class="box-scontent"><a href='<%# Eval("Website") %> '><%# Eval("CompanyName") %> </a></div>
                        </div>
                    </li>
                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>
        </ul>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server"  
            SelectMethod="AdsBannerSelectAll" TypeName="TLLib.AdsBanner"> 
            <SelectParameters> 
                <asp:Parameter Name="StartRowIndex" Type="String" /> 
                <asp:Parameter Name="EndRowIndex" Type="String" /> 
                <asp:Parameter Name="AdsCategoryID" Type="String" DefaultValue="4" /> 
                <asp:Parameter Name="CompanyName" Type="String" /> 
                <asp:Parameter Name="Website" Type="String" /> 
                <asp:Parameter Name="FromDate" Type="String" /> 
                <asp:Parameter Name="ToDate" Type="String" /> 
                <asp:Parameter Name="IsAvailable" Type="String" DefaultValue="true" /> 
                <asp:Parameter Name="Priority" Type="String" /> 
                <asp:Parameter Name="SortByPriority" Type="String" DefaultValue="true" /> 
            </SelectParameters> 
        </asp:ObjectDataSource> 
    <div class="box-wp">
        <h2 class="title-b">các hoạt động của
            <br />
            cộng tác viên</h2>
        <div class="row">
            <asp:ListView ID="ListView2" runat="server"
                DataSourceID="ObjectDataSource2"
                EnableModelValidation="True">
                <ItemTemplate>
                    <div class="col-md-12 col-xs-6">
                        <div class="box-news news-bs">
                            <a href='<%# progressTitle(Eval("LawCounselTitle")) + "-cb-" + Eval("LawCounselID") +".aspx" %>' class="news-img">
                                <img id="Img1" alt='' src='<%# "http://www.liveplus.vn/res/lawcounsel/" + Eval("ImageName") %>' class="img-responsive"
                                    visible='<%# string.IsNullOrEmpty( Eval("ImageName").ToString()) ? false : 
true %>'
                                    runat="server" /></a>
                            <div class="news-content">
                                <h3 class="news-names"><a href='<%# progressTitle(Eval("LawCounselTitle")) + "-cb-" + Eval("LawCounselID") +".aspx" %>'><%# Eval("LawCounselTitle") %></a></h3>
                                <div class="colla-bottom">
                                    <span class="date"><%# string.Format("{0:dd.MM.yyyy}", Eval("CreateDate"))%></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>

        </div>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSource2" runat="server"
        SelectMethod="LawCounselSelectAll" TypeName="TLLib.LawCounsel">
        <SelectParameters>
            <asp:Parameter Name="StartRowIndex" Type="String" DefaultValue="1" />
            <asp:Parameter DefaultValue="6" Name="EndRowIndex" Type="String" />
            <asp:Parameter Name="Keyword" Type="String" />
            <asp:Parameter Name="LawCounselTitle" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="LawCounselCategoryID" Type="String" />
            <asp:Parameter Name="Tag" Type="String" />
            <asp:Parameter Name="IsShowOnHomePage" Type="String" />
            <asp:Parameter Name="IsHot" Type="String" />
            <asp:Parameter Name="IsNew" Type="String" />
            <asp:Parameter Name="FromDate" Type="String" />
            <asp:Parameter Name="ToDate" Type="String" />
            <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
            <asp:Parameter Name="Priority" Type="String" />
            <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphPopup" runat="Server">
   
    <!-- Modal -->
    <a href="javascript:void(0);" class="show-ctvup" data-toggle="modal" data-target="#popupctv"></a>
    <div class="modal fade popuplogin" id="popupctv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <uc1:registeredCTV runat="server" ID="registeredCTV" />
            </div>
        </div>
    </div>
      <!-- Modal -->
    <a href="javascript:void(0);" class="ctv-ctvacc" data-toggle="modal" data-target="#ctva"></a>
    <div class="modal fade popupbox" id="ctva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <uc1:successCTV runat="server" ID="successCTV" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="clientScript" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            //Goi Upload Choi De Hieu video
            <%if (Session["LoginCMD"] != null)
              { %>
              <%if (Session["LoginCMD"].ToString() == "CTV")
                { %>
            popuporctvupload();
            <% Session["LoginCMD"] = ""; %>
            <% }
          } %>
            //Goi Success Choi De Hieu video
            <%if (Session["Success"] != null)
              { %>
            <%if (Session["Success"].ToString() == "true")
              { %>
            popupctvaccess();
            <% Session["Success"] = ""; %>
            <% }
          } %>
        });</script>
</asp:Content>

