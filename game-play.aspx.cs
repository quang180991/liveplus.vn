﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TLLib;

public partial class game_play : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["IsGame"] != null)
        {
            if (Session["IsGame"] == "True")
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "runtime", " $(document).ready(function () {popupgamesd('" + Session["texthtmls"] + "');});", true);
                Session["IsGame"] = "";
                Session["texthtmls"] = "";
            }
        }
        //if (Session["PlayGames"] != null)
        //{
        //    if (Session["PlayGames"] != "True")
        //    {
        //        Response.Redirect("~/game.aspx");
        //    }
        //}
        if (!IsPostBack)
        {
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/");
            }
        }
    }
    protected void btnResults_Click(object sender, EventArgs e)
    {
        //var btn = (Button)sender;
        int itotal = 0;
        foreach (ListViewDataItem item in lstGames.Items)
        {
            //var GamesID = (item.FindControl("hdnGamesID") as HiddenField).Value;
            var hdnGamesTitleEn = (item.FindControl("hdnGamesTitleEn") as HiddenField).Value;
            var hdnAnswer1 = (item.FindControl("hdnAnswer1") as HiddenField).Value;
            var hdnAnswer2 = (item.FindControl("hdnAnswer2") as HiddenField).Value;
            var hdnAnswer3 = (item.FindControl("hdnAnswer3") as HiddenField).Value;
            var hdnAnswer4 = (item.FindControl("hdnAnswer4") as HiddenField).Value;
            var hdnAnswer5 = (item.FindControl("hdnAnswer5") as HiddenField).Value;

            var rdbAnswer1 = (item.FindControl("rdbAnswer1") as RadioButton).Checked;
            var rdbAnswer2 = (item.FindControl("rdbAnswer2") as RadioButton).Checked;
            var rdbAnswer3 = (item.FindControl("rdbAnswer3") as RadioButton).Checked;
            var rdbAnswer4 = (item.FindControl("rdbAnswer4") as RadioButton).Checked;
            var rdbAnswer5 = (item.FindControl("rdbAnswer5") as RadioButton).Checked;

            if (rdbAnswer1 && Convert.ToInt32(string.IsNullOrEmpty(hdnAnswer1) ? "0" : hdnAnswer1) == Convert.ToInt32(string.IsNullOrEmpty(hdnGamesTitleEn) ? "0" : hdnGamesTitleEn))
                itotal = itotal + 1;
            else if (rdbAnswer2 && Convert.ToInt32(string.IsNullOrEmpty(hdnAnswer2) ? "0" : hdnAnswer2) == Convert.ToInt32(string.IsNullOrEmpty(hdnGamesTitleEn) ? "0" : hdnGamesTitleEn))
                itotal = itotal + 1;
            else if (rdbAnswer3 && Convert.ToInt32(string.IsNullOrEmpty(hdnAnswer3) ? "0" : hdnAnswer3) == Convert.ToInt32(string.IsNullOrEmpty(hdnGamesTitleEn) ? "0" : hdnGamesTitleEn))
                itotal = itotal + 1;
            else if (rdbAnswer4 && Convert.ToInt32(string.IsNullOrEmpty(hdnAnswer4) ? "0" : hdnAnswer4) == Convert.ToInt32(string.IsNullOrEmpty(hdnGamesTitleEn) ? "0" : hdnGamesTitleEn))
                itotal = itotal + 1;
            else if (rdbAnswer5 && Convert.ToInt32(string.IsNullOrEmpty(hdnAnswer5) ? "0" : hdnAnswer5) == Convert.ToInt32(string.IsNullOrEmpty(hdnGamesTitleEn) ? "0" : hdnGamesTitleEn))
                itotal = itotal + 1;
        }
        var min = txtMM.Text;
        var sec = txtSS.Text;
        var total = lstGames.Items.Count;
        var stotal = itotal;
        var UserName = User.Identity.Name;
        var iDate = DateTime.Today.ToString("MM/dd/yyyy");
        var iTime = new TimeSpan(0, Convert.ToInt32(min), Convert.ToInt32(sec));
        var TimePlay = iDate + " " + DateTime.Parse(iTime.ToString()).ToString("hh:mm:ss");

        var oHistoryGame = new HistoryGame();
        oHistoryGame.HistoryGameInsert(
            UserName,
            TimePlay,
            total.ToString(),
            stotal.ToString(),
            stotal.ToString()
            );
        string texthtmls = "<p>Bạn trả lời đúng <strong>" + stotal + "</strong>/" + total + " câu hỏi trong <strong>" + min + "</strong> phút <strong>" + sec + "</strong> giây</p>";
        texthtmls += "<p>Sau 2 tuần, Live Plus sẽ tổng kết và trao quà cho người chơi có số câu trả lời đúng cao nhất trong thời gian ngắn nhất. </p>";
        texthtmls += "<h5>HÃY ĐỢI KẾT QUẢ TỪ CHƯƠNG TRÌNH BẠN NHÉ! </h5>";
        texthtmls += "<h4>Liveplus.vn</h4>";
        Session["texthtmls"] = texthtmls;
        Session["IsGame"] = "True";
        Response.Redirect(Page.Request.Url.AbsolutePath.ToString());
        //Common.ShowAlert(total.ToString() + " - " + stotal.ToString());
    }
}