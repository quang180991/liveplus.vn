﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TLLib;

public partial class choi_de_hieu_chi_tiet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            string ID = Request.QueryString["pl"];
            if (ID != null)
            {
                new Learns().LearnsUpdateCountViews(ID);
                string UserName = System.Web.HttpContext.Current.User.Identity.Name;
                var oVote = new Learns_User().LearnsUserSelectAll(ID, UserName).DefaultView;
                if (oVote.Count == 1)
                {

//btnVote.Enabled = false;

                }
            }
        }
    }
    protected string progressTitle(object input)
    {
        var convertTitle = new ConvertTitle();
        return convertTitle.convertToLowerCase(input.ToString());
    }
    protected void btnVote_Click(object sender, EventArgs e)
    {
        bool IsLogin = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;

        var dv1 = (DataView)ObjectDataSource1.Select();
        if (dv1.Count > 0)
        {
            if (IsLogin == true)
            {
                string UserName = System.Web.HttpContext.Current.User.Identity.Name;
                string LearnsID = dv1[0]["LearnsID"].ToString();
                new Learns_User().LearnsUserInsert(LearnsID, UserName, "", "", "1");
                new Learns().LearnsUpdateCountVoted(LearnsID);
            }
            else
            {
                TLLib.Common.ShowAlert(" Bạn hãy đăng nhập để thực hiện chức năng này");
                Response.Redirect("Login.aspx");
            }

        }
    }
}