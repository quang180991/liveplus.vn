﻿<%@ Page Title="" Language="C#" MasterPageFile="~/site-sub.master" AutoEventWireup="true"
    CodeFile="cong-tac-vien.aspx.cs" Inherits="cong_tac_vien" %>

<%@ Register Src="~/uc/registeredCTV.ascx" TagPrefix="uc1" TagName="registeredCTV" %>
<%@ Register Src="~/uc/successCTV.ascx" TagPrefix="uc1" TagName="successCTV" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Hoạt động cộng tác viên</title>
    <meta name="Description" content="Hoạt động cộng tác viên" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="site">
        <a href="cong-tac-vien.aspx">Cộng tác viên</a><span>Hoạt động cộng tác viên</span>
    </div>
    <div class="head-box">
        <div class="head-box-b">
            <h1 class="title">hoạt động cộng tác viên</h1>
        </div>
    </div>
    <div id="congtacvien" class="wrapper-section">
        <div class="row">
            <asp:ListView ID="ListView1" runat="server" DataSourceID="ObjectDataSource1" EnableModelValidation="True">
                <ItemTemplate>
                    <div class="col-sm-6 item-border-bottom">
                        <div class="colla-box">
                            <div class="colla-box-b">
                                <a href='<%# progressTitle(Eval("LawCounselTitle")) + "-cb-" + Eval("LawCounselID") +".aspx" %>'
                                    class="colla-img bcover">
                                    <img id="Img1" alt='' src='<%# "http://www.liveplus.vn/res/lawcounsel/" + Eval("ImageName") %>' class="img-responsive"
                                        visible='<%# string.IsNullOrEmpty( Eval("ImageName").ToString()) ? false : 
true %>'
                                        runat="server" /></a>
                                <div class="colla-content">
                                    <h3 class="colla-name">
                                        <a href='<%# progressTitle(Eval("LawCounselTitle")) + "-cb-" + Eval("LawCounselID") +".aspx" %>'>
                                            <%# Eval("LawCounselTitle") %></a></h3>
                                    <div class="description">
                                        <%# TLLib.Common.SplitSummary(Eval("Description").ToString(),180) %>
                                    </div>
                                    <div class="colla-bottom">
                                        <span class="date">
                                            <%# string.Format("{0:dd.MM.yyyy}", Eval("CreateDate"))%>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="LawCounselSelectAll"
                TypeName="TLLib.LawCounsel">
                <SelectParameters>
                    <asp:Parameter Name="StartRowIndex" Type="String" />
                    <asp:Parameter Name="EndRowIndex" Type="String" />
                    <asp:Parameter Name="Keyword" Type="String" />
                    <asp:Parameter Name="LawCounselTitle" Type="String" />
                    <asp:Parameter Name="Description" Type="String" />
                    <asp:Parameter Name="LawCounselCategoryID" Type="String" />
                    <asp:Parameter Name="Tag" Type="String" />
                    <asp:Parameter Name="IsShowOnHomePage" Type="String" />
                    <asp:Parameter Name="IsHot" Type="String" />
                    <asp:Parameter Name="IsNew" Type="String" />
                    <asp:Parameter Name="FromDate" Type="String" />
                    <asp:Parameter Name="ToDate" Type="String" />
                    <asp:Parameter DefaultValue="true" Name="IsAvailable" Type="String" />
                    <asp:Parameter Name="Priority" Type="String" />
                    <asp:Parameter DefaultValue="true" Name="SortByPriority" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
        <div class="pager">
            <asp:DataPager ID="DataPager1" runat="server" PageSize="6" PagedControlID="ListView1">
                <Fields>
                    <asp:NumericPagerField ButtonCount="5" NumericButtonCssClass="numer-paging" CurrentPageLabelCssClass="current" />
                </Fields>
            </asp:DataPager>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphSibar" runat="Server">
    <h2 class="title-c">Danh mục</h2>
    <ul class="list-nav">
        <li><a href="cong-tac-vien-quy-dinh.aspx">Thông tin dành cho cộng tác viên</a></li>
        <li><a href="cong-tac-vien.aspx">Hoạt động cộng tác viên</a></li>
    </ul>
    <div class="box-res">
        <h2>Hãy tham gia vào cộng đồng <strong>CỘNG TÁC VIÊN LIVE PLUS </strong><span>để cuộc sống
                mỗi ngày trở nên ý nghĩa hơn</span></h2>
        <p class="more-gis">
            <a href='<%= System.Web.HttpContext.Current.User.Identity.IsAuthenticated ?"javarscript:void(0);" : "popuppage/Login.aspx?cmd=CTV" %>' data-toggle="modal" data-target='<%= Roles.IsUserInRole(System.Web.HttpContext.Current.User.Identity.Name,"partners") ==true ?"": System.Web.HttpContext.Current.User.Identity.IsAuthenticated ? "#popupctv" : "#popupPages" %>'><%= Roles.IsUserInRole(System.Web.HttpContext.Current.User.Identity.Name,"partners") ==true ?"Bạn đã là Cộng Tác Viên":"Đăng kí"%></a>
        </p>
    </div>
    <div class="asibar-slider">
        <ul id="slidera">
            <asp:ListView ID="ListView3" runat="server" DataSourceID="ObjectDataSource3" EnableModelValidation="True">
                <ItemTemplate>
                    <li>
                        <div class="box-s">
                            <img id="Img2" alt='' src='<%# "http://www.liveplus.vn/res/advertisement/" + Eval("FileName") %>' class="img-responsive"
                                visible='<%# string.IsNullOrEmpty( Eval("FileName").ToString()) ? false : 
true %>'
                                runat="server" />
                            <div class="box-scontent">
                                <a href='<%# Eval("Website") %> '>
                                    <%# Eval("CompanyName") %>
                                </a>
                            </div>
                        </div>
                    </li>
                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>
        </ul>
    </div>
    <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" SelectMethod="AdsBannerSelectAll"
        TypeName="TLLib.AdsBanner">
        <SelectParameters>
            <asp:Parameter Name="StartRowIndex" Type="String" />
            <asp:Parameter Name="EndRowIndex" Type="String" />
            <asp:Parameter Name="AdsCategoryID" Type="String" DefaultValue="4" />
            <asp:Parameter Name="CompanyName" Type="String" />
            <asp:Parameter Name="Website" Type="String" />
            <asp:Parameter Name="FromDate" Type="String" />
            <asp:Parameter Name="ToDate" Type="String" />
            <asp:Parameter Name="IsAvailable" Type="String" DefaultValue="true" />
            <asp:Parameter Name="Priority" Type="String" />
            <asp:Parameter Name="SortByPriority" Type="String" DefaultValue="true" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <div class="box-wp">
        <h2 class="title-b">cộng tác viên tiêu biểu</h2>
        <div class="row">
            <asp:ListView ID="ListView2" runat="server" DataSourceID="ObjectDataSource2" EnableModelValidation="True">
                <ItemTemplate>
                    <div class="col-md-12 col-xs-6">
                        <div class="box-news">
                            <a href="javasrcipt:void(0);" class="news-img">
                                <img id="Img1" alt='' src='<%# Eval("ImageName") != DBNull.Value ? "http://www.liveplus.vn/res/userprofile/" + Eval("ImageName") : (Eval("FaceBookID") != DBNull.Value ? "https://graph.facebook.com/" + Eval("FaceBookID") + "/picture?type=large" : "~/assets/images/No-avatar.png")  %>' class="img-responsive"
                                    runat="server" /></a>
                            <div class="news-content">
                                <h3 class="news-name">
                                    <a href="javasrcipt:void(0);">
                                        <%# Eval("FirstName") %></a></h3>
                                <p class="p-list list-p1">
                                    Gia nhập:<%# string.Format("{0:dd.MM.yyyy}", Eval("CreateDate"))%>
                                </p>
                                <p class="p-list list-p2">
                                    Điểm tích lũy:
                                        <%# Eval("Point") %>
                                </p>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
                <LayoutTemplate>
                    <span runat="server" id="itemPlaceholder" />
                </LayoutTemplate>
            </asp:ListView>
            <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="UserProfileSelectAll_AddressBook_CTV"
                TypeName="TLLib.AddressBook">
                <SelectParameters>
                    <asp:Parameter Name="UserName" Type="String" />
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter DefaultValue="partners" Name="RoleName" Type="String" />
                    <asp:Parameter Name="Company" Type="String" />
                    <asp:Parameter Name="FirstName" Type="String" />
                    <asp:Parameter Name="LastName" Type="String" />
                    <asp:Parameter Name="Address1" Type="String" />
                    <asp:Parameter Name="Address2" Type="String" />
                    <asp:Parameter Name="HomePhone" Type="String" />
                    <asp:Parameter Name="CellPhone" Type="String" />
                    <asp:Parameter Name="Fax" Type="String" />
                    <asp:Parameter Name="CountryID" Type="String" />
                    <asp:Parameter Name="ProvinceID" Type="String" />
                    <asp:Parameter Name="DistrictID" Type="String" />
                    <asp:Parameter Name="IsPrimary" Type="String" />
                    <asp:Parameter Name="IsPrimaryBilling" Type="String" />
                    <asp:Parameter Name="IsPrimaryShipping" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphPopup" runat="Server">
    <a href="javascript:void(0);" class="show-ctvup" data-toggle="modal" data-target="#popupctv"></a>
    <div class="modal fade popuplogin" id="popupctv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <uc1:registeredCTV runat="server" ID="registeredCTV" />
            </div>
        </div>
    </div>
    <!-- Modal -->
    <a href="javascript:void(0);" class="ctv-ctvacc" data-toggle="modal" data-target="#ctva"></a>
    <div class="modal fade popupbox" id="ctva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <uc1:successCTV runat="server" ID="successCTV" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="clientScript" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            //Goi Upload Choi De Hieu video
            <%if (Session["LoginCMD"] != null)
              { %>
              <%if (Session["LoginCMD"].ToString() == "CTV")
                { %>
            popuporctvupload();
            <% Session["LoginCMD"] = ""; %>
            <% }
          } %>
            //Goi Success Choi De Hieu video
            <%if (Session["Success"] != null)
              { %>
            <%if (Session["Success"].ToString() == "true")
              { %>
            popupctvaccess();
            <% Session["Success"] = ""; %>
            <% }
          } %>
        });</script>
</asp:Content>
